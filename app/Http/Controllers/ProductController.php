<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Product_image;
use Response;
class ProductController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function product(){
        $products = Product::where('product_status',0)->where('product_qty','>',0)->get();
        return view('account.product.index',compact('products'));
    }

    public function getinfo(Request $request){
        $productid = $request->productid;
        if($productid){
            $product = Product::where('id',$productid)->first();
            $viewRendered = view('partial.product',compact('product'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }
}
