<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    //
    protected $fillable = [
        'user_id', 'parcelcode', 'weight' , 'width', 'height' ,'lenght', 'tracking' , 'shipcountry' , 'shipnumber' ,'status','pricetopay','statuspay'
    ];

    public function getuser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function GetImage(){
        return $this->hasMany('App\Shipimage');
    }

    public function getDeclaration(){
        return $this->hasMany('App\Declaration','shipping_id');
    }

    public function getService(){
        return $this->hasMany('App\Service','shipping_id')->where('type','service');
    }
    public function getCountService(){
        return $this->hasMany('App\Service','shipping_id')->where('type','service')->where('status',1);
    }
}
