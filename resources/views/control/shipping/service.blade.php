@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> طلبات على الشحنات </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>رقم الشحنة</th>
                          <th> رقم العميل </th>
                          <th> نوع الطب  </th>
                          <th> تاريخ الطلب </th>
                          <th> المحتوى </th>
                          <th> سعر الخدمة  </th>
                          <th> الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="">
                            @foreach ($services as $service)
                                <tr class="{{ $service->id }}">
                                  <td>
                                    <label class="custom-control custom-control-primary custom-checkbox">
                                        <input class="custom-control-input" name="checkAll" type="checkbox" value="{{ $service->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                  </td>
                                  <td class="parcelcode">{{ $service->getShip->parcelcode }}</td>
                                  <td>
                                      <a href="{{ url('control/clients',$service->getShip->getuser->policia)}}" target="_blank"> {{ $service->getShip->getuser->policia }}</a>
                                  </td>
                                  <td>
                                      @if($service->type == 'service')
                                        <b> طلب خدمة خاصة </b>
                                      @else
                                        <b> التبليغ عن خطأ </b>
                                      @endif
                                  </td>
                                  <td>{{ $service->updated_at->format('Y-m-d') }}</td>
                                  <td>{{ $service->content }}</td>
                                  <td>
                                    @if($service->type == 'service')
                                      @if($service->getShip->getuser->plan == 1)
                                        <b>   15 د.ل</b>
                                      @else
                                        <b>  25 د.ل</b>
                                      @endif
                                    @else
                                        <b> بدون رسوم </b>
                                    @endif
                                  </td>
                                  <td>
                                      @if($service->status  == 0)
                                        <span class='label label-info'>في الانتظار </span>
                                      @elseif($service->status  == 1)
                                        <span class='label label-success'> تم التنفيد </span>
                                      @else
                                        <span class='label label-primary'> طبي ملغي </span>
                                      @endif
                                  </td>
                                  <td>
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                          <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if($service->type == 'service')
                                                @if($service->status  == 0)
                                                    <li><a href="#" id="{{ $service->id }}" class="btn btn-link 1 opservice"> تم التنفيد </a></li>
                                                    <li><a href="#" id="{{ $service->id }}" class="btn btn-link 2 opservice"> الغاء الطلب  </a></li>
                                                @endif
                                            @else
                                                @if($service->status  == 0)
                                                    <li><a href="#" id="{{ $service->id }}" class="btn btn-link contact_service {{ $service->getShip->getuser->id }}">  تواصل مع العميل </a></li>
                                                @endif
                                            @endif
                                        </ul>
                                    </div>
                                  </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
