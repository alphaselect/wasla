@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> شحنات في الطريق </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <div class="row">
                <div class="col-xs-12">
                    <a href="#" class="btn btn-info pull-right"> تقرير الشحنات  </a>
                </div>
            </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>الصورة</th>
                          <th> كود الشحنة</th>
                          <th>رقم العميل</th>
                          <th>تاريخ الاضافة</th>
                          <th>الوزن / الأبعاد</th>
                          <th>الدولة </th>
                          <th> التتبع </th>
                          <th> المحتوى </th>
                          <th> السعر </th>
                          <th> الحالة </th>
                          <th> الدفع </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($parcels as $parcel)
                            <tr class="{{ $parcel->id}}">
                                <td>
                                    <label class="custom-control custom-control-primary custom-checkbox">
                                        <input class="custom-control-input" name="checkAllparcel" type="checkbox" value="{{ $parcel->id}}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>
                                    <span class="icon-with-child m-r">
                                        @foreach ($parcel->GetImage as $image)
                                            <img class="circle" width="40" height="40" src="{{ asset('upload/ship') }}/{{ $image->imagename}}" alt="">
                                            @break
                                        @endforeach
                                    </span>
                                </td>
                                <td> {{ $parcel->parcelcode }} </td>
                                <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
                                <td> {{ $parcel->created_at->format('Y-m-d')}} </td>
                                <td>
                                    {{ $parcel->weight }} كيلو <br>
                                    {{ $parcel->width }} * {{ $parcel->height }} * {{ $parcel->lenght }} سم
                                </td>
                                <td>
                                    @if($parcel->shipcountry == 'usa')
                                        امريكا
                                    @endif
                                </td>
                                <td>{{ $parcel->tracking }}</td>
                                <td> <a href="#" id="{{ $parcel->id}}" class="getDeclaration">مشاهدة </a> </td>
                                <td>
                                    @if($parcel->pricetopay)
                                        {{ $parcel->pricetopay }}$
                                    @else
                                    <b> ليس بعد </b>
                                    @endif
                                </td>
                                <td>
                                    @if($parcel->status == 1)
                                        <span class='label label-warning'> تم الشحن </span>
                                    @endif
                                </td>
                                <td>
                                    @if($parcel->statuspay == 0)
                                        <span class='label label-primary'> ليس بعد </span>
                                    @elseif($parcel->statuspay == 1)
                                        <span class='label label-success'> تم الدفع  </span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                        <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
