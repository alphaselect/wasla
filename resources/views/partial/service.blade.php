@foreach ($parcel->getService as $service)
    <tr>
        <td> {{ $parcel->parcelcode }} </td>
        <td> {{ $service->content }}</td>
        <td> {{ $service->created_at}}</td>
        <td>
            @if($service->status == 0)
                <span class="label label-info"> قيد التنقيد </span>
            @elseif($service->status == 1)
                <span class="label label-success"> تم التنقيد </span>
            @else
                <span class="label label-danger"> طلب ملغي </span>
            @endif
        </td>
    </tr>
@endforeach


