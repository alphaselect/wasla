<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\helpers;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use App\admin;
use App\Payment;
use App\User;
class AdminLoginController extends Controller
{
    /**
     * Show the application’s login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('control.auth.login');
    }
    protected function guard(){
        return Auth::guard('admin');
    }

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/control';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest:admin')->except('logout');
    }

    protected function login(Request $request){
        $data = $request->all();

        // Validate the form data
        $validator = Validator::make($data,[
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        $credentials = $request->only('email', 'password');
        if(auth()->guard('admin')->attempt($credentials)){
            // if successful, then redirect to their intended location
        // return redirect()->intended(route('control.index'));
        return redirect('/control');
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function register(Request $request){

        $data = $request->all();

        $validator = Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:admins',
            'password'  => 'required|string|min:6',
            'role'      => 'required|string|max:255',
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }


        if($file = $request->file('image')){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/admin/img');
            $file->move($destinationPath, $name);
        }else{
            $name = "-";
        }


        Admin::create([
            'username'  => $data['firstname'],
            'email'     => $data['email'],
            'firstname' => $data['firstname'],
            'lastname'  => $data['lastname'],
            'role'      => $data['role'],
            'image'     => $name,
            'password'  => bcrypt($data['password'])
        ]);


        return Response::json(array('success' => true), 200);

    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/control');
    }


    public function createPolicia(){
        $user = User::select('policia')->orderBy('id', 'desc')->first();
        if($user){
            $policia = generate_policia($user->policia);
        }else{
            $policia = generate_policia();
        }
        return $policia;
    }

    protected function getPolicia($policia){
        return User::select('policia')->where('policia', 'like', $policia.'%')->get();
    }

    protected function adduser(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $validator = Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'second_firstname' => 'required|string|max:255',
            'second_lastname'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6',
            'country'   => 'required',
            'city'      => 'required',
            'address'   => 'required',
            'phone'     => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }

        $user = User::create([
            'firstname'   => $data['firstname'],
            'lastname'    => $data['lastname'],
            'second_firstname'   => $data['second_firstname'],
            'second_lastname'    => $data['second_lastname'],
            'email'       => $data['email'],
            'country'     => $data['country'],
            'city'        => $data['city'],
            'address'     => $data['address'],
            'password'    => bcrypt($data['password']),
            'verifyToken' => Str::random(60),
            'policia'     => $this->createPolicia(),
            'phone'       => $data['phone'],
            'sms_code'    => generateRandomNumber(4),
            'plan'        => 0,
            'status'      => 1
        ]);

        $payment = Payment::create([
            'user_id' =>  $user->id,
            'invoice' => str_random(15),
            'operations' => 'حساب مجاني ',
            'funds_add' => 0,
            'funds_total' => 0,
            'modpayment' => '-'
        ]);

        return Response::json(array('success' => true), 200);

    }
}
