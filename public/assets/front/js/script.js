jQuery(document).ready(function($){


    $(".registerform").on("click",function(){
        var proceed = true;
		$(".registerfrom input[required=true] , .registerfrom select[name='city']").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
            var terms  = $("input[name='terms']:checked").val();
            if(terms){
                $.ajax({
                    type:'POST',
                    url : 'register',
                    data:$(".registerfrom").serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(resp){
                        if(resp.success == true){
                            $(".registererreur").html(" ");
                            //$('#loginmodal').modal('hide');
                            $('#registration-modal').modal('toggle');
                            //$('#myModalSignSuccess').modal();
                            $(".my_phone").val(resp.phone);
                            $(".userID").val(resp.user_id);
                            $('#ModalSMS').modal();
                        }
                    },
                    error:function(resp){
                        var response = JSON.parse(resp.responseText);
                        var errorString = '<ul>';
                        $.each( response.errors, function( key, value) {
                            errorString += '<li>' + value + '</li>';
                        });
                        errorString += '</ul>';
                        $(".registererreur").append(errorString);
                    }
                });
            }else{
                alert("يجب اولا الموافقة على الشروط و الاحكام الخاصة بالخدمات ");
            }
        }
    });
    //verify phone number
    $(".verifyPhone").on("click",function(){
        var my_phone = $(".my_phone").val();
        var sms_code = $(".sms_code").val();
        var userID   = $(".userID").val();
        if(userID && sms_code && my_phone){
            var data = "userID="+userID+"&sms_code="+sms_code+"&my_phone="+my_phone;
            $.ajax({
                type : 'POST',
                url  : 'verify_phone',
                data : data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success == true){
                        $('#ModalSMS').modal('toggle');
                        $('#myModalSignSuccess').modal();
                    }
                }
            });
        }else{
            alert("رقم الهاتف  و رمز التفعيل ضروي لتفعيل الحساب ");
        }
    });
    //changephone
    /*$(".changephone").on("click",function(){
        var my_phone = $(".my_phone").val();
        var userID   = $(".userID").val();
        if(my_phone && userID){
            var data = "userID="+userID+"&sms_code="+sms_code+"&my_phone="+my_phone;
            $.ajax({
                type : 'POST',
                url  : 'verify_phone',
                data : data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success == true){
                        $('#ModalSMS').modal('toggle');
                        $('#myModalSignSuccess').modal();
                    }
                }
            });
        }
    });*/
    //
    $(".formlogin").on("click",function(){
        //alert("hello")
        var proceed = true;
		$(".signinform input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
                var useremail = $("#email").val();
                var userpassowrd = $("#password").val();
                var data ="lg_username="+useremail+"&lg_password="+userpassowrd;
            $.ajax({
                type:'POST',
                url : 'login',
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $(".login-errors-msg").html(" ");
                    if(resp.success == true){
                        window.location.href = "/account";
                    }else{
                        $(".login-errors-msg").append("<span class='bg-erors'> المعلومات التي ادخلتها غير صحيحه او لا يوجد لديك حساب </span>");
                    }
                }
            });
        }
    });


    $(".declarationContentShipment").on("click",function(){
        var idshipment = $(this).attr("id");
        $(".idparcel").val(idshipment);
        $(".declarationContentShipment-modal").modal();
    });

    //reportError-modal
    $(".reportError").on("click",function(){
        var idshipment = $(this).attr("id");
        $(".idparcel").val(idshipment);
        $(".reportError-modal").modal();
    });

    //RequestSpecialService-modal
    $(".requestSpecialService").on("click",function(){
        var idshipment = $(this).attr("id");
        $(".idparcel").val(idshipment);
        $(".RequestSpecialService-modal").modal();
    });

    $('.addinformation').on("click",function(){
        var proceed = true;
		$(".declaration input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
            var someJSON = [];
            var idparcel    = $(".idparcel").val();
            $('.bts').each(function(index,element){
                var productname =  $("#field"+index+" div > .productname").val();
                var productqty  =  $("#field"+index+" div > .productqty").val();
                var productcost =  $("#field"+index+" div > .productcost").val();
                var l = {
                    "idparcel"    : idparcel,
                    "productname" : productname,
                    "productcost" : productcost,
                    "productqty"  : productqty,
                }
                someJSON.push(l);
            });
            $.ajax({
                type:'POST',
                url : 'addinformation',
                data: { data_array: JSON.stringify(someJSON) },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success === true){
                        swal("ممتاز", "تمت العملية المطلوبة بنجاح", "success");
                        setTimeout(function(){
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    $(".delared").on('click',function(){
        var delared = $(this).attr("id");
        var data    = "idparcel="+delared;
        $.ajax({
            type:'POST',
            url : 'delared',
            data:data,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(response){
                $(".result_decla").html(" ");
                $(".result_decla").append(response.html);
                $(".ModalTable_declaraion").modal();
                //if(resp.success === true){
                //}
            }
        });
    });

    //
    $('.addservices').on("click",function(){
        var type     = $(this).attr("id");
        var idparcel = $("."+type+" .idparcel").val();
        var content  = $("."+type+" .content").val();
        if(content){
            var data  = "idparcel="+idparcel+"&type="+type+"&content="+content;
            $.ajax({
                type:'POST',
                url :'addservices',
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success === true){
                        swal("ممتاز", "تمت العملية المطلوبة بنجاح", "success");
                        setTimeout(function(){
                            location.reload();
                        });
                    }
                }
            });
        }else{
            $(".content").css('border-color','red');
        }
    });
    $(".SpecialService").on("click",function(){
        var idparcel = $(this).attr("id");
        var data = "idparcel="+idparcel;
        $.ajax({
            type:'POST',
            url :'specialservice',
            data:data,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                $(".result_Service").html("");
                $(".result_Service").append(resp.html);
                $(".ModalTable_Service").modal();
            }
        });
    });
    $(".acceptshipping").on("click",function(){
        var idparcel = $(this).attr("id");
        var data = "idparcel="+idparcel;
        if(idparcel){
            $.ajax({
                type:'POST',
                url :'acceptshipping',
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success === true){
                        swal("ممتاز", "تمت العملية المطلوبة بنجاح", "success");
                        setTimeout(function(){
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    $(".payshipping").on("click",function(){
        var idparcel = $(this).attr("id");
        var pricetopay = $(this).val();
        var parcelcode = $("#"+idparcel+" .parcelcode").text();
        var SpecialService = $("#"+idparcel+" .price_Service").val();
        var global = parseFloat(pricetopay) + parseFloat(SpecialService);
        $(".pricetopay_service").val(SpecialService + "د.ل");
        $(".parcel_code").val(parcelcode);
        $(".idparcel").val(idparcel);
        $(".pricetopay_d").val(pricetopay);
        $(".pricetopay_global").val(global + "د.ل");
        $("#PayShippingModal").modal();
    });

    //paynowShipping
    $(".paynowShipping").on("click",function(){
        var idparcel = $(".idparcel").val();
        var data = "idparcel="+idparcel;
        if(idparcel){
            $.ajax({
                type:'POST',
                url :'paynowshipping',
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $(".formgroupShipping").html(" ");
                    if(resp.success === true){
                        $(".formgroupShipping").append("<span class='alert alert-success'>"+resp.message+"</span>");
                        setTimeout(function(){
                            location.reload();
                        });
                    }else{
                        $(".formgroupShipping").append("<span class='alert alert-danger'>"+resp.message+"</span>");
                    }
                }
            });
        }
    });
    $(".showprd").on("click",function(){
        var productid = $(this).attr("id");
        var data = "productid="+productid;
        $.ajax({
            type:'POST',
            url :'getinfo',
            data:data,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                $(".product_List").html(" ");
                $(".product_List").append(resp.html);
                $("#ProducteSliderModal").modal();
            }
        });
    });
    //
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        next = next + 1;
        var newIn = '<div id="field'+ next +'" class="bts"> <div class="col-md-5 col-xs-5 form-group"> <label class="control-label" for="productname"> اسم المنتج </label> <input id="productname" name="productname" required="true" type="text" placeholder="اسم المنتج" class="form-control productname"> </div> <div class="col-md-3 col-xs-3 form-group"> <label class="control-label" for="productqty"> الكمية </label> <input id="productqty" name="productqty" required="true" type="number" placeholder="الكمية" class="form-control productqty"> </div> <div class="col-md-3 col-xs-3 form-group"> <label class="control-label" for="productcost"> السعر </label> <input id="productcost" name="productcost" required="true" type="number" placeholder="السعر" class="form-control productcost"> </div><div class="col-md-1 col-xs-1 ls form-group"> <a href="#" id="remove' +next+ '" class="fa fa-trash remove_bs remove-me"><i class="icondelet"></i></a> </div>';
        var newInput = $(newIn);
        $(addto).after(newInput);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                next = fieldNum - 1;
                $(fieldID).remove();
            });
    });
    /*** end Query ***/
});





/*** stare Query ***/
$('.carousel').carousel()

$(document).on('click','.nav-rs a',function(){
	//$("top-menu nav").addClass("nav-responsive");
	$(".top-menu nav").toggleClass("nav-responsive");
	$(".top-menu .nav .nav-main-item").slideToggle();
});

function get_windowWith(){
    var $with=$( window ).width();
    if($with<767){
        $(".nav").addClass("menu-rs-active");
    }else{
        $(".nav").removeClass("menu-rs-active");
    }
}
get_windowWith();
$(window).resize(function() {
	get_windowWith();
});

/**
* drop down menu user
**/
$(document).on("click","li.menu-sm i",function(){
    var el=$(".sidebar-right");
    var curHeight = el.height();
    $(this).toggleClass('fa-bars');
    $(this).toggleClass('fa-times-circle');
    if(curHeight<69){
        autoHeight = el.css({'height':'auto' }).height();
        el.css({'height':'69' });
        el.animate({height:autoHeight},1000,function(){})
    }else{
        el.animate({height:"69"},1000,function(){})
    }

});
/**
* select menu
**/
select_menu_sm("","");

$(document).on("click",".sidebar-right .menu-user> ul >li a",function(){
    $(".sidebar-right .menu-user> ul >li").removeClass("active");
    $(this).parent().addClass("active");
   var $text=$(this).text();
   var $class=$(this).children("i").prop("class");

   $(".sidebar-right .menu-user> ul >li.menu-sm >a >span").empty();
    select_menu_sm($class,$text);
});

function select_menu_sm($class,$text){
    var $with=$( window ).width();
    if($with<767){
        if($class=="" && $text==""){
            var class_i_active=$(".sidebar-right .menu-user> ul >li.active >a>i").attr('class');
            var text_a_active=$(".sidebar-right .menu-user> ul >li.active a").text();
        }else{
            var class_i_active=$class;
            var text_a_active=$text
        }

        $(".sidebar-right .menu-user> ul >li.menu-sm >a >span").empty();
        $(".sidebar-right .menu-user> ul >li.menu-sm >a >span").text(text_a_active);
        $(".sidebar-right .menu-user> ul >li.menu-sm >a>i").attr("class",' ');
        $(".sidebar-right .menu-user> ul >li.menu-sm> a> i").addClass(class_i_active);
        $(".sidebar-right .menu-user> ul >li").removeClass("active");
        $(".sidebar-right .menu-user> ul >li.menu-sm").addClass("active");
    }else{
    // $(".sidebar-right .menu-user> ul >li").removeClass("active");

    }

}





$(document).ready(function(e){


/*********** Add collect data purchase ***********/
    $("body").on('click','.collect-purchase',function(e){

        var c_product=$("#c_product").val();
        var c_url=$("#c_url").val();
        var c_price=parseFloat($("#c_price").val());
        var c_properties=$("#c_properties").val();
        var c_quantity=parseInt($("#c_quantity").val());
        var total_data=parseInt($("#total_data").val());
        var total_price=parseFloat($("#total_price").val());
        var taux_purchase=parseFloat($("#taux_purchase").val());
        var exchange=parseFloat($("#exchange").val());
        total_price=total_price+(c_quantity*c_price);
           // alert(!isNaN(c_quantity));
           total_data=total_data+1;

           // alert(Number.isInteger(c_quantity));
            //var tr='<tr role="row" id="'+total_data+'"><td role="cell" class="c_product">'+c_product+'<span></td><td role="cell"><a href="'+c_url+'" target="_blank" class="c_url">رابط الموقع</a></td><td class="price" role="cell"><span class="c_price">'+c_price+'</span>$</td><td role="cell" class="c_quantity">'+c_quantity+'</td><td class="c_properties">'+c_properties+'</td><td><button class="btn btn-edit" data-toggle="modal" data-target=".editCollectOrder-modal" data-id="'+total_data+'"><i class="fa fa-pencil"></i>تعديل </button><button class="btn btn-delet" data-id="'+total_data+'"><i class="fa fa-trash-o"></i>حذف </button></td></tr>';
            var tr='<tr role="row" id="tr_'+total_data+'"><td role="cell" class="c_product">'+c_product+'<span><input type="hidden" name="in_product" class="in_product" value="'+c_product+'"></td><td role="cell"><a href="'+c_url+'" target="_blank" class="c_url">رابط الموقع</a><input type="hidden" name="in_url" class="in_url" value="'+c_url+'"></td><td class="price" role="cell"><span class="c_price">'+c_price+'</span>$<input type="hidden" name="in_price" class="in_price" value="'+c_price+'"></td><td role="cell" class="c_quantity">'+c_quantity+'<input type="hidden" name="in_quantity" class="in_quantity" value="'+c_quantity+'"></td><td class="c_properties">'+c_properties+'<input type="hidden" name="in_properties" class="in_properties" value="'+c_properties+'"></td><td><button class="btn btn-edit btn-sm" data-id="'+total_data+'"><i class="fa fa-pencil"></i>تعديل </button><button class="btn btn-delet btn-sm" data-id="'+total_data+'"><i class="fa fa-trash-o"></i>حذف </button></td></tr>';
        if(total_data<5){

            if(c_product && c_url && !isNaN(c_price) && c_properties && !isNaN(c_quantity)){


                $('.purchase-body table tbody').append(tr);
                hide_forme("add_colect_purchase");
                border_color_nn("#add_colect_purchase");
                $("#total_data").val(total_data);
                $("#total_price").val(total_price);
                $(".sum-purchase .price").html("");
                $(".sum-purchase .price").html(total_price+" $"+"/"+parseFloat(exchange*total_price).toFixed(2)+" د.ل");
                $(".sum-purchase").removeClass("hide");
            }else{
                //alert('المرجو ملء جميع الحقول');
                controle_form("#add_colect_purchase");
                is_float("#add_colect_purchase");
                is_int("#add_colect_purchase");
            }

        }else{
            alert("لقد تجاوزت الحد الأقصى من الطلبيات");
        }




});/*******end Add collect data purchase**/

/*********** edite data purchase ***********/
$("body").on('click','.purchase-body .btn-edit',function(e){

        var parent=$(this).parent().parent();
        var dataId=$(this).attr("data-id");
        parent.addClass('edit');
        var c_product=parent.find(".in_product").val();
        var c_url=parent.find(".in_url").val();
        var c_price=parent.find(".in_price").val();
        var c_properties=parent.find(".in_properties").val();
        var c_quantity=parent.find(".in_quantity").val();
        var exchange=parseFloat($("#exchange").val());
        var price=c_quantity*c_price;
        var tr='<td role="cell"><input type="text" name="c_product" class="form-control c_product" value="'+c_product+'" placeholder="المنتج" required></td><td role="cell"><input type="text" name="c_url" class="c_url form-control" value="'+c_url+'" placeholder="رابط المنتج" required></td><td role="cell"><input type="text" name="c_price"  class="c_price form-control float" value="'+c_price+'" placeholder="السعر" required></td><td role="cell"><input type="text" name="c_quantity" class="form-control c_quantity int" value="'+c_quantity+'" placeholder="الكمية" required></td><td><input type="text"name="c_properties" class="form-control c_properties" value="'+c_properties+'" placeholder="الخصائص"></td><td><button class="btn update-collect-purchase" data-id="'+dataId+'"><i class="fa fa-check"></i>تعديل </button><button class="btn btn-delet" data-id="'+dataId+'"><i class="fa fa-trash-o"></i>حذف </button></td>';

//var tr='<tr role="row"><td role="cell" class="c_product">'+c_product+'<span></td><td role="cell"><a href="'+c_url+'" target="_blank" class="c_url">رابط الموقع</a></td><td class="price" role="cell"><span class="c_price">'+c_price+'</span>$</td><td role="cell" class="c_quantity">'+c_quantity+'</td><td class="c_properties">'+c_properties+'</td><td><button class="btn btn-edit" data-toggle="modal" data-target=".editCollectOrder-modal"><i class="fa fa-pencil"></i>تعديل </button><button class="btn btn-delet"><i class="fa fa-trash-o"></i>حذف </button></td></tr>';

        if(c_product && c_url && c_price && c_properties && c_quantity){

            $(this).parent().parent().html(tr);

            $("#price_deduct").val(price);
        }else{
            alert('لا يمكن تحدث المنتج');
        }



});/*******end edite data purchase**/

/*********** delet data collect purchase ***********/
$("body").on('click','.purchase-body .btn-delet',function(e){

    var id=$(this).attr("data-id");
   // var count=$("table tbody tr").each();
    var tr_id="#tr_"+id;
   var c_price=parseFloat($(tr_id+" .in_price").val());
   var c_quantity=parseInt($(tr_id+" .in_quantity").val());
   var total_price=parseFloat($("#total_price").val());
   var exchange=parseFloat($("#exchange").val());
   total_price=total_price-(c_price*c_quantity);

   $("#total_price").val(total_price);

    var total_data=parseInt($("#total_data").val());
        total_data=total_data-1;
    if(total_price>0){
        $("#total_price").val(total_price);
        $(".sum-purchase .price").html("");
        $(".sum-purchase .price").html(total_price+"$"+"/"+parseFloat(exchange*total_price).toFixed(2)+" د.ل");
        $(".sum-purchase").removeClass("hide");
    }else{
        $("#total_price").val(0);
        $(".sum-purchase").addClass("hide");
    }



    $(tr_id).slideUp();
    $(tr_id).remove();

    $(".purchase-body table tbody tr").each(function(index){
        var id=index+1;

        $(this).attr('id','tr_'+id);
    });
});/*********** end delet data collect purchase ***********/

/*********** update data purchase ***********/
$("body").on('click','.update-collect-purchase',function(e){

        var dataId=$(this).attr("data-id");
        var tr_id="#tr_"+dataId;
        $(tr_id).removeClass('edit');
        var c_product =$(tr_id+" .c_product").val();
        var c_url =$(tr_id+" .c_url").val();
        var c_price=parseFloat($(tr_id+" .c_price").val());
        var c_properties=$(tr_id+" .c_properties").val();
        var c_quantity=parseInt($(tr_id+" .c_quantity").val());
        var total_price=parseFloat($("#total_price").val());
        var price_deduct=parseFloat($("#price_deduct").val());
        var tr='<td role="cell" class="c_product">'+c_product+'<span><input type="hidden" name="in_product" class="in_product" value="'+c_product+'"></td><td role="cell"><a href="'+c_url+'" target="_blank" class="c_url">رابط الموقع</a><input type="hidden" name="in_url" class="in_url" value="'+c_url+'"></td><td class="price" role="cell"><span class="c_price">'+c_price+'</span>$<input type="hidden" name="in_price" class="in_price" value="'+c_price+'"></td><td role="cell" class="c_quantity">'+c_quantity+'<input type="hidden" name="in_quantity" class="in_quantity" value="'+c_quantity+'"></td><td class="c_properties">'+c_properties+'<input type="hidden" name="in_properties" class="in_properties" value="'+c_properties+'"></td><td><button class="btn btn-edit" data-id="'+dataId+'"><i class="fa fa-pencil"></i>تعديل </button><button class="btn btn-delet" data-id="'+dataId+'"><i class="fa fa-trash-o"></i>حذف </button></td>';

        var exchange=parseFloat($("#exchange").val());
        total_price=total_price-price_deduct+(c_quantity*c_price);

        if(c_product && c_url && !isNaN(c_price) && c_properties && !isNaN(c_quantity)){

            $(this).parent().parent().html(tr);
            border_color_nn(tr_id);
            $("#total_price").val(total_price);
            $(".sum-purchase .price").html("");
            $(".sum-purchase .price").html(total_price+"$"+"/"+parseFloat(exchange*total_price).toFixed(2)+" د.ل");
            $(".sum-purchase").removeClass("hide");
        }else{
            controle_form(tr_id);
            is_float(tr_id);
            is_int(tr_id);
        }

});/*********** end update data purchase ***********/





/*******send data purchase**/
$(".send-data-purchase").on("click",function(e){
    e.preventDefault();

    var count = $('.purchase-body > table > tbody >tr').length;
    var someJSON = [];

    var storeName=$("#storeName").val();
    var storeUrl=$("#storeUrl").val();
    var comment=$('#comment').val();
    var InternalShippingPrice=parseFloat($("#InternalShippingPrice").val());
    var tax=parseFloat($("#tax").val());
    var shippingAddresses=$("#shippingAddresses").val();

    if(count>0 && storeName && storeUrl && !isNaN(InternalShippingPrice) && !isNaN(tax) && shippingAddresses){

        $(".loading").css({'display':'block'});
        $(".loading").removeClass("hide");
        border_color_nn("#purchase_order");

       // alert(count)

        for(i = 1; i <= count; i++){

            var c_product    = $("#tr_"+i).find(".in_product").val();
            var c_url        = $("#tr_"+i).find(".in_url").val();
            var c_price      = $("#tr_"+i).find('.in_price').val();
            var c_properties = $("#tr_"+i).find(".in_properties").val();
            var c_quantity   = $("#tr_"+i).find(".in_quantity").val();

            var data = {
                'storeName':storeName,
                'storeUrl':storeUrl,
                'comment':comment,
                'InternalShippingPrice':InternalShippingPrice,
                'tax':tax,
                'shippingAddresses':shippingAddresses,
                'c_product':c_product,
                'c_url':c_url,
                'c_price':c_price,
                'c_properties':c_properties,
                'c_quantity':c_quantity,
                'total_data'  :count,
            };

            someJSON.push(data);
        }
        $.ajax({
            type: "post",
            url: "addorder",
            data: {data_array: JSON.stringify(someJSON)},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(resp) {
                if(resp.success == true){
                    $(".loading").addClass("hide");
                    $(".order-success-msg").text(resp.message);
                    $(".order-errors-msg").addClass("hidde");
                    $(".order-success-msg").removeClass("hidde");
                    hide_forme("purchase_order");
                    $(".purchase-body table tbody").html("")
                   /* setTimeout(function() {
                        swal({
                            title: "شكرا لك",
                            text: "تم استقبال طلبك بنجاح وستتم مراجعته خلال ٤٨ ساعه",
                            type: "success",
                            confirmButtonText: "موافق",
                        }, function() {
                            window.location = "waiting"
                        });
                    });
                    */
                }else{
                    $(".loading").addClass("hide");
                    $(".order-errors-msg").text("المرجو اعادة المحاولة");
                    $(".order-success-msg").addClass("hidde");
                    $(".order-errors-msg").removeClass("hidde");
                }
            },
            error: function (xhr, status, errors) {
                    $(".loading").addClass("hide");
                    $(".order-errors-msg").text(xhr.errors);
                    $(".order-success-msg").addClass("hidde");
                    $(".order-errors-msg").removeClass("hidde");
              }
        });
    }else{
        controle_form("#purchase_order");
        is_float("#purchase_order");

    }
});
/*******end send data purchase**/


/** calculatrice ***/
$("body").on('click',".calculatorform .btn-calcule", function(e){
 e.preventDefault();

    var length=$("#length").val();
    var contry=$("#contry").val();
    var prd_price=$("#prd_price").val();
    var type_account=$("#type_account").val();
    var weight =$("#weight").val();
    var width =$("#width").val();
    var height =$("#height").val();

    if(length && prd_price && type_account && weight && height && width){

        $(".loading").css({'display':'block'});
        $(".loading").removeClass("hide");

        $.ajax({
            type:'post',
            url : '/calculator_calcule',
            data:$('#calculatorform').serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                $(".loading").addClass("hide");
            if(resp.success){
                $('.account_type').text(resp.account_type);
                $('.prd_price').text(resp.product_price);
                $('.actual_weight').text(resp.actual_weight);
                $('.volumetric_charge').text(resp.volumetric_charge);
                $('.price').text(resp.price);
               // $(".loading").addClass("hide");
               $('.price').css('color','#03b913');
                $(".resulte  table").removeClass("hide");

            }else{

                //alert("المرجو التأكد من جميع الحقول");
                $('.account_type').text(resp.account_type);
                $('.prd_price').text(resp.product_price);
                $('.actual_weight').text(resp.actual_weight);
                $('.volumetric_charge').text(resp.volumetric_charge);
                $('.price').text(resp.error);
                $('.price').css('color','red');
                $(".resulte  table").removeClass("hide");

            }

            },

        });
    }else{
       controle_form("#calculatorform");

    }
});
/*** end calculatrice ***/

        var country=$("#country").val();
        var prd_price=$("#prd_price").val();
        var type_account=$("#type_account").val();
        var weight =$("#weight").val();
        var width =$("#width").val();
        var height =$("#height").val();
        var length=$("#length").val();

/** control inpute **/
function controle_inpute_is_vide($id){
    var val=$($id).val();
    if(val!=''){
        $($id).removeClass("br-danger");
    }else{
        $($id).addClass("br-danger");
    }
};
/** control inpute **/
function controle_form($id){

    $($id+" input[required]").each(function(){
        $(this).css('border-color','');
        if(!$.trim($(this).val())){ //if this field is empty
            $(this).css('border-color','red'); //change border color to red

        }

    });
    $($id+" select[required]").each(function(){
        $(this).css('border-color','');
        if(!$.trim($(this).val())){ //if this field is empty
            $(this).css('border-color','red'); //change border color to red

        }

    });
    $($id+" textarea[required]").each(function(){
        $(this).css('border-color','');
        if(!$.trim($(this).val())){ //if this field is empty
            $(this).css('border-color','red'); //change border color to red

        }

    });
    return false;
};

 /**Forme Control */
 $(".input-control").blur(function(){
    controle_inpute_is_vide(this);
});


    /** view order **/
    //$(".table-orders .btn-eye").on('click', function(){
$("body").on('click','.table-orders .btn-eye',function(e){
    var id=parseInt($(this).val());

        $.ajax({
            type:'post',
            url : '/view-order',
            data:{'id':+id},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
            // $(".loading").addClass("hide");
                $(".itmesOrderModal .table-purchase tbody").html("");
                $(".itmesOrderModal .code-order").html("");
                $(".itmesOrderModal .table-purchase tbody").html(resp.body);
                $(".itmesOrderModal .code-order").html(resp.head);



            },

        });
    });

       /***** search Purchase *****/
       $(".purshase-search-form .serch-btn").on('click',function(e){
        e.preventDefault();
        var s=parseInt($(".purshase-search-form #search").val());

    if(s>0){
//var tr='<tr><td colspan="6" style="text-align: center"><img src="{{asset('assets/front/images/icon/Loading.gif')}}" width="60"></td></tr>'
        $.ajax({
            type:'post',
            url : '/purchase/search',
            data:{'search':+s},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                if(resp.success){
                    $(".table-orders tbody").html('');
                    $(".table-orders tbody").html(resp.html);
                }else{
                    $(".table-orders tbody").html('');
                    $(".table-orders tbody").html(resp.message);
                }

            },

        });

    }else{
        controle_form("#purshase-search-form");
        is_int("#purshase-search-form");
    }

    });

/*
    $('body').on('click','.payerPurchaseModal .purchase-colose', function(e){
        window.location.href = "/purchase";
    });
    */
$('body').on('click','.cancel-purchase', function(e){
    e.preventDefault();
    var val=$(this).val();
    $('#cancelPurchaseModal .cancel-purchase-now').val(val);
});
$('body').on('click','.cancel-purchase-now', function(e){
    e.preventDefault();
    var val=$(this).val();

    $(".modale-alert-message .alert-success").addClass('hidde');
    $(".modale-alert-message .alert-danger").addClass('hidde');
    $.ajax({
        type:'get',
        url : '/purchase/cancel_purchase',
        data:{'id':+val},
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success:function(resp){
            if(resp.success){
                $(".modale-alert-message .alert-success").text("");
                $(".modale-alert-message .alert-success").text(resp.message);
                $(".modale-alert-message .alert-success").removeClass('hidde');
                $(".modale-alert-message .alert-danger").addClass('hidde');
                setTimeout(function(){
                    location.reload();
                });
            }else{
                $(".modale-alert-message .alert-danger").text("");
                $(".modale-alert-message .alert-danger").text(resp.message);
                $(".modale-alert-message .alert-danger").removeClass('hidde');
                $(".modale-alert-message .alert-success").addClass('hidde');
            }

        },

    });

});

    $('body').on('click','.payer-purchase', function(e){
        e.preventDefault();
        var val=$(this).val();
        var id="#tr_"+val;
        var code=$(id+" .code").text();
        var purchase_price=$(id+" .purchase_price").text();
        $(".modale-alert-message .alert-success").addClass('hidde');
        $(".modale-alert-message .alert-danger").addClass('hidde');
        $('#payerPurchaseModal .purchase_code').val(code);
        $('#payerPurchaseModal .purchase_price').val(purchase_price);
        $('.payer-purchase-now').val(val);

    });
    $('body').on('click','.payer-purchase-now', function(e){
            e.preventDefault();
            var val=$(this).val();
            var tr="#tr_"+val;
            $(".modale-alert-message .alert-success").addClass('hidde');
            $(".modale-alert-message .alert-danger").addClass('hidde');
        $.ajax({
            type:'post',
            url : '/purchase/payer_purchase',
            data:{'id':+val},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                if(resp.success){

                    // $(".alert-success").prepand(resp.message);
                    $(".modale-alert-message .alert-success").text("");
                    $(".modale-alert-message .alert-success").text(resp.message);
                    //$('.modale-alert-message .solde').val(resp.solde)
                    $(".modale-alert-message .alert-success").removeClass('hidde');
                    $(".modale-alert-message .alert-danger").addClass('hidde');
                    $(tr+' .payer-purchase').hide();
                    $(tr+' .cancel-purchase').hide();
                    $(".payer-purchase-now").hide();
                    $(tr+' .status').text("قيد التنفيد");
                }else{

                    $(".modale-alert-message .alert-danger").text("");
                    $(".modale-alert-message .alert-danger").text(resp.message);
                    $(".modale-alert-message .alert-danger").removeClass('hidde');
                    $(".modale-alert-message .alert-success").addClass('hidde');
                }

            },

        });

    });
/*
$('body').on('click','.btn-payer-purchase', function(e){
    e.preventDefault();
    var val=$(this).val();
    $(".modale-alert-message .alert-success").addClass('hidde');
    $(".modale-alert-message .alert-danger").addClass('hidde');
    $.ajax({
        type:'post',
        url : '/purchase/payer_purchase',
        data:{'id':+val},
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success:function(resp){

            if(resp.success){

                // $(".alert-success").prepand(resp.message);
                $(".modale-alert-message .alert-success").text("");
                $(".modale-alert-message .alert-success").text(resp.message);
                //$('.modale-alert-message .solde').val(resp.solde)
                $(".modale-alert-message .alert-success").removeClass('hidde');
                $(".modale-alert-message .alert-danger").addClass('hidde');

             }else{

                 $(".modale-alert-message .alert-danger").text("");
                 $(".modale-alert-message .alert-danger").text(resp.message);
                 $(".modale-alert-message .alert-danger").removeClass('hidde');
                 $(".modale-alert-message .alert-success").addClass('hidde');
             }



        },

    });

});
*/
/** upgrade plansaving **/


//$('body').onchange('.payerPurchaseModal .plan_date', function(){
$('.plan_duration').change(function(e){
    e.preventDefault();
    var plan_duration=$(this).val();

    if(plan_duration>0){
        $.ajax({
            type:'post',
            url : '/plan/plan_duration_price',
            data:{'plan_duration':+plan_duration},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                if(resp.success){
                    $(".price_payer").val(resp.duration_price);
                  //  $(".table-purchase tbody").html(resp.body);
                }else{
                  //  $(".table-orders tbody").html('');
                  //  $(".table-orders tbody").html(resp.message);
                }

            },

        });
    }else{
        $(".price_payer").val("");
    }

});

$('body').on('click','.btn-pricing', function(){
    var plan=$(this).val();
        $('.btn-upgrade-plan').val(plan);
        if(plan==0){

            $('#plan_duration option:first').prop('selected',true);
            $("#plan_duration").attr("disabled","disabled");
            $("#plan_duration").removeAttr("required");
            $('.price_payer').val("");
        }else{

            $('#plan_duration').val("");
            $("#plan_duration").attr("required","required");
            $("#plan_duration").removeAttr("disabled");
        }
});

$('body').on('click','.btn-upgrade-plan', function(e){
    e.preventDefault();
    var val=$(this).val();
    var plan_duration=$('#plan_duration').val();
    $(".modale-alert-message .alert-success").addClass('hidde');
    $(".modale-alert-message .alert-danger").addClass('hidde');

   if(plan_duration>0){
        $.ajax({
            type:'post',
            url : '/plan/upgrade',
            data:{'plan':+val,'plan_duration':+plan_duration},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){

                if(resp.success){

                   // $(".alert-success").prepand(resp.message);
                   $(".modale-alert-message .alert-success p").text("");
                   $(".modale-alert-message .alert-success p").text(resp.message);
                   $('.modale-alert-message .solde').val(resp.solde)
                   $(".modale-alert-message .alert-success").removeClass('hidde');
                   $(".modale-alert-message .alert-danger").addClass('hidde');
                    setTimeout(function(){
                        window.location.href = "/plan";
                    });
                }else{

                    $(".modale-alert-message .alert-danger p").text("");
                    $(".modale-alert-message .alert-danger p").text(resp.message);
                    $(".modale-alert-message .alert-danger").removeClass('hidde');
                    $(".modale-alert-message .alert-success").addClass('hidde');
                }
                   // $(".modal-body").html("");

            },

        });
   }else{

    controle_form("#upadetPlanModal");
   }


});

$('body').on('click','.btn-upgrade-plan-free', function(e){
    e.preventDefault();


        $.ajax({
            type:'post',
            url : '/plan/upgrade',
            data:{'plan':0,'plan_duration':0},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){

                if(resp.success){

                    // $(".alert-success").prepand(resp.message);
                    $(".alert-message .alert-success p").text("");
                    $(".alert-message .alert-success p").text(resp.message);
                    $(".alert-message .alert-success").removeClass('hidde');
                    $(".alert-message .alert-danger").addClass('hidde');
                    setTimeout(function(){
                        window.location.href = "/plan";
                    });
                }else{

                    $(".alert-message .alert-danger p").text("");
                    $(".alert-message .alert-danger p").text(resp.message);
                    $(".alert-message .alert-danger").removeClass('hidde');
                    $(".alert-message .alert-success").addClass('hidde');
                }
                   // $(".modal-body").html("");

            },

        });



});

$('#fileUpload').on('change',function(){
    $('.avatar').removeClass('open');
  });
  $('.avatar').on('click',function(){

    $(this).addClass('open');
  });
  // added code to close the modal if you click outside
  $('html').click(function() {
   $('.avatar').removeClass('open');
  });

  $('.avatar').click(function(event){
      event.stopPropagation();
  });

  $(function(){
    $('#thumbnail').on('change', function() {
        var file = $(this).get(0).files;
        var reader = new FileReader();
        reader.readAsDataURL(file[0]);
        reader.addEventListener("load", function(e) {
        var image = e.target.result;
    $("#imgthumbnail").attr('src', image);
    });
    });
    });
$('.edit-profile .upload-avatar').click(function(){
    $('#thumbnail').trigger('click');
})


$(".verifierProfileModale .btn-verifier-profile").click(function(e){
    e.preventDefault();

    $(".verifierProfileModale .alert-success").addClass('hidde');
    $(".verifierProfileModale .alert-danger").addClass('hidde');

    var form = document.querySelector('#form_verifier_profile')
    var formData  = new FormData(form);
    var file = $('#verifier_profile')[0].files[0];
    formData.append('verifier_profile',file);

    $.ajax({
        url: '/verifier_profile',
        type: 'post',
        data: formData ,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(resp){


            if(resp.success){
                $(".verifierProfileModale .alert-danger").addClass('hidde');
                $(".verifierProfileModale .alert-success").removeClass('hidde');
                $(".verifierProfileModale .alert-success").append(resp.msg);
                setTimeout(function(){
                    window.location.href = "/account";
                });

            }else{
                $(".verifierProfileModale .alert-success").addClass('hidde');
                $(".verifierProfileModale .alert-danger").removeClass('hidde');
                $(".verifierProfileModale .alert-danger").append(resp.msg);
            }
        /*    if(response != 0){
                $("#img").attr("src",response);
                $(".preview img").show(); // Display image element
            }else{
                alert('file not uploaded');
            }
*/
        },
    });
});



$(".allow_numeric").on("input", function(evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57))
    {
      evt.preventDefault();
    }
});


});
/**end Jquery */




function is_float($idform){

    $($idform+" .float").each(function(){
        var val=parseFloat($.trim($(this).val()));
        $(this).css('border-color','');
        if(isNaN(val) || val<0){ //if this field is empty
            $(this).css('border-color','red'); //change border color to red

        }

    });

}

function is_int($idform){

    $($idform+" .int").each(function(){
        var val=parseInt($.trim($(this).val()));
        $(this).css('border-color','');
        if(isNaN(val) || val<0){ //if this field is empty
            $(this).css('border-color','red'); //change border color to red
        }
    });
}

function border_color_nn($idform){

    $($idform+" input[type='text']").each(function(){
        $(this).css('border-color','');
    });
    $($idform+" select").each(function(){
        $(this).css('border-color','');
    });

}












function hide_forme(id){
    $("#"+id+" input[type='text']").val("");
    $("#"+id+" textarea").val("");
    $("#"+id+" select option:first-child").attr("selected", "selected");
}













