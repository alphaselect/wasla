<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Product_image;
class ProductController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }


    public function list(){
        $products = Product::get();
        return view('control.product.index',compact('products'));
    }

    public function create(){
        return view('control.product.create');
    }

    public function store(Request $request){
        $data = $request->all();

        $product = Product::create($data);

        $files = $request->file('product_image');
        //dd($files);
        foreach($files as $file){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/product');
            $file->move($destinationPath, $name);
            Product_image::create([
                'product_image'     =>  $name,
                'product_id'        => $product->id
            ]);
        }

        return back()->with('success_add','تمت اضافة المنتج لحساب العميل بنجاح');
    }

    public function edit(Request $request){
        $product = Product::where('id',$request->id)->first();
        return view('control.product.edit',compact('product'));
    }
    public function update(Request $request){
        $data = $request->all();
        $product = Product::findOrFail($data['product_id']);
        $product->fill($data)->save();

        $files = $request->file('product_image');
        if($files){
            Product_image::where('product_id',$data['product_id'])->delete();
            foreach($files as $file){
                $name = time().'-'.$file->getClientOriginalName();
                $destinationPath = public_path('upload/product');
                $file->move($destinationPath, $name);
                Product_image::create([
                    'product_image'     =>  $name,
                    'product_id'        => $data['product_id']
                ]);
            }
        }

        return back()->with('success_add','تمت تحديث بيانات المنتج بنجاح ');
    }

    public function delete(Request $request){
        $product = Product::where('id',$request->id)->update(['product_status' => 5]);
        return back();
    }
    public function backup(Request $request){
        $product = Product::where('id',$request->id)->update(['product_status' => 0]);
        return back();
    }
}

