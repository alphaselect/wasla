<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
class SettingController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function general(){
        $general = Setting::get();
        return view('control.settings.general',compact('general'));
    }
}
