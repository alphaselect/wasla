@extends('layouts.account')
@section('content')
<section>
    <div class="chipment">
        <div class="chipment-table">
            <div class="new-purchase">
                <a href="{{url('neworder')}}" class="btn-plus">
                    &nbsp;<i class="fa fa-plus"></i>&nbsp;
                    <span class="btnplus btnplus">طلبيات جديدة</span>
                </a>
            </div>
            <div class="clear"></div>
            <div class="serche-form">
                <form class="form-inline purshase-search-form" id="purshase-search-form" method="post" action="{{url('/purchase/search')}}" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-10 col-xs-8">
                            <input type="text" class="form-control int " id="search" name="search" placeholder="البحث عن طلب شراء رقم ..." required>
                        </div>
                        <div class="div-btn col-md-2 col-xs-4">
                            <button class="btn serch-btn" type="button"><i class="fa fa-search"></i></button>
                           <!-- <button class="btn filter-btn" type="button"><i class="fa fa-filter"></i></button>-->
                        </div>
                    </div>
                    @csrf
                </form>
                <div class="clear"></div>
            </div>
            <div class="table-purchases">
                <table role="table" class="table table-orders table-responsive">
                        <thead role="rowgroup" border-spacin="10">
                            <tr role="row">
                            <th >رقم الطلب</th>
                            <th>سعر الطلب</th>
                            <th>الموقع</th>
                            <th>ملاحظة</th>
                            <th>الحالة</th>
                            <th>اخر تحديث</th>
                            <th style="width:10%">-</th>
                            </tr>
                        </thead>
                        <tbody role="rowgroup">
                            @foreach($orders as $order)
                            <tr role="row" id="tr_{{$order->id}}">
                                <td role="cell" class="code">{{$order->code()}}</td>
                                <td role="cell" class="purchase_price">{{ round_up(get_pricePurchaseTotale($order),2) }}$ / {{ round_up((get_pricePurchaseTotale($order) * get_exchange()),2) }}د.ل
                                </td>
                                <td role="cell"><a href="{{ $order->store_url }}" target="_blank" class="c_url">رابط الموقع</a></td>
                                <td role="cell">{{ $order->comment }} &nbsp;</td>
                                <td role="cell" class="status"> <span class="status-color-{{$order->status}}">{{$order->status()}}</span></td>
                                <td role="cell">{{forma_date_dmy($order->updated_at)}}</td>
                                <td>
                                    <button class="btn btn-info btn-eye btn-sm btn-width" data-toggle="modal" value="{{$order->id}}" data-target="#itmesOrderModal">
                                        تفاصيل
                                    </button>
                                    @if($order->status==1)
                                        <button type="button" class="btn  btn-success payer-purchase btn-sm btn-width" value="{{$order->id}}" data-toggle="modal" data-target="#payerPurchaseModal"> الدفع الان </button>
                                        <button type="button" class="btn  btn-danger cancel-purchase btn-sm btn-width" value="{{$order->id}}"  data-toggle="modal" data-target="#cancelPurchaseModal"> الغاء الطلب</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                </table>
                <div class="paginate">{{$orders->render()}}</div>
            </div>
        </div>
    </div>
</section>
@endsection('content')
