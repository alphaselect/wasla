<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\Shipping;
use App\ShipAdress;
use App\Service;
use App\Purchase;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(255);

        view()->composer(['account.shipping.newshipment','account.shipping.ready','account.shipping.delivered'],function($view){
            $item1 = Shipping::where('user_id',Auth::user()->id)->whereIn('status',[0,1,5])->count();
            $item2 = Shipping::where('user_id',Auth::user()->id)->where('status',2)->count();
            $item3 = Shipping::where('user_id',Auth::user()->id)->where('status',3)->count();
            $view->with(compact('item1','item2','item3'));
        });

        view()->composer(['layouts.admin'],function($view){

            $item1 = Shipping::whereIn('status',[0,5])->count();
            $item2 = Shipping::where('status',1)->count();
            $item3 = Shipping::where('status',2)->count();
            $item4 = Shipping::where('status',3)->count();

            $service  = Service::where('status',0)->count();

            //purchase
            $order1 = Purchase::where('status',0)->count();
            $order2 = Purchase::where('status',1)->count();
            $order3 = Purchase::where('status',2)->count();
            $order4 = Purchase::where('status',3)->count();
            $order5 = Purchase::where('status',4)->count();

            $view->with(compact('item1','item2','item3','item4','service','order1','order2','order3','order4','order5'));
        });

        view()->composer(['account.purchase.neworder'],function($view){

            $freightaddress = ShipAdress::where('status',0)->where('type_address','freight')->get();
            $view->with(compact('freightaddress'));

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
