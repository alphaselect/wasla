<!DOCTYPE html>
<html dir="rtl" lang="ar"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
  </head>
  <body>
    <table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 394px;">
            <table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                    <td style="  height: 78px;" align="center">
                        <img src="https://global-link.host/assets/front/images/logo.png">
                    </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif; text-align: center;">&nbsp;</td>
                </tr>
                <tr style="background-color: #499491;">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 20px;">
                    <table class="devicewidth" width="648" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 15px; color: #000; font-family: Arial, Helvetica, sans-serif;">
                            أهلاً بك سيد/سيدة @if($resp) {{ $resp->firstname }} @endif
                            </span>
                            </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;">
                              <span style="font-size: 15px; color: #000; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                                {{ $content }}
                              </span>
                            </td>
                        </tr>
                        <tr style="height: 1px;">
                            <td style="font-family: Arial, Helvetica, sans-serif; height: 1px;font-size: 15px;">
                              <p> إذا كانت لديك أي أسئلة ، فما عليك سوى الرد على هذه الرسالة الإلكترونية - يسرنا دائما تقديم المساعدة
                                <span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;"></span>
                               </p>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; height: 20px;" height="10">---------------------------------------</td>
        </tr>
      </tbody>
    </table>
<table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: rtl !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">
  جميع الحقوق محفوظة لشركة وصلة العالمية 2022- 2020
</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p></p>
</body></html>
