<!DOCTYPE html>
<html dir="rtl" lang="ar">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title></title>
		</head>
		<body>
			<table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
				<tbody >
					<tr >
						<td style="  height: 394px;">
							<table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
								<tbody >
									<tr></tr>
									<tr >
										<td style="  height: 78px;" align="center">
											<img src="https://global-link.host/assets/front/images/logo.png">
										</td>
										</tr>
										<tr>
											<td style="text-align: center;">&nbsp;</td>
										</tr>
										<tr style="background-color: #499491;">
											<td >
												<p>
													<span style="color: #595959;   font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span>
												</p>
											</td>
										</tr>
										<tr>
											<td >
												<br>
												</td>
											</tr>
											<tr>
												<td>
													<br>
													</td>
												</tr>
												<tr >
													<td style="  height: 20px;">
														<table class="devicewidth" width="648" cellspacing="0" cellpadding="0" border="0" align="center">
															<tbody>
																<tr>
																	<td>
																		<span style="font-size: 15px; color: #000;  ">
                                                                            أهلاً بك سيد/سيدة {{ $content->firstname }}
                                                                        </span>
																	</td>
																</tr>
																<tr >
																	<td  height="15">
																		<br>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<span style="font-size: 15px; color: #000; line-height: 24px;  ">
																				للتحقق من حسابك، الرجاء النقر على الزر أدناه:
                                                                            </span>
																		</td>
																	</tr>
																	<tr>
																		<td bgcolor="#ffffff" align="left">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td bgcolor="#ffffff" align="center" style="padding: 20px 30px 60px 30px;">
																						<table border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="center" style="border-radius: 3px;" bgcolor="#499491">
																									<a href="{{ url('/verify') }}/{{ $content->verifyToken }}" target="_blank"
																										style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #499491; display: inline-block;">تفعيل الحساب</a>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	  </tr> <!-- COPY -->
																	  <tr>
																		<td>
																			<p style="font-size: 16px; color: #000;">إذا لم يعمل ذلك ، انسخ الرابط التالي والصقه في متصفحك:</p>
																		</td>
																	  </tr> <!-- COPY -->
																	  <tr>
																		<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
																			<p style="margin: 0;">
																				<a href="{{ url('/verify') }}/{{ $content->verifyToken }}" target="_blank" style="color: #FFA73B;">
																				  {{ url('/verify') }}/{{ $content->verifyToken }}
																				</a>
																			</p>
																		</td>
																	</tr>
																	<tr style="height: 1px;">
																		<td style="height: 1px;">
																			<p> إذا كانت لديك أي أسئلة ، فما عليك سوى الرد على هذه الرسالة الإلكترونية - يسرنا دائما تقديم المساعدة
																				<span style="font-size: 16px; color: #595959; line-height: 24px;  "></span>
																			</p>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td style="text-align: center; height: 20px;" height="10">---------------------------------------</td>
									</tr>
								</tbody>
							</table>
							<table id="backgroundTable" style="  border-collapse: collapse; direction: rtl !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
								<tbody>
									<tr>
										<td >
											<table class="devicewidth" style="  border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
												<tbody>
													<tr >
														<td style="text-align: center;">
															<table class="devicewidth" style="color: #000000;   font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
																<tbody >
																	<tr >
																		<td style="text-align: center;  ">
																			<table class="devicewidth" style="color: #000000;   font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
																				<tbody >
																					<tr >
																						<td style="text-align: center;  ">
																							<span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center;  ">
                                                                                                جميع الحقوق محفوظة لشركة وصلة العالمية 2022- 2020
                                                                                            </span>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<p></p>
		</body>
	</html>
