@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> وسائل الدفع </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form method="POST" action="{{ url('control/modepay')}}" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-6">

                    </div>
                    <div class="col-lg-6">

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-info btn-block">تحديث المعلومات</button>
                        </div>
                    </div>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection
