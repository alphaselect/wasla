@extends('layouts.account')
@section('content')
<section>
    <div class=" " id="purchase_order">
        <div class="purchase-table">
            <ul class="head-table">
                <li>المنتج </li>
                <li>رابط المنتج</li>
                <li>السعر</li>
                <li>الكمية</li>
                <li>الخصائص</li>
            </ul>
            <div class="purchase-body">
                <table role="table" class="table table-responsive purchase_order">
                    <thead role="rowgroup" border-spacin="10">
                        <tr role="row">
                        <th role="columnheader">المنتج </th>
                        <th role="columnheader">رابط المنتج</th>
                        <th role="columnheader">السعر</th>
                        <th role="columnheader">الكمية</th>
                        <th role="columnheader">الخصائص</th>
                        <th></th>

                        </tr>
                    </thead>

                    <tbody role="rowgroup">



                    </tbody>

                </table>
                <div class="clear"></div>
                <div class="purchase-bottom" id="add_colect_purchase">
                    <form class="form" method="post" action="{{url('/addpurchase')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="c_product" name="c_product" placeholder="المنتج" required>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="c_url" name="c_url" placeholder="رابط المنتج" required>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="text" class="form-control float" id="c_price" name="c_price" placeholder="السعر" required>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="text" class="form-control int" id="c_quantity" name="c_quantity" placeholder="الكمية" required>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="c_properties" name="c_properties" placeholder="الخصائص">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-front-full collect-purchase" style="withd">اضف عنصر</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="sum-purchase hide">
                    السعر الإجمالي
                    <span class="price" style="color: rgb(3, 185, 19);">
                        0 $
                    </span>
                </div>

            </div>


        </div>
        <form class="form" method="post" action="{{url('user/addorder')}}" enctype="multipart/form-data">

            <div class="new-purchase newpurchase">
                <h3>طلبية جديدة</h3>
                    <div class="row">

                        <div class="col-sm-12">
                        <p class="bg-danger order-errors-msg hidde"></p>
                        <p class="bg-success order-success-msg hidde"></p>
                            <div class="form-group">
                                <label for="store" class="">اسم المتجر</label>
                                <input type="text" class="form-control" placeholder="اسم المتجر" id="storeName" name="storeName" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="storeUrl" class="">رابط المتجر</label>
                                <input type="text" class="form-control" placeholder="رابط المتجر" id="storeUrl" name="storeUrl" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="comment" class="">ملاحظة</label>
                                <textarea class="form-control" rows="4" id="comment" name="comment" placeholder="ملاحظة"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="InternalShippingPrice" class="">سعر الشحن الداخلي</label>
                                <input type="text" class="form-control float" id="InternalShippingPrice" placeholder="سعر الشحن الداخلي" name="InternalShippingPrice" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="tax" class="">الضريبة</label>
                                <input type="text" class="form-control float" placeholder="الضريبة" id="tax" name="tax" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="hippingAddresses" class="">عناوين الشحن</label>
                                <select type="text" class="form-control" id="shippingAddresses" name="shippingAddresses" required>
                                    <option value=""> اختر المستودع </option>
                                    @if(count($freightaddress)>0)
                                        @foreach ($freightaddress as $freight)
                                            <option value="{{ $freight->id }}">
                                                @if($freight->country == 'usa')
                                                    مستودع امريكي
                                                @endif
                                            </option>
                                        @endforeach
                                    @endif
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="form-group row-btn">
                    <input type="hidden" id="order_id" class="order_id" value="0">
                    <input type="hidden" id="total_data" class="total_data" value="0">
                    <input type="hidden" name="total_price" id="total_price" value="0">
                    <!--<input type="hidden" name="taux_purchase" id="taux_purchase" value="{{get_taux_purchase()}}">-->
                    <input type="hidden" name="exchange" id="exchange" value="{{get_exchange()}}">
                    <input type="hidden" name="price_deduct" id="price_deduct" value="0">

                    @csrf
                     <button type="button" class="btn btn-front send-data-purchase"> إضافة الطلب </button>
                    </div>

            </div>
        </form>
    </div>




</section>
@endsection('content')
