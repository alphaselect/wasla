<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipAdress extends Model
{
    //

    protected $fillable = [
        'type_address','Adressline' , 'city' , 'state' , 'zipcode' , 'country' , 'mobile' , 'status'
        //'country','fulladdress' , 'status'
    ];
}
