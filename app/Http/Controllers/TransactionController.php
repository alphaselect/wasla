<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Purchase;
use App\ChildPurchase;
use App\Payment;
use App\Shipping;
use Carbon\Carbon;
use Response;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function transaction(){
        $transactions  = Payment::where('user_id',Auth::user()->id)->orderByDesc('id')->paginate(50);
        return view('account.transaction.index',compact('transactions'));
    }

    public function addfunds(){
        return view('account.transaction.addfunds');
    }

    /** Totale price Payer  ***/
    public function payer_purchase(Request $request){
        $orderid    = $request->id;
        $fundsTotal = get_solde();
        $order = Purchase::where('user_id',Auth::user()->id)->where('id',$orderid)->first();
        $order_price = round_up((get_pricePurchaseTotale($order) * get_exchange()),2);
       if($fundsTotal >= $order_price){

        $funds_total = $fundsTotal - $order_price;

            $payment = Payment::create([
                'user_id'     => Auth::user()->id,
                'operations'  => 'دفع مصاريف الطلب '.$order->code(),
                'invoice'     => generateRandomString(11),
                'funds_add'   => -$order_price,
                'funds_total' => $funds_total,
                'modpayment'  => 'رصيد المحفظة',
                'status'      => 0,
            ]);

            Payment::where('user_id',Auth::user()->id)->where('id','!=',$payment->id)->update([
                'status'     => 1,
                'updated_at' => Now()
            ]);

            Purchase::where('user_id',Auth::user()->id)->where('id',$orderid)->update([
                'status'     => 2,
                'updated_at'    => Now()
            ]);

            ChildPurchase::where('purchase_id',$orderid)->update(
                array(
                    'status'  => 2,
                    'updated_at' => Now()
                )
            );

            return Response::json(array(
                'success'   => true,
                'message'      => "تمت عملية الدفع بنجاح .",
            ),200);

       }else{

            return Response::json(array(
                'success'   => false,
                'message'      => "رصيد حسابك غير كافي لاجراء عملية الدفع المطلوبة",
            ),200);
        }
    }

    public function plan_duration_price(Request $request){

        $data = $request->all();
        $duration=(int)$data['plan_duration'];

        $price_plan=100;
        $duration_price=get_exchange()*$price_plan*$duration;

        return Response::json(array(
            'success'   => true,
            'duration_price'      => $duration_price,

            ),200);


    }

    public function plan_upgrade(Request $request){

        $data = $request->all();
        $plan=(int)$data['plan'];
        $plan_duration=(int)$data['plan_duration'];
        $user_date_end=Auth::user()->date_end;
        $plan_curent=Auth::user()->plan;
        $user_id=Auth::user()->id;
        $created_at=Carbon::now();
        $date=date('Y-m-d');
        $solde= get_solde();
        $plan_price=100;

    // dd(Payment::latest('id')->first()->id);

        if($plan==0){
            if($user_date_end<=$date){
                Auth::user()->update([
                    'plan'=>0,
                    'updated_at'=>$date
                ]);
                return Response::json(array(
                'success'   => true,
                'message'      => "لقد تم ترقية حسابك الى حساب مجاني",
                ),200);
            }else{
                return Response::json(array(
                    'success'   => false,
                    'message'      => "لم تنتهي مدة ترقية حسابك بعد",
                    ),200);
            }


        }elseif($plan==1){

            $funds_add=$plan_price*$plan_duration;

            if($solde >= $funds_add && $plan_duration>0){

                if($user_date_end>$date){
                    $end_date= Carbon::createFromFormat('Y-m-d', $user_date_end)->addYear($plan_duration);
                }else{
                    $end_date= Carbon::createFromFormat('Y-m-d', $date)->addYear($plan_duration);
                }

                $funds_total = $solde-$funds_add;
                $invoice=generateRandomNumber(11).''.Payment::latest('id')->first()->id;

                    Auth::user()->update([
                        'plan'       => 1,
                        'date_start' => $date,
                        'date_end'   => $end_date,
                        'updated_at' => Now()
                    ]);

                    Payment::where('user_id',$user_id)->where('status',0)->update([
                        'status' => 1
                    ]);
                    Payment::create([
                        'user_id'=>$user_id,
                        'operations'=>'ترقية الحساب ',
                        'invoice'=>$invoice,
                        'funds_add'=>-$funds_add,
                        'funds_total'=>$funds_total,
                        'modpayment'=>'رصيد المحفظة',
                        'status'=>0,
                    ]);
                    return Response::json(array(
                        'success'   => true,
                        'message'      => "لقد تم ترقية حسابك",
                        'solde'      =>$funds_total*get_exchange(),
                        ),200);
                }else{

                    return Response::json(array(
                        'success'   => false,
                        'message'      => "رصيد حسابك غير كافي لاجراء عملية الدفع المطلوبة",
                        ),200);
                }
            }else{
                return Response::json(array(
                    'success'   => false,
                    'message'      => "المرجو اعادة المحاولة لاحقا",
                    ),200);
            }

    }

    public function paynowshipping(Request $request){
        $idparcel = $request->idparcel;
        $fundsTotal = get_solde();
        $parcel     = Shipping::where('user_id',Auth::user()->id)->where('id',$idparcel)->first();
        $priceTopay = number_format($parcel->pricetopay * get_exchange(),2);
        if(Auth::user()->plan == 1){
            $price_Service = count($parcel->getCountService) * 15;
        }else{
            $price_Service = count($parcel->getCountService) * 25;
        }
        $global = $priceTopay  + $price_Service;
        if($fundsTotal >= $global && $global>0){

            $funds_total = $fundsTotal - $global;

            $payment = Payment::create([
                'user_id'     => Auth::user()->id,
                'operations'  => 'دفع مصاريف الشحنة '.$parcel->parcelcode,
                'invoice'     => generateRandomString(11),
                'funds_add'   => -$global,
                'funds_total' => $funds_total,
                'modpayment'  => 'رصيد المحفظة',
                'status'      => 0,
            ]);

            Payment::where('user_id',Auth::user()->id)->where('id','!=',$payment->id)->update([
                'status'     => 1,
                'updated_at' => Now()
            ]);

            Shipping::where('user_id',Auth::user()->id)->where('id',$idparcel)->update([
                'statuspay'     => 1,
                'updated_at'    => Now()
            ]);

            return Response::json(array(
                'success'   => true,
                'message'      => "تمت عملية الدفع بنجاح .",
            ),200);

        }else{
            return Response::json(array(
                'success'   => false,
                'message'      => " رصيد حسابك غير كافي لاجراء عملية الدفع المطلوبة",
            ),200);
        }
    }



}
