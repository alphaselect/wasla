@extends('layouts.admin')
@section('content')
<style> .msg_parcel{ width:100%; padding:10px; margin-bottom:10px; } .toshow{ display:none; } </style>
	<div class="layout-content">
        <div class="layout-content-body">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header"> رفع المنتجات ... </h2>
				</div>
			</div>
			<div class="row gutter-xs">
            @if ($message = Session::get('success_import'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="imports" id="myform" method="POST" action="{{ url('control/add_import')}}" enctype="multipart/form-data">
				<div class="btn-group col-lg-6" id="status" data-toggle="buttons">
					<!--<label class="btn btn-default btn-on active">
					<input type="radio" value="-1" name="identity" class="identityvalue" checked="checked"> هوية معروفة</label>
					<label class="btn btn-default btn-off">
					<input type="radio" value="-2" name="identity" class="identityvalue" >هوية مجهولة </label>-->
				</div>
				<div class="btn-group col-lg-6" id="status" data-toggle="buttons">
					<label class="btn btn-default btn-on active">
					<input type="radio" value="1" name="unity" checked="checked">Kg</label>
					<label class="btn btn-default btn-off">
					<input type="radio" value="2" name="unity">Lbs</label>
                </div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>رقم العميل </label>
                        <input type="text" name="codeclient" placeholder="رقم العميل" required="true" id="codeclient" class="form-control txt-auto codeclient" >
                        <input type="hidden" name="user_id" id="user_id" value="" required="true">
                    </div>
                    <div class="form-group">
						<label> كود الشحنة </label>
						<input type="text" name="parcelcode" placeholder="كود الشحنة" class="form-control" required="true" >
					</div>
                    <div class="form-group tohide">
                        <label> الاسم الكامل </label>
                        <input type="text" name="fullname" placeholder="الاسم الكامل" id="fullname" class="form-control txt-auto" readonly>
                    </div>
                    <!--<div class="form-group toshow">
                        <label>الاسم الشخصي  </label>
                        <input type="text" name="firstname" placeholder="الاسم الشخصي" id="firstname" class="form-control">
                    </div>-->
					<div class="form-group">
						<label>وزن الشحنة</label>
						<input type="text" name="weight" placeholder="وزن الشحنة" class="form-control" required="true" >
                    </div>
                    @csrf
					<div class="form-group">
						<label>الطول</label>
							<input type="text" name="width" placeholder="الطول" class="form-control" required="true">
					</div>
					<div class="form-group">
						<label>الارتفاع</label>
						<input type="text" name="height" placeholder="الارتفاع" class="form-control" required="true">
                    </div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>رقم التتبع </label>
						<input type="text" name="tracking" placeholder="رقم التتبع" class="form-control" required="true" >
                    </div>
                    <div class="form-group">
						<label> موقع الشحنة  </label>
						<select class="form-control" name="shipcountry" required="true">
							<option value="usa">أمريكا</option>
							<option value="cn">الصين</option>
                            <option value="uk">بريطانيا</option>
                            <option value="gb">المانيا</option>
						</select>
                    </div>
                    <div class="form-group">
                        <label> البريد الالكتروني</label>
                        <input type="text" name="email"  id="email" class="form-control txt-auto" readonly>
                    </div>
                    <!--<div class="form-group tohide">
                        <label>رقم الرحلة  </label>
                        <input type="text" name="shipnumber" placeholder="رقم الرحلة" id="shipnumber" class="form-control" >
                    </div>
                    <div class="form-group toshow">
                        <label>الاسم العائلي  </label>
                        <input type="text" name="lastname" placeholder="الاسم العائلي" id="lastname" class="form-control">
                    </div>-->
					<!--<div class="form-group">
						<label>تاريخ الوصول </label>
						<div class="input-group date">
						  <input name="date_arrival" placeholder="تاريخ الوصول" class="form-control datetopickup" type="text" required="true">
						  <span class="input-group-btn">
							<button id="demo-datepicker-2-btn" class="btn btn-primary" type="button">
							  <span class="icon icon-calendar"></span>
							</button>
						  </span>
						</div>
					</div>-->
					<div class="form-group">
						<label>العرض</label>
						<input type="text" name="lenght" placeholder="الطول" class="form-control" required="true">
					</div>
					<div class="form-group">
						<label>صور الشحنة</label>
                        <input type="file" name="image_file[]"  class="form-control" multiple required="true">
					</div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-2 text-center"></div>
                    <div class="col-lg-8 text-center">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">رفع الشحنة</button>
                    </div>
                </div>
			</form>
			</div>
		</div>
	</div>
@endsection
