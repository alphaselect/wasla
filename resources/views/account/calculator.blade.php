@extends('layouts.account')
@section('content')
<section>
<h1 class="h-front"> حاسبة الاسعار </h1>
<div class="mycompt calculator">
    <form action="{{url('/calculator')}}" id="calculatorform" method="post">
        <div class="calculatorform">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="country" name="country">
                            <option value="0">دولة الشحن</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="prd_price" name="prd_price" placeholder="سعر المنتج" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="weight" name="weight" placeholder="وزن الشحنة بالكلوغرام" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="width" name="width" placeholder="العرض" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control input-control" id="type_account" name="type_account" required>
                            <option value=""> نوع الحساب  </option>
                            <option value="0"> اشتراك مجاني </option>
                            <option value="1"> اشتراك توفير </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="length" name="length" placeholder="الطول" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="height" name="height" placeholder="الأرتفاع" required>
                    </div>
                </div>
            </div>
            @csrf
            <div class="b text-center">
                <button type="button" class="btn btn-front btn-calcule" style="float: none;">
                    حساب تكلفة الشحن
                </button>
            </div>
        </div>
    </form>
    <div class="resulte ">
        <div class="table-responsive col-sm-12">
            <table class="table table-bordered hide">
                <thead>
                    <tr>
                    <th >نوع الحساب</th>
                    <th >سعر المنتج</th>
                    <th >الوزن الفعلي</th>
                    <th >الوزن الحجمي</th>
                    <th ><strong>تكلفة الشحن</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td class="account_type"></td>
                        <td class="prd_price"></td>
                        <td class="actual_weight"></td>
                        <td class="volumetric_charge"></td>
                        <td class="price"></td>
                        <!--<span class="price"></span>-->
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="clear"></div>
    </div>
</div>
</section>
@endsection('content')
