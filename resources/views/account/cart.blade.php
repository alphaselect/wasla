@extends('layouts.account')
@section('content')
<section>
    <h1 class="h-front"> سلة التسوق  </h1>
    <div class="mycompt">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>اسم المنتج </th>
                    <th>الكمية </th>
                    <th class="text-center">السعر </th>
                    <th class="text-center">المجموع </th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                @foreach(Cart::instance('wasla')->content() as $item)
                <tr>
                    <td class="col-sm-8 col-md-6">
                    <div class="media">
                        <div class="media-body textalign">
                            <h4 class="media-heading"><a href="#">{{ $item->name }}</a></h4>
                        </div>
                    </div></td>
                    <td class="col-sm-1 col-md-1" style="text-align: center">
                    <input type="email" class="form-control" id="exampleInputEmail1" value="{{ $item->qty }}">
                    </td>
                    <td class="col-sm-1 col-md-1 text-center"><strong>{{ $item->price}} د.ل</strong></td>
                    <td class="col-sm-1 col-md-1 text-center"><strong>{{ $item->qty * $item->price }} د.ل </strong></td>
                    <td class="col-sm-1 col-md-1">
                        <a href="{{ url('destroy',$item->rowId)}}" class="btn btn-danger pull-right">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
                <!--<tr>
                    <td colspan="3">   </td>
                    <td><h5>المجموع </h5></td>
                    <td class="text-right"><h5><strong>$24.59</strong></h5></td>
                </tr>
                <tr>
                    <td colspan="3">   </td>
                    <td><h5> تكلفة الشحن </h5></td>
                    <td class="text-right"><h5><strong>$6.94</strong></h5></td>
                </tr>-->
                <tr>
                    <td colspan="3">   </td>
                    <td><h3>الاجمالي </h3></td>
                    <td class="text-right"><h3><strong>{{ Cart::subtotal() }} د.ل</strong></h3></td>
                </tr>
                <tr>
                    <td colspan="3">   </td>
                    <td>
                    <a href="{{ url('product') }}" class="btn btn-default pad">
                        <span class="glyphicon glyphicon-shopping-cart"></span> متابعة التسوق
                    </a></td>
                    <td>
                    <button type="button" class="btn btn-secondary pad">
                        الدفع الان  <span class="glyphicon glyphicon-play"></span>
                    </button></td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
@endsection('content')
