<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\User;
use Response;
use App\Admin;
use App\Service;
use App\Mail\Sendmail;
use Mail;
class AdminController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('control.index');
    }

    public function profile(){
        return view('control.profile');
    }

    public function users(){
        $admins = Admin::get();
        return view('control.settings.users',compact('admins'));
    }
    public function clients(){
        $clients = User::paginate(20);
        return view('control.clients',compact('clients'));
    }
    public function identity(){
        $clients = User::where('identity_file','!=','')->paginate(20);
        return view('control.identity',compact('clients'));
    }

    public function operaccount(Request $request){
        $idusers = $request->idusers;
        $status = $request->status;
        if($idusers && $status){
            DB::table('users')->where('id', $idusers)->update(array('status' => $status));
            return Response::json(array('success' => true), 200);
        }
    }


    public function operidentity(Request $request){
        $idusers = $request->idusers;
        $status = $request->status;
        if($idusers){

            if($status == 1){
                DB::table('users')->where('id', $idusers)
                ->update(
                    array('identity_file' => '')
                );
            }else{
                DB::table('users')->where('id', $idusers)
                ->update(
                    array('identity_status' => 1)
                );
            }

            return Response::json(array('success' => true), 200);
        }
    }

    public function search(Request $request){

        $search = $request->search;
        if($search){

            $clients = User::where('policia', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search. '%')
            ->orWhere('firstname', 'LIKE', '%' . $search. '%')
            ->orWhere('lastname', 'LIKE', '%' . $search. '%')
            ->get();

            $viewRendered = view('control.partial.client',compact('clients'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

    public function sendemail(Request $request){
        //dd($request->all());
       $user_id = $request->user_id;
       $service_id = $request->service_id;
       $subject = $request->subject;
       $message = $request->message;
       if($user_id && $subject && $message){

            $user = User::where('id', $user_id)->first();

            if($service_id !=0){
                Service::where('id',$service_id)->update(['status' => 1]);
            }

            $data = [
                'subject'   => $subject,
                'message'   => $message,
                'resp'      => $user,
                'status'    => 'admin_email',
            ];

            Mail::to($user->email)->send(new Sendmail($data));

            return Response::json(array('success' => true), 200);

       }
    }

    //
}
