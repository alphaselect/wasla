<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    protected $fillable = [
        'user_id','comment','ship_adresse_id','store_name','store_url','comment','internal_shipping_price','tax','status'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function childPurchases(){
        return $this->hasMany('App\ChildPurchase','purchase_id');
    }

    public function CountPurchases(){
        return $this->hasMany('App\ChildPurchase','purchase_id')->where('status','!=',5)->count();
    }

    public function purchases_price(){
        $price=0;
        $childPurchases= $this->childPurchases;
        foreach($childPurchases as $child){
            $price=$price+$child->product_price*$child->product_quantity;
        }
      return $price;
    }
    public function status(){
        $status=  $this->status;
        switch ($status) {
            case 0:
                return "قيد المراجعة";
                break;
            case 1:
                return "في انتظار الدفع";
                break;
            case 2:
                return "قيد التنفيد";
                break;
            case 3:
                return "تم الشراء";
                    break;
            case 4:
                return "مكتمل";
                break;
            case 5:
                return "ملغي";
                break;
            default:
            return "قيد المراجعة";
        }
    }
    public function code(){
        return $this->id+1000;
    }

}




