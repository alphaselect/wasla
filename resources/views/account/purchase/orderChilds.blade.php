
@foreach($childPurchases as $child)

<tr role="row">
    <td role="cell">{{$child->product_name}}</td>
    <td role="cell"><a href="{{$child->product_url}}" target="_blank" class="c_url">رابط الموقع</a></td>
    <td role="cell">  {{ $child->product_price }}$ / {{ $child->product_price * get_exchange() }}د.ل</td>
    <td role="cell">{{$child->product_quantity}}</td>
    <td role="cell">{{$child->product_properties}}</td>
    <td role="cell">{{$child->updated_at->format('Y-m-d')}}</td>
    <td role="cell">
        @if($child->companyurl)
            <a href="{{$child->companyurl}}" target="_blank"> {{$child->trackinginfo}} </a>
        @else
            -
        @endif
    </td>
    <td role="cell"> <span class="status-color-{{$child->status}}">{{$child->status()}}</span></td>

</tr>
@endforeach

