@extends('layouts.app')

@section('content')

<div class="container">
   <div class="logo-hom contact">
       <h1 class="h-home">
        {!! $terms->title !!}
       </h1>
   </div><!--end logo--hom-->
   <div class="section">
        <p class="section-description">
          {!! $terms->content !!}
        </p>
   </div>
</div>
@endsection
