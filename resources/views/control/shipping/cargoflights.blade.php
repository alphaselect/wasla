@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">  رحلات الشحن </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row gutter-xs">
            <div data-toggle="validator">
                <div class="col-lg-12">
                    <form class="savepolicia">
                        <div class="form-group col-lg-3">
                            <label>  رقم الرحلة </label>
                            <input type="text" name="codepolicia" class="form-control codepolicia" required placeholder="رقم الرحلة ">
                        </div>
                        <div class="form-group col-lg-3">
                            <label> عدد الشحنات  </label>
                            <input type="number" name="nbparcel" id="addScnt" class="form-control nbparcel" required placeholder="عدد الشحنات " >
                        </div>
                        <div class="form-group col-lg-3">
                            <label> دولة الشحن </label>
                            <select class="form-control shipcountry" name="shipcountry" required>
                                <option value="أمريكا">أمريكا</option>
                                <option value="الصين">الصين</option>
                                <option value="بريطانيا">بريطانيا</option>
                                <option value="ألمانيا">ألمانيا</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label> - </label>
                            <button type="submit" class="btn btn-primary btn-block "> إضافة رقم الرحلة</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br/>
        <div class="row gutter-xs">
            <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                <thead>
                    <tr>
                        <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                                <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                                <span class="custom-control-indicator"></span>
                            </label>
                        </th>
                        <th>رقم الرحلة</th>
                        <th>عدد الشحنات </th>
                        <th> تاريخ الاضافة  </th>
                        <th> دولة الشحن </th>
                        <th> الحالة  </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cargos as $cargo)
                        <tr class="{{ $cargo->id }}">
                            <td>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" name="checkAllyoushop[]" type="checkbox" value="">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                {{ $cargo->codepolicia }}
                            </td>
                            <td>
                                {{ $cargo->nbparcel }}
                            </td>
                            <td>
                                {{ $cargo->created_at->format('Y-m-d') }}
                            </td>
                            <td>
                                {{ $cargo->shipcountry }}
                            </td>
                            <td>
                                @if($cargo->status == 0)
                                    <span class="label label-gray label-pill"> اضافة شحنات </span>
                                @elseif($cargo->status == 1)
                                    <span class="label label-danger label-pill">  تم الشحن  </span>
                                @elseif($cargo->status == 2)
                                    <span class="label label-info label-pill">  جاهزة للاستلام </span>
                                @elseif($cargo->status == 3)
                                    <span class="label label-success label-pill"> تم التسليم </span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                    <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                      <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        @if($cargo->status == 0)
                                        <li><a href="{{ url('control/shipping/all',$cargo->id)}}" id="{{ $cargo->id }}" class="btn-link"> إضافة شحنات  </a></li>
                                        <li><a href="#" id="{{ $cargo->id }}" class="btn-link p_shipped 1"> تم الشحن   </a></li>
                                        @elseif($cargo->status == 1)
                                        <li><a href="#" id="{{ $cargo->id }}" class="btn-link p_shipped 2"> جاهزة للاستلام </a></li>
                                        @endif
                                        <li><a href="#" id="{{ $cargo->id }}" data_id="{{ $cargo->codepolicia }}" class="btn-link addtrackinginfo"> إضافة معلومات الشحن  </a></li>
                                        <!--<li><a href="#" id="{{ $cargo->codepolicia }}" class="btn-link sendemails"> تواصل مع العملاء  </a></li>-->
                                        <li><a href="#" id="{{ $cargo->codepolicia }}" class="btn-link shiplist">  قائمة الشحنات </a></li>
                                        <li><a href="#" id="{{ $cargo->id }}" class="btn-link"> تقرير شخنات الرحلة </a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="contactModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> رسالــة جماعية  ...</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal commentform">
                <div class="form-group">
                    <label for="InputName" class="control-label">موضوع الرسالة ...</label>
                    <input type="text" class="form-control" name="subjects" id="subjects"  required>
                    <input type="hidden" name="idpolicia" class="validpolicia" value=""/>
                </div>
                <div class="form-group">
                    <label for="InputMessage" class="control-label"> محتوى الرسالة ...</label>
                    <textarea name="message" id="message" class="form-control" rows="5" required></textarea>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" id="submit" value="أرسال الرسالــة " class="btn btn-success btn-block commentform">
                </div>
            </form>
        </div><!-- End of Modal body -->
    </div><!-- End of Modal content -->
</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
@endsection
