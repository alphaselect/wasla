<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Login',
    'signup' => 'Sign up',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    //form login
    'areyounew' => 'Are you new ?',
    'register'  => 'Register Now',
    'email'     => 'Address Email',
    'password'  => 'Password',
    'forgetpassword' => 'Forget Password ?',
    'resetpassword'  => 'Reset Password',
    'resetmsg'       => 'Enter your Email address we you will send you a link to reset your passowrd',
    'send'           => 'Send',
    'cancel'         => 'Cancel'

];
