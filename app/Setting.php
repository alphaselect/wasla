<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

    protected $fillable = [
        'facebook' , 'instagram' , 'twitter' , 'taux_purchase' ,'taux_purchase_free', 'exchange' , 'description' , 'contact_phone_tripoli' ,'contact_phone_benghazi',
        'contact_email_tripoli' , 'contact_email_benghazi' , 'maps_iframe_tripoli' , 'maps_iframe_benghazi',
    ];
}
