@extends('layouts.admin')

@section('content')
<div class="layout-content page">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> وسائل الشحن و الدفع المعتمدة
                </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row gutter-xs">
        <form data-toggle="validator" role="form" method="POST" action="{{ url('control/methode')}}" enctype="multipart/form-data">
            <div class="col-lg-12">
                <br/>
                <div class="form-group">
                    <textarea  name="content" id="content" row="4" class="form-control content">
                        {{ $methode->content }}
                    </textarea>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-2 text-center">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="idmethode" value="{{ $methode->id }}">
                    </div>
                    <div class="col-lg-8 text-center">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">تحديث المعلومات </button>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
