@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> المعاملات المالية </h2>
			</div>
			<!-- /.col-lg-12 -->
        </div>
        <style>
            .demo-dynamic-tables > thead>tr>th{
                text-align: center;
            }
        </style>
		<div class="row gutter-xs">
        <div class="row" style="margin-top: 20px;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                        <div id="demo-dynamic-tables-2_filter" class="dataTables_filter">
                            <label><input type="search" id="search" class="form-control input-sm searchtransaction" placeholder="البحث عن معاملة" aria-controls="demo-dynamic-tables-2"></label>
                        </div>
                    </div>
                </div>
              <div class="panel">
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="demo-dynamic-tables+2" class="table table-middle nowrap cell-border demo-dynamic-tables">
                        <thead>
                          <tr>
                              <th>اسم العميل </th>
                              <th>رقم العميل</th>
                              <th> رصيد الحساب د.ل</th>
                              <th>وصف العملية</th>
                              <th>القيمة</th>
                              <th> الوسيلة  </th>
                              <th> نوع العملية </th>
                              <th></th>
                          </tr>
                        </thead>
                        <tbody class="">
                            @foreach ($transactions as $transaction)
                            <tr class="{{ $transaction->id }}">
                                <td class="maw-320">
                                    {{ $transaction->getuser->firstname }}  {{ $transaction->getuser->lastname }}
                                </td>
                                <td><a href="{{ url('control/clients',$transaction->getuser->policia)}}" target="_blank">{{ $transaction->getuser->policia }}</a></td>
                                <td>{{ $transaction->funds_total }} د.ل</td>
                                <td><input type="text" name="funds_type" class="funds_type col-xs-12" placeholder="وصف العملية" required></td>
                                <td><input type="text" name="funds_add" class="funds_add col-xs-12" placeholder="القيمة " required></td>
                                <td>
                                    <select class="methodepayment" tabindex="1" name="methodepayment">
                                        <option value=""> الوسيلة </option>
                                        <option value=" كاش (وكيل معتمد)"> كاش (وكيل معتمد)</option>
                                        <option value=" وثبة (التجارة والتنمية)"> وثبة (التجارة والتنمية)</option>
                                        <option value=" ادفع لي (التجارة والتنمية)"> ادفع لي (التجارة والتنمية)</option>
                                        <option value=" بطاقة مصرفية محلية (ليبيا)"> بطاقة مصرفية محلية (ليبيا)</option>
                                        <option value=" سداد (المدار الجديد)"> سداد (المدار الجديد)</option>
                                        <option value=" رصيد (مدار أو ليبيانا)">  رصيد (مدار أو ليبيانا)</option>
                                        <option value=" مكتب هون للحوالات"> مكتب هون للحوالات‎</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="type_trasaction" class="type_trasaction">
                                        <option value=""> نوع العملية </option>
                                        <option value="1"> شحن الحساب </option>
                                        <option value="2"> خصم من المحفظة  </option>
                                    </select>
                                </td>
                                <td>
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                        <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" id="{{ $transaction->id }}" class="btn-link mosal_purchases_btn"> تنفيد العملية</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
                {!! $transactions->render() !!}
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
