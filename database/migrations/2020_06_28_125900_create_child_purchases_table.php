<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('purchase_id')->index();
            $table->foreign('purchase_id')->references('id')->on('purchases');
            $table->string('product_name');
            $table->string('product_url');
            $table->float('product_price');
            $table->string('product_quantity');
            $table->string('companyurl')->nullable();
            $table->string('trackinginfo')->nullable();
            $table->string('product_properties')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_purchases');
    }
}
