
 <tr role="row">
    <td role="cell">{{$purchase->code()}}</td>
    <td role="cell">{{$purchase->purchases_price()}}$</td>
    <td role="cell"><a href="{{$purchase->store_url}}" target="_blank" class="c_url">رابط الموقع</a></td>
    <td role="cell">{{$purchase->comment}}</td>
    <td role="cell">{{$purchase->status()}}</td>
    <td role="cell">{{$purchase->updated_at->format('Y-m-d')}}</td>
    <td><button class="btn btn-eye " data-toggle="modal" value="{{$purchase->id}}" data-target="#itmesOrderModal"><i class="fa fa-eye"></i> </button></td>
</tr>
