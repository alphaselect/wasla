@extends('layouts.account')
@section('content')
<section class="transactions">
    <h1 class="h-front"> المعاملات المالية </h1>
    <div class="add-funds">
        <a href="{{url('/addfunds')}}" class="btn-plus">
            &nbsp;<i class="fa fa-plus"></i>&nbsp;
            <span class="btnplus btnAddFunds">شحـن الحساب </span>
        </a>
    </div>
    <div class="mycompt">
        <div class="rows">
            <table class="table table-transactions table-bordered table-hover table-condensed table-responsive">
                <thead>
                    <tr>
                        <th> تاريخ العملية  </th>
                        <th>نوع العملية </th>
                        <th>مبلغ العملية بالدينار</th>
                        <th>الرصيد الحالي يالدينار </th>
                        <th> وسيلة الدفع </th>
                    </tr>
                </thead>
                <tbody class="targetcontent">
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td class="transaction_td">{{$transaction->created_at->format('Y-m-d')}}</td>
                            <td class="transaction_td">{{$transaction->operations}}</td>
                            <td class="transaction_td">{{$transaction->funds_add}} د.ل</td>
                            <td class="transaction_td">{{$transaction->funds_total}} د.ل</td>
                            <td class="transaction_td">{{$transaction->modpayment}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            {!! $transactions->render() !!}
        </div>
    </div>
</section>
@endsection('content')
