<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use Response;
class TransactionController extends Controller
{
    //

    public function __construct(){
       $this->middleware('auth:admin');
    }

    public function transaction(){
        $transactions  = Payment::where('status',0)->paginate(50);
        return view('control.transaction',compact('transactions'));
    }

    public function addfunds(Request $request){

        $transactid = $request->transactid;
        $funds_type = $request->funds_type;
        $funds_add = $request->funds_add;
        $methodepayment = $request->methodepayment;
        $type_trasaction = $request->type_trasaction;

        if($transactid && $funds_type && $funds_add){

            $transactions  = Payment::where('id',$transactid)->first();

            $funds_total = $transactions->funds_total + $funds_add;
            if($type_trasaction == 1){
                $type = "شحن الحساب ";
            }else{
                $type="خصم من المحفظة";
            }
            $payment = Payment::create([
                'user_id'     => $transactions->user_id,
                'operations'  => $type,
                'invoice'     => generateRandomString(11),
                'funds_add'   => $funds_add,
                'funds_total' => $funds_total,
                'modpayment'  => $methodepayment,
                'status'      => 0,
            ]);

            Payment::where('user_id',$transactions->user_id)->where('id','!=',$payment->id)->update([
                'status'     => 1,
                'updated_at' => Now()
            ]);

            return Response::json(array(
                'success'   => true,
                'message'      => "تمت عملية الدفع بنجاح .",
            ),200);

        }
    }
}
