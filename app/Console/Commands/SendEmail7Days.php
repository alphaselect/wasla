<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Shipping;
use App\Mail\Sendmail;
use Mail;
class SendEmail7Days extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parcelone:7days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email after 7 days parcel arrive warehouse in libya';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Shipping::where('updated_at',now()->subDays(7))->get()->each(function ($parcel) {
            $data = [
                'resp'   => $parcel,
                'status' => '7days',
            ];
            Mail::to($parcel->getuser->email)->send(new Sendmail($data));
        });
    }
}
