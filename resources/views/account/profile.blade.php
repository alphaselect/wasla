@extends('layouts.account')
@section('content')
<section>
    <h1 class="h-front"> الملف الشخصي </h1>
    <div class="mycompt">

    <div class="modal-body">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session()->get('success') }}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session()->get('error') }}
            </div>
        @endif
            <form class="modal-body registerfrom" action="{{url('/update_profile')}}" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="panel panel-default edit-profile ">
                            <div class="user-avatar">
                                <input id="thumbnail" name="thumbnail" type="file" class="hide"/>
                                @if($user->thumbnail)
                                    <img src="{{asset('storage/avatars').'/'.$user->thumbnail}}" class="main-profile-img" id="imgthumbnail" />
                                @else
                                    <img src="{{asset('assets/front/images/users_profile')}}/user-avatar.png" class="main-profile-img" id="imgthumbnail"  alt="" title=""/>
                                @endif
                                <span class="icon upload-avatar"><i class="fa fa-edit"></i></span>
                            </div>
                            <div class="clear"></div>

                    </div>
                </div>
            <div class="row">
                <div class="panel panel-default edit-profile ">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstname">الاسم الشخصي <samp class="asterisk">*</samp> </label>
                                <input id="firstname" name="firstname" class="form-control firstnames" type="text" placeholder="الاسم الشخصي" required="true" value="{{$user->firstname}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastname">الاسم العائلي <samp class="asterisk">*</samp> </label>
                                <input id="lastname" name="lastname" class="form-control lastnames" type="text" placeholder="الاسم العائلي" required="true" value="{{$user->lastname}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="country"> الدولة <samp class="asterisk">*</samp> </label>
                                <select class="form-control country" name="country" required="true" id="country">
                                    <option value="ليبيا">ليبيا</option>
                                    <!--<option value="مصر">مصر</option>
                                    <option value="تونس">تونس</option>
                                    <option value="المغرب">المغرب</option>
                                    <option value="الجزائر">الجزائر</option>
                                    <option value="السعودية">السعودية</option>
                                    <option value="انجلترا">انجلترا</option>
                                    <option value="امريكيا">امريكيا</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="city"> مدينة الاستلام <samp class="asterisk">*</samp> </label>
                                <select class="form-control" name="city" required="true" id="mycity">
                                    <option @if($user->city =="طرابلس") selected @endif value="طرابلس">طرابلس</option>
                                    <option @if($user->city =="بنغازي") selected @endif value="بنغازي">بنغازي</option>
                                    <!--<option value="مصراتة">مصراتة</option>
                                    <option value="البيضاء">البيضاء</option>
                                    <option value="الزاوية">الزاوية</option>
                                    <option value="زليتن">زليتن</option>
                                    <option value="أجدابيا">أجدابيا</option>
                                    <option value="طبرق">طبرق</option>
                                    <option value="سبها">سبها</option>
                                    <option value="الخمس">الخمس</option>
                                    <option value="درنة">درنة</option>
                                    <option value="صبراتة">صبراتة</option>
                                    <option value="زوارة">زوارة</option>
                                    <option value="الكفرة">الكفرة</option>
                                    <option value="المرج">المرج</option>
                                    <option value="ترهونة">ترهونة</option>
                                    <option value="سرت">سرت</option>
                                    <option value="غريان">غريان</option>
                                    <option value="مسلاتة">مسلاتة</option>
                                    <option value="بني وليد">بني وليد</option>
                                    <option value="الزنتان">الزنتان</option>
                                    <option value="الجميل">الجميل</option>
                                    <option value="صرمان">صرمان</option>
                                    <option value="شحات">شحات</option>
                                    <option value="أوباري">أوباري</option>
                                    <option value="يفرن">يفرن</option>
                                    <option value="الأبيار">الأبيار</option>
                                    <option value="رقدالين">رقدالين</option>
                                    <option value="القبة">القبة</option>
                                    <option value="تاورغاء">تاورغاء</option>
                                    <option value="الماية">الماية</option>
                                    <option value="مرزق">مرزق</option>
                                    <option value="البريقة">البريقة</option>
                                    <option value="هون">هون</option>
                                    <option value="جالو">جالو</option>
                                    <option value="العجيلات">العجيلات</option>
                                    <option value="نالوت">نالوت</option>
                                    <option value="سلوق">سلوق</option>
                                    <option value="زلطن">زلطن</option>
                                    <option value="مزدة">مزدة</option>
                                    <option value="راس لانوف">راس لانوف</option>
                                    <option value="العربان">العربان</option>
                                    <option value="ودان">ودان</option>
                                    <option value="توكرة">توكرة</option>
                                    <option value="براك">براك</option>
                                    <option value="غدامس">غدامس</option>
                                    <option value="غات">غات</option>
                                    <option value="أوجلة">أوجلة</option>
                                    <option value="سوسة">سوسة</option>-->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="address">العنوان <samp class="asterisk">*</samp> </label>
                                <input class="form-control fulladdress" id="fulladdress" name="address" type="text"  placeholder="العنوان" required="true" value="{{$user->address}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phonenumber">رقم الهاتف</label>
                                <input id="phonenumber" name="phone" class="form-control phonenumbers" type="tel" readonly value="{{$user->phone}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">البريد الالكتروني</label>
                                <input class="form-control emails" id="email" name="email" type="email" readonly value="{{$user->email}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="plan">نوع الباقة <samp class="asterisk"></samp> </label>
                                <input class="form-control " id="plan" name="plan" type="text" readonly value="{{ $user->plan == 1 ? 'توفير ' : 'مجاني' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="plan">اثبات الهوية<samp class="asterisk"></samp> </label>
                                @if($user->identity_status == 1)
                                    <input class="form-control"  type="text" readonly="" value="تم التحقق">
                                @elseif($user->identity_file)
                                    <input class="form-control"  type="text" readonly="" value="في انتظار التحقق">
                                @else
                                    <input class="form-control emails" id="verifier_profile" name="verifier_profile" type="file"  >
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="policia"> رقم العضوية  <samp class="asterisk"></samp> </label>
                                <input class="form-control " id="policia" name="policia" type="text" readonly value="{{ Auth::user()->policia }}">
                            </div>
                        </div>
                    </div>

            </div>



            <div class="row">
                <div class="panel panel-default edit-profile ">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="password">كلمة المرور <samp class="asterisk"></samp> </label>
                                <input type="password" class="form-control" id="password" name="password" value="">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="newPassword" class="">كلمة المرور الجديد</label>
                                <input type="password" class="form-control" id="newPassword" name="newPassword" value="">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="repetNewPassword" class="">اعادة كلمة المرور الجديد</label>
                                <input type="password" class="form-control" id="repetNewPassword" name="repetNewPassword" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    @csrf
                    <button type="submit" class="btn btn-front btn-block">
                    تحديث الملف الشخصي
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
</section>
@endsection('content')
