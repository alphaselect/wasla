<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->string('parcelcode')->unique();
            $table->float('weight');
            $table->float('width');
            $table->float('height');
            $table->float('lenght');
            $table->string('tracking');
            $table->string('shipcountry');
            $table->string('shipnumber')->default('-');
            $table->float('pricetopay')->default(0);
            $table->integer('statuspay')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
    }
}
