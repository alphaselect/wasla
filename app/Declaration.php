<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaration extends Model
{
    //

    protected $fillable = [
        'shipping_id','productname' , 'productcost' , 'productqty' , 'status'
    ];

    public function getShipping(){
        return $this->hasOne('App\Shipping','id','shipping_id');
    }
}
