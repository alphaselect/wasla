
<div id="registration-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header registermodal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">
            املأ الخانات التالية لإنشاء حساب مجاني
          </h4>
        </div>
        <div class="modal-body">
            <div id="div-login-msg" class="text-center">
                <div class="registererreur"></div>
            </div>
            <form class="modal-body registerfrom">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="firstname">الاسم الشخصي <samp class="asterisk">*</samp> </label>
                            <input id="firstname" name="firstname" class="form-control" type="text" placeholder="الاسم الشخصي" required="true" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="lastname">الاسم العائلي <samp class="asterisk">*</samp> </label>
                            <input id="lastname" name="lastname" class="form-control" type="text" placeholder="الاسم العائلي" required="true" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="second_firstname"> اسم الأب  <samp class="asterisk">*</samp> </label>
                            <input id="second_firstname" name="second_firstname" class="form-control" type="text" placeholder="إسم الأب" required="true" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="second_lastname"> إسم الجد  <samp class="asterisk">*</samp> </label>
                            <input id="second_lastname" name="second_lastname" class="form-control" type="text" placeholder="إسم الجد " required="true" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="country">اختر الدولة <samp class="asterisk">*</samp> </label>
                            <select class="form-control country" name="country" required="true" id="country">
                                <option value="ليبيا">ليبيا</option>
                                <!--<option value="مصر">مصر</option>
                                <option value="تونس">تونس</option>
                                <option value="المغرب">المغرب</option>
                                <option value="الجزائر">الجزائر</option>
                                <option value="السعودية">السعودية</option>
                                <option value="انجلترا">انجلترا</option>
                                <option value="امريكيا">امريكيا</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="city"> مدينة الاستلام <samp class="asterisk">*</samp> </label>
                            <select class="form-control" name="city" required="true" id="mycity">
                                <option value="">اختر المدينة</option>
                                <option value="طرابلس">طرابلس</option>
                                <option value="بنغازي">بنغازي</option>
                                <!--<option value="مصراتة">مصراتة</option>
                                <option value="البيضاء">البيضاء</option>
                                <option value="الزاوية">الزاوية</option>
                                <option value="زليتن">زليتن</option>
                                <option value="أجدابيا">أجدابيا</option>
                                <option value="طبرق">طبرق</option>
                                <option value="سبها">سبها</option>
                                <option value="الخمس">الخمس</option>
                                <option value="درنة">درنة</option>
                                <option value="صبراتة">صبراتة</option>
                                <option value="زوارة">زوارة</option>
                                <option value="الكفرة">الكفرة</option>
                                <option value="المرج">المرج</option>
                                <option value="ترهونة">ترهونة</option>
                                <option value="سرت">سرت</option>
                                <option value="غريان">غريان</option>
                                <option value="مسلاتة">مسلاتة</option>
                                <option value="بني وليد">بني وليد</option>
                                <option value="الزنتان">الزنتان</option>
                                <option value="الجميل">الجميل</option>
                                <option value="صرمان">صرمان</option>
                                <option value="شحات">شحات</option>
                                <option value="أوباري">أوباري</option>
                                <option value="يفرن">يفرن</option>
                                <option value="الأبيار">الأبيار</option>
                                <option value="رقدالين">رقدالين</option>
                                <option value="القبة">القبة</option>
                                <option value="تاورغاء">تاورغاء</option>
                                <option value="الماية">الماية</option>
                                <option value="مرزق">مرزق</option>
                                <option value="البريقة">البريقة</option>
                                <option value="هون">هون</option>
                                <option value="جالو">جالو</option>
                                <option value="العجيلات">العجيلات</option>
                                <option value="نالوت">نالوت</option>
                                <option value="سلوق">سلوق</option>
                                <option value="زلطن">زلطن</option>
                                <option value="مزدة">مزدة</option>
                                <option value="راس لانوف">راس لانوف</option>
                                <option value="العربان">العربان</option>
                                <option value="ودان">ودان</option>
                                <option value="توكرة">توكرة</option>
                                <option value="براك">براك</option>
                                <option value="غدامس">غدامس</option>
                                <option value="غات">غات</option>
                                <option value="أوجلة">أوجلة</option>
                                <option value="سوسة">سوسة</option>-->
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone">رقم الهاتف <samp class="asterisk">*</samp> </label>
                            <input id="phonenumber" name="phone" class="form-control phonenumbers allow_numeric" type="tel" value="09" maxlength="10" placeholder="رقم الهاتف" required="true" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">البريد الالكتروني <samp class="asterisk">*</samp> </label>
                            <input class="form-control emails" id="email" name="email" type="email" placeholder="البريد الالكتروني" required="true" />
                        </div>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label for="plan">نوع الباقة <samp class="asterisk">*</samp> </label>
                    <select class="form-control" name="plan" required="true">
                        <option value="0"> اشتراك مجاني  </option>
                    </select>
                </div>-->
                <div class="form-group">
                    <label for="address">عنوان السكن <samp class="asterisk">*</samp> </label>
                    <input class="form-control fulladdress" id="fulladdress" name="address" type="text" placeholder="عنوان السكن" required="true" />
                </div>
                <div class="form-group">
                    <label for="password">كلمة المرور <samp class="asterisk">*</samp> </label>
                    <input class="form-control plainPasswords" id="plainPassword" name="password" type="password" placeholder="كلمة المرور" required="true"/>
                </div>
                <div class="form-group">
                    <label> <input type="radio" name="terms" value="1" required="true" /> <span style="display: inline;">لقد قرأت ووافقت على الأحكام و الشروط </span><a href="{{url('terms')}}">إعرض الشروطٍ</a> </label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" id="register-form" class="btn btn-register btn-default btn-block registerform">إنشاء الحساب</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="ModalSMS" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header registermodal text-center">
                صفحة تاكيد و تفعيل رقمك الهاتفي ...
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!--<div class="col-lg-12 form-group">
                        <label for="phone"> رقمك الهاتفي  <samp class="asterisk">*</samp> </label>
                        <div class="input-group">
                            <input type="text" class="form-control my_phone" readonly name="phone" placeholder="">
                            <span class="input-group-btn">
                              <button class="btn btn-info changephone" type="button"> تغيير رقم الهاتف </button>
                            </span>
                        </div>
                    </div>-->
                    <div class="col-lg-12 form-group">
                        <label for="phone"> رقمك الهاتفي  <samp class="asterisk">*</samp> </label>
                        <input type="text" class="form-control my_phone" readonly name="phone" placeholder="">
                    </div>
                    <div class="col-lg-12 form-group">
                        <label for="smscode"> رمز تفعيل الهاتف <samp class="asterisk">*</samp> </label>
                        <input type="text" class="form-control sms_code" name="smscode" placeholder="رمز تفعيل الهاتف">
                        <input type="hidden" name="user_id" class="userID"/>
                    </div>
                    <div class="col-lg-12 form-group">
                        <button type="button" class="btn btn-register btn-default btn-block verifyPhone"> تفعيل رقم الهاتف </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalVerify" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-body text-center">
            <h2>
               تم تفعيل حسابك على الموقع بنجاج
            </h2>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> اغلاق </button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalSignSuccess" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header registermodal text-center">
                تاكيد ارسال معلومات التفعيل الى البريد الالكلتروني
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <div id="div-login-msg" class="text-center">
                    <div class="message text-center">
                        <h3> تم ارسال رسالة التفعيل الى بريدك الالكتروني </h3>
                        <blockquote class="blockquote-reverse" style="text-align:center;padding: 10px;">
                            <p>
                            نرجو التحقق من صندوق البريد، علماً بأنه قد تجد البريد في سلة البريد المزعح او السبام.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>
