@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> طلبات الشراء </h2>
			</div>
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table id="demo-dynamic-tables-2" class="table table-middle">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                            <th>رقم الطلب</th>
                            <th> رقم العميل</th>
                            <th>تاريخ الطلب </th>
                            <th>العدد</th>
                            <th> الموقع </th>
                            <th>سعر الشحن د</th>
                            <th>الضريبية</th>
                            <th> ملاحظات </th>
                            <th> المستودع</th>
							<th> الاجمالي</th>
                            <th>حالة </th>
						    <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAll[]" type="checkbox" value="">
                                              <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>{{ $order->code() }}</td>
                                    <td><a href="{{ url('control/clients',$order->user->policia)}}" target="_blank">{{ $order->user->policia }}</a></td>
                                    <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $order->CountPurchases() }}</td>
                                    <td><a href="{{ $order->store_url }}" target="_blank">{{ $order->store_name }}</a></td>
                                    <td>{{ $order->internal_shipping_price }}$</td>
                                    <td>{{ $order->tax }}$</td>
                                    <td>{{ $order->comment }}</td>
                                    <td>{{ $order->ship_adresse_id }}</td>
                                    <td>
                                        {{ round_up(get_pricePurchaseTotale($order),2) }}$ /
                                        {{ round_up((get_pricePurchaseTotale($order) * get_exchange()),2) }}د.ل
                                    </td>
                                    <td><span class='label label-warning'> قيد المراجعة </span></td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" class="btn-link fastbuyetat_ok" id="{{ $order->id }}"> مراجعة المنتجات </a></li>
                                                <li><a href="#"  class="btn-link acceptorders 1" id="{{ $order->id }}"> الموافقة على الطلب </a></li>
                                                <li><a href="#"  class="btn-link acceptorders 5" id="{{ $order->id }}"> الغاء الطلب </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
