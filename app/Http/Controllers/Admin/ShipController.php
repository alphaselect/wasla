<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Shipping;
use App\Shipimage;
use App\Cargoflight;
use App\Service;
use App\Cargotracking;
use Response;
use App\Mail\Sendmail;
use Mail;
class ShipController extends Controller
{
    //
    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function import(){
        return view('control.shipping.import');
    }
    public function GetallUser(Request $request){
        $type = $request->type;
        if($type == "codeclient"){
            $codeclient = $request->codeclt;
            $users  = DB::table('users')->select('policia')
                    ->where('users.policia','like','%'.strtoupper($codeclient).'%')->get();
            $data = array();
            foreach ($users as $user) {
                array_push($data,$user->policia);
            }
            return response()->json($data);
        }
    }
    public function signleUser(Request $request){
        $policia = $request->policia;
        $users  = DB::table('users')->where('policia',$policia)->get();
        return response()->json($users);
    }
    public function add_import(Request $request){
        $data = $request->all();

        $validator = Validator::make($data, [
            'weight'       => 'required',
            'lenght'       => 'required',
            'width'        => 'required',
            'height'       => 'required',
            'user_id'      => 'required',
            'shipcountry'  => 'required'
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        //dd($data);
        $unity = $request->unity;
        if($unity != 1){
            $data['weight'] = floatval(number_format(str_replace(',', '.',$request->weight) * 0.453592,1));
            $data['lenght'] = floatval(number_format(str_replace(',', '.',$request->lenght) * 2.54,1));
            $data['width']  = floatval(number_format(str_replace(',', '.',$request->width) * 2.54,1));
            $data['height'] = floatval(number_format(str_replace(',', '.',$request->height) * 2.54,1));
        }else{
            $data['weight'] = $request->weight;
            $data['lenght'] = $request->lenght;
            $data['width']  = $request->width;
            $data['height'] = $request->height;
        }
        $dimension = ($data['lenght'] +  $data['width'] + $data['height']);
        if($dimension>=150){
            $data['status'] = 5;
        }else{
            $data['status'] = 0;
        }

        $import = Shipping::create($data);

        $files = $request->file('image_file');
        foreach($files as $file){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/ship');
            $file->move($destinationPath, $name);
            Shipimage::create([
                'imagename'     =>  $name,
                'shipping_id'   => $import->id
            ]);
        }

        $data = [
            'resp'   => $import,
            'status' => 'import',
        ];

        Mail::to($import->getuser->email)->send(new Sendmail($data));

        return back()->with('success_import','تمت اضافة المنتج لحساب العميل بنجاح');
    }
    public function newshipment(){
        $parcels = Shipping::whereIn('status',[0,5])->get();
        return view('control.shipping.newshipment',compact('parcels'));
    }
    public function all($id){
        $parcels = Shipping::where('status',0)->where('shipnumber','=','-')->where('statuspay',1)->paginate(30);
        return view('control.shipping.all',compact('parcels','id'));
    }
    public function service(){
        $services  = Service::whereIn('status',[0,1,2])->get();
        return view('control.shipping.service',compact('services'));
    }
    public function opservice(Request $request){
        $service_id = $request->service_id;
        $status     = $request->status;
        if($service_id && $status){
            Service::where('id',$service_id)->update(['status' => $status]);
            return Response::json(array('success' => true), 200);
        }
    }
    public function ontheway(){
        $parcels = Shipping::where('status',1)->get();
        return view('control.shipping.ontheway',compact('parcels'));
    }
    public function ready(){
        $parcels = Shipping::where('status',2)->get();
        return view('control.shipping.ready',compact('parcels'));
    }
    public function delivred(){
        $parcels = Shipping::where('status',3)->get();
        return view('control.shipping.delivred',compact('parcels'));
    }
    /**********************************************/
    public function cargoflights(){
        $cargos = Cargoflight::get();
        return view('control.shipping.cargoflights',compact('cargos'));
    }

    public function savepolicia(Request $request){
        $data = $request->all();
        Cargoflight::create($data);
        return Response::json(array('success' => true), 200);
    }

    //getdeclaration
    public function getdeclaration(Request $request){
        $parcel = Shipping::where('id',$request->parcelid)->first();
        $viewRendered = view('partial.declaration',compact('parcel'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function addparceltopolicia(Request $request){
        $cargo = Cargoflight::where('id',$request->idpolicia)->first();
        $shipping = Shipping::where('id', $request->parcelid)->update(array('shipnumber' => $cargo->codepolicia));
        $count = Shipping::where('shipnumber','LIKE', $cargo->codepolicia)->count();
        $cargo = Cargoflight::where('id',$request->idpolicia)->update(array('nbparcel' => $count));
        return Response::json(array('success' => true), 200);
    }

    public function selectedparcel(Request $request){
        $data  = json_decode($request->data_array,true);
        $cargo = Cargoflight::where('id',$data[0]['idpolicia'])->first();
        foreach($data AS $value){
            $shipping = Shipping::where('id', $value['parcelid'])->update(array('shipnumber' => $cargo->codepolicia));
        }
        $count = Shipping::where('shipnumber','LIKE', $cargo->codepolicia)->count();
        $cargo = Cargoflight::where('id',$data[0]['idpolicia'])->update(array('nbparcel' => $count));
        return Response::json(array('success' => true), 200);
    }

    public function p_shipped(Request $request){
        $cargo_id   = $request->cargo_id;
        $status     = $request->status;
        if($cargo_id && $status){

            $cargo = Cargoflight::where('id',$cargo_id)->first();
            Cargoflight::where('id',$cargo_id)->update(array('status' => $status));
            Shipping::where('shipnumber','LIKE', $cargo->codepolicia)
            ->update(
                array(
                    'status' => $status ,
                    'updated_at' => NOW()
                )
            );

            if($status == 2){

                $parcels = Shipping::where('shipnumber','LIKE', $cargo->codepolicia)->get();

                foreach($parcels as $parcel){

                    $data = [
                        'resp'   => $parcel,
                        'status' => 'arrive',
                    ];

                    Mail::to($parcel->getuser->email)->send(new Sendmail($data));
                }
            }

            return Response::json(array('success' => true), 200);
        }
    }

    public function delivred_p(Request $request){
        $parcel_id   = $request->parcel_id;
        if($parcel_id){
            $parcel = Shipping::where('id',$parcel_id)->first();
            Shipping::where('id', $parcel_id)->update(array('status' => 3 , 'updated_at' => NOW()));
            $count = Shipping::where('shipnumber','LIKE', $parcel->shipnumber)->where('status',2)->count();
            if($count<1){
                Cargoflight::where('codepolicia','LIKE', $parcel->shipnumber)->update(array('status' => 3));
            }

            return Response::json(array('success' => true), 200);
        }
    }

    public function shiplist(Request $request){
        $parcels = Shipping::where('shipnumber','LIKE',$request->cargocode)->get();
        $viewRendered = view('control.partial.shipment',compact('parcels'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function tracking_info(Request $request){
        $cargo_id   = $request->cargo_id;
        $message      = $request->message;
        if($cargo_id && $message){
            Cargotracking::create([
                'cargoflight_id' => $cargo_id,
                'message' => $message,
            ]);
            return Response::json(array('success' => true), 200);
        }
    }
}
