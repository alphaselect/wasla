<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use DB;
class CalculatorController extends Controller
{


    public function calcule_price(Request $request){

        $data = $request->all();

        $validator = Validator::make($data, [
            'prd_price' => 'required',
            'weight'  => 'required',
            'length'     => 'required',
            'width'  => 'required',
            'height'      => 'required',
            //'type_account'=>'required'
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }

        $country        = $data["country"];
        $product_price  =(float)floatval($data["prd_price"]);
        $weight         =(float)floatval($data["weight"]);
        $length         =(float)floatval($data["length"]);
        $width          =(float)floatval($data["width"]);
        $heigh          =(float)$data["height"];
        $account_type   =(int)$data["type_account"];



     return $this->calcul_price($product_price,$weight,$length,$width,$heigh,$account_type);


    }





    public function float_to_int($value){
       $var = round_up($value);
       return str_replace(".0", "", $var);
    }

      public  function calcul_price($product_price,$weight,$length,$width,$heigh,$account_type){


        $insurance_costs=DB::table('settings')->first()->insurance_costs;
        $handling_cost=DB::table('settings')->first()->handling_cost;
        if($account_type==0){
            $accountType="اشتراك مجاني";
        }else{
            $accountType="اشتراك توفير";
        }
          if(0<$weight && 0<$length && 0<$width && 0<$heigh && 0<$product_price){


            $sum_length=$length+$width+$heigh;
            $fee=$product_price*$insurance_costs+$handling_cost;
            $volumetric_charge_weight=($length*$width*$heigh)/5000;
            $volumetric_charge=round_up($volumetric_charge_weight,1);

                if($volumetric_charge>60 || $weight>60){
                    return Response::json(array(
                        'success' => false,
                        'error' => 'لا يمكن شحنها',
                        'product_price' => $product_price,
                        'price' => 'لا يمكن شحنها',
                        'volumetric_charge' => $volumetric_charge,
                        'actual_weight' =>$weight,
                        'account_type' => $accountType,
                    ),200);
                }else{

                    if($sum_length>150 && $volumetric_charge>$weight){

                            $i=$volumetric_charge;
                            if(0<$i & $i<=10){
                                $price=$i*15+$fee;
                            }
                            if(10<$i & $i<=20){
                                $price=$i*12+$fee;
                            }
                            if(20<$i & $i<=30){
                                $price=$i*10+$fee;
                            }
                            if(30<$i){
                                $price=$i*9+$fee;
                            }



                        //return $this->round_up($price,1);
                        $price= round_up($price,1);
                        return Response::json(array(
                            'success' => true,
                            'product_price' => $product_price,
                            'price' => $price." $",
                            'fee' => $fee,
                            'volumetric_charge' => $volumetric_charge,
                            'actual_weight' =>$weight,
                            'account_type' => $accountType,
                        ),200);

                    }else{

                        if($account_type==0){

                            //  $price=($weight-0.1)*10+3+floor($weight)*3-floor($weight)+$fee;

                              $price=(round_up($weight)-0.1)*10+3+floor($weight)*3-floor($weight)+$fee;

                              //$price=$weight*10+2+$fee;
                             // return   $this->round_up($price,1);
                             $price=round_up($price,3);
                             return Response::json(array(
                              'success' => true,
                              'product_price' => $product_price,
                              'price' => $price." $",
                              'volumetric_charge' => $volumetric_charge,
                              'actual_weight' =>$weight,
                              'account_type' => $accountType,
                          ),200);


                          }else{
                              $price=($weight*10)+$fee;
                              //return $this->round_up($price,1);
                              $price= round_up($price,1);
                              return Response::json(array(
                                  'success' => true,
                                  'product_price' => $product_price,
                                  'price' => $price." $",
                                  'volumetric_charge' => $volumetric_charge,
                                  'actual_weight' =>$weight,
                                  'account_type' => $accountType,
                              ),200);

                          }
                    }

                }


            }else{
                //return "error";
                return Response::json(array(
                    'success' => false,
                    'error' => 'المرجو التأكد من جميع الحقول',
                ),200);
            }

      }


}
