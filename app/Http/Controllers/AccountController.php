<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\ShipAdress;
use File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Response;
use App\Mail\Sendmail;
use Mail;
class AccountController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function account(){
        $dateNow=Carbon::now();
        $seaaddress     = ShipAdress::where('status',0)->where('type_address','sea')->get();
        $freightaddress = ShipAdress::where('status',0)->where('type_address','freight')->get();

        $plan=Auth::user()->plan;
        if($plan==1){

            $date_end=Auth::user()->date_end;
            $date_end= strtotime($date_end);
            $dateNow= strtotime($dateNow);
            $calculate_seconds = $date_end- $dateNow;

            if($calculate_seconds<=0){
               Auth::user()->update([
                   'plan'=>0,
                   'updated_at'=>$dateNow
               ]);
            }

        }else{

        }
        return view('account.index',compact('seaaddress','freightaddress'));
    }
    public function profile(){
        $user=Auth::user();
        return view('account.profile',compact('user'));
    }

    public function update_profile(Request $request){
        $id=$user=Auth::user()->id;
        $data = $request->all();
        $error="";
        $updated_at=Carbon::now();
        $password = $data['password'];
        $newPassword = $data['newPassword'];
        $repetNewPassword = $data['repetNewPassword'];

        $validator = Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            //'email'     => 'required|string|email|max:255|unique:users',
            //'password'  => 'required|string|min:6',
            'country'   => 'required',
            'city'      => 'required',
            'address'   => 'required',
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }


        $extensions = array('jpg','jpeg','png');
        if($request->hasFile('thumbnail')){
            $image = $request->thumbnail;
            $verifyimg = getimagesize($image);
            $pattern = "#^(image/)[^\s\n<]+$#i";
            if(in_array($request->thumbnail->extension(), $extensions)){
                if(!preg_match($pattern, $verifyimg['mime']))
                {
                    $user_picture = "";
                }else {
                    $user_picture = time().'_p'.$id.'.'.$image->getClientOriginalExtension();
                    $image->move(storage_path('app/public/avatars'), $user_picture);
                }
            }else{
                $error="المرجو تحميل صورة ذات الصيغة jpg أو png";
            }
        }else{
            $user_picture = Auth::user()->thumbnail;
        }


        $extensions_v = array('jpg','jpeg','png');
        if($request->hasFile('verifier_profile')){
            $image_v = $request->verifier_profile;
            $verifyimg = getimagesize($image_v);
            $pattern = "#^(image/)[^\s\n<]+$#i";
            if(in_array($request->verifier_profile->extension(), $extensions_v)){
                if(!preg_match($pattern, $verifyimg['mime']))
                {
                    $verifier_file = "";
                }else {
                    $verifier_file = time().'_v'.$id.'.'.$image_v->getClientOriginalExtension();
                    $image_v->move(storage_path('app/public/identity'), $verifier_file);
                    //$image->move(storage_path('app/public/avatars'), $user_picture);
                }
            }else{
                $error="المرجو تحميل صورة ذات الصيغة jpg أو png ";
            }


        }else{
            $verifier_file = Auth::user()->identity_file;
        }


       if($error==""){

        if(!empty($newPassword) && !empty($repetNewPassword)  && ($newPassword==$repetNewPassword)){

            $validator_pass = Validator::make($data, [

                'newPassword'  => 'required|string|min:6',
                'repetNewPassword'  => 'required|string|min:6',
            ]);

            if ($validator_pass->fails()){
                return Response::json(array(
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                ),400); // 400 being the HTTP code for an invalid request.
            }else{

                if(Auth::attempt(['email'=>Auth::user()->email,'password'=>$request->password])){
                    Auth::user()->update([
                        'firstname'   => $data['firstname'],
                        'lastname'    => $data['lastname'],
                        'country'     => $data['country'],
                        'city'        => $data['city'],
                        'address'     => $data['address'],
                        'thumbnail'    => $user_picture,
                        'identity_file'=> $verifier_file,
                        'password'     => bcrypt($data['newPassword']),
                        'updated_at'    =>$updated_at
                    ]);
                    return redirect()->back()->with('success', 'تم تحديث الملف الشخصي');
                }else{

                    $error="المرجو التأكد من الرقم السري";
                    return redirect()->back()->with('error', $error);
                }

            }


        }elseif(empty($newPassword) && empty($repetNewPassword) ){
           // dd($verifier_file);
            Auth::user()->update([
                'firstname'   => $data['firstname'],
                'lastname'    => $data['lastname'],
                'country'     => $data['country'],
                'city'        => $data['city'],
                'address'     => $data['address'],
                'thumbnail'    => $user_picture,
                'identity_file'    => $verifier_file,
                'updated_at'    =>$updated_at

            ]);
            return redirect()->back()->with('success', 'تم تحديث الملف الشخصي');
        }elseif(!empty($newPassword) || !empty($repetNewPassword)){
            $error="المرجو التأكد من الرقم السري";
            return redirect()->back()->with('error', $error);
        }

        }else{
            return redirect()->back()->with('error', $error);
            //echo $error;
        }

    }

    public function contact(){
        return view('account.contact');
    }

    public function sendemail(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'name'     => 'required',
            'email'    => 'required|email',
            'subject'  => 'required',
            'message'  => 'required',
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }

        $data = [
            'resp'   => $data,
            'status' => 'contact_email',
        ];
        Mail::to("contact@alphaselect.net")->send(new Sendmail($data));

        return redirect()->back()->with('success', 'تم ارسال الرسالة . سيتم الرد عليك خلال 24ساعة ');
    }

    public function plan(){
        $plan= Auth::user()->plan;
        $date= Auth::user()->date_end;

       if(!empty($date)){
        $date= date_create($date);
        $date_end=date_format($date,'Y-m-d H:i:s');
       $date_end= date("M j, Y H:i:s",strtotime($date_end));
       }else{
         $date_end=0;
       }

      //dd($date_end);
        return view('account.plan')->with(['plan'=>$plan,'date_end'=>$date_end]);
    }
    public function calculator(){
        return view('account.calculator');
    }

    public function verifier_profile(Request $request){

        $data = $request->all();
        $id=Auth::user()->id;
        $updated_at=Carbon::now();
      //$error="المرجو تحميل صورة للبطاقة الوطنية او جواز السفر";
        $extensions_v = array('jpg','jpeg','png');
        if($request->hasFile('verifier_profile')){
            $image_v = $request->verifier_profile;
            $verifyimg = getimagesize($image_v);
            $pattern = "#^(image/)[^\s\n<]+$#i";
           // dd($image_v);
            if(in_array($request->verifier_profile->extension(), $extensions_v)){
                if(!preg_match($pattern, $verifyimg['mime']))
                {
                    $verifier_file = "";
                }else {
                 // "hh";
                    $verifier_file = time().'_v'.$id.'.'.$image_v->getClientOriginalExtension();
                    //$image_v->move(storage_path('images\verified_profiles'), $verifier_file);
                    $image_v->move(storage_path('app/public/identity'), $verifier_file);
                   $error="";
                   Auth::user()->update([

                    'identity_file'    => $verifier_file,
                    'updated_at'    =>$updated_at

                ]);
                }
            }else{
                $error="المرجو تحميل صورة ذات الصيغة jpg أو png ";
            }


        }else{
            $error="المرجو تحميل صورة ذات الصيغة jpg أو png ";
        }

        if(!empty($error)){
            return Response::json(array(
                'success' => false,
                'msg' => $error
            ),200);
        }else{
            return Response::json(array(
                'success' => true,
                'msg' => "لقد تم ارسال الملف"
            ),200);
        }

    }




}
