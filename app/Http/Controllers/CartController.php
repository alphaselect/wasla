<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
class CartController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function cart(){
        if(count(Cart::instance('wasla')->content())<=0){
            return redirect()->back();
        }else{
            //dd(Cart::instance('wasla')->content());
            return view('account.cart');
        }
    }

    public function add(Request $request){
        //dd($request->all());
        $cartItem = Cart::instance('wasla')->add(
            $request->product_id,$request->product_name,$request->product_qty,$request->product_price
        );

        flashy()->success(" __('home.msg_addtocart')" );

        return redirect()->back()->with('success_code',1);
    }
    public function destroy($id){
       Cart::instance('wasla')->remove($id);
       if(count(Cart::instance('wasla')->content())<1){
            return redirect('account')->withSuccessMessage('Item has been removed!');
       }else{
            return redirect('cart')->withSuccessMessage('Item has been removed!');
       }
    }
}
