<div class="row">
    <div class="col-sm-12">
        <div class="chipment">
            <div class="chipment-table">
                <div class="row">
                <div class="col-md-5">
                        <div class=" product-slider-m">
                            <div class="sp-loading" style="display: none;">
                                <img src="{{asset('assets/front/images/icon/Loading.gif')}}" alt=""><br>LOADING IMAGES
                            </div>
                            <div class="sp-wrap">
                                @foreach ($product->Prdimages as $img)
                                    <div class="brd">
                                        <a href="#" >
                                            <img src="{{ url('upload/product') }}/{{ $img->product_image }}" alt="">
                                        </a>
                                    </div>
                                @endforeach
                                <!--<div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/1.jpg')}}" alt=""></a></div>
                                <div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/2.jpg')}}" alt=""></a></div>
                                <div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/3.jpg')}}" alt=""></a></div>
                                <div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/4.jpg')}}" alt=""></a></div>
                                <div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/5.jpg')}}" alt=""></a></div>
                                <div class="brd"><a href="#" ><img src="{{asset('assets/front/images/products/6.jpg')}}" alt=""></a></div>-->
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="panel panel-default product-header-m">
                            <h3>{{$product->product_name}}</h3>
                            <!--<p class="sub-text">دولار أيو فيجايت
                                نيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت
                            </p>-->
                            <p>
                                <span class="price">{{$product->product_price}} د.ل</span>
                                <span class="qty"> الكمية : {{$product->product_qty}}</span>
                            </p>

                        </div>
                        <div class="panel panel-default product-description-m">
                            <p>
                            {!! $product->product_desc !!}
                            </p>
                        </div>
                        <div class="product-btn">
                            <form action="{{ url('cart')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label> الكمية </label>
                                    <input type="number" placeholder="الكمية المطلوبة" value="1" class="form-control" name="product_qty">
                                    <input type="hidden" name="product_id" value="{{$product->id}}"/>
                                    <input type="hidden" name="product_name" value="{{$product->product_name}}"/>
                                    <input type="hidden" name="product_price" value="{{$product->product_price}}"/>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-front-full" style="withd"><i class="fa fa-shopping-cart"><span>+</span></i>اضف لسلة التسوق</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.sp-wrap').smoothproducts();
</script>
