<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargotrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargotrackings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('cargoflight_id')->index()->nullable();
            $table->foreign('cargoflight_id')->nullable()->references('id')->on('cargoflights');
            $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargotrackings');
    }
}
