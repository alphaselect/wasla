@foreach ($parcels as $parcel)
    <tr class="{{ $parcel->id}}">
        <td>
            <span class="icon-with-child m-r">
                @foreach ($parcel->GetImage as $image)
                    <img class="circle" width="40" height="40" src="{{ asset('upload/ship') }}/{{ $image->imagename}}" alt="">
                    @break
                @endforeach
            </span>
        </td>
        <td> {{ $parcel->parcelcode }} </td>
        <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
        <td> {{ $parcel->created_at->format('Y-m-d')}} </td>
        <td>
            {{ $parcel->weight }} كيلو <br>
            {{ $parcel->width }} * {{ $parcel->height }} * {{ $parcel->lenght }} سم
        </td>
        <td>
            @if($parcel->shipcountry == 'usa')
                امريكا
            @endif
        </td>
        <td>{{ $parcel->tracking }}</td>
        <td> <a href="#" id="{{ $parcel->id}}" class="getDeclaration">مشاهدة </a> </td>
        <td>
            @if($parcel->pricetopay)
                {{ $parcel->pricetopay }}$
            @else
                <b> ليس بعد </b>
            @endif
        </td>
        <td>
            @if($parcel->status == 0)
                <span class='label label-info'> في المستودع </span>
            @elseif($parcel->status == 1)
                <span class='label label-warning'> تم الشحن </span>
            @elseif($parcel->status == 2)
                <span class='label label-info'> جاهز للتسليم </span>
            @elseif($parcel->status == 3)
                <span class='label label-success'> تم التسليم </span>
            @elseif($parcel->status == 5)
                <span class='label label-primary'> شحنة معلقة  </span>
            @endif
        </td>
        <td>
            @if($parcel->statuspay == 0)
                <span class='label label-primary'> ليس بعد </span>
            @elseif($parcel->statuspay == 1)
                <span class='label label-success'> تم الدفع  </span>
            @endif
        </td>
    </tr>
@endforeach
