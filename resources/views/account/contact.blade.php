@extends('layouts.account')
@section('content')

<h1 class="h-front">الدعم الفني</h1>
<div class="mycompt aide">
    <div class="header-form-aide">
        <h3>هل لديك سؤال؟</h3>
        <h3>فريقنا دائما موجود لرد على استفساراتكم. يمكنكم مراسلتنا عبر البريد الالكتروني الاتي</h3>
        <div class="contact">
            <ul>
                <li>info@global-link.ly<i class="fa fa-envelope-o"></i></li>
                <li> +218 910457534<i class="fa fa-phone"></i></li>
            </ul>
        </div>
        <p>يمكنك ايضا التحدث معنا مباشرا او عبر البريد الإلكتروني عبر ملئ نمودج الاتصال التالي</p>
        <div class="clear"></div>
    </div>

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session()->get('success') }}
        </div>
    @endif

    @if(session()->has('errors'))
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session()->get('errors')->first() }}
        </div>
    @endif
    <form class="form" method="post" action="{{url('contact')}}">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="name" class="">الاسم الكامل </label>
                    <input type="text" class="form-control" required value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" placeholder="الاسم الكامل" id="name" name="name">
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="email" class="">البريد الالكتروني</label>
                        <input type="email" class="form-control" required value="{{ Auth::user()->email }}" placeholder="البريد الالكتروني" id="email" name="email" >
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="subject" class=""> موضوع الرسالة  </label>
                        <input type="text" class="form-control" required placeholder="موضوع الرسالة" id="subject" name="subject">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="message" class=""> محتوى الرسالة </label>
                    <textarea class="form-control" rows="8" required id="message" name="message" placeholder="محتوى الرسالة "></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class=col-sm-12>
                <div class="form-group row-btn">
                    <button type="submit" class="btn btn-front">أرسل</button>
                </div>
            </div>
        </div>
        @csrf
    </form>
</div>

@endsection('content')
