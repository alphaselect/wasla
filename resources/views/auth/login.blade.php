<div class="modal fade login-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content login-modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="login-modal-body">
                <div class="row">
                    <p class="login-errors-msg"></p>
                    <form class="form signinform" id="form-login">
                        <div class="col-sm-6">
                            <h3>تسجيل الدخول</h3>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="email"><i class="fa fa-user"></i></label>
                                    <input type="text" class="form-control" id="email"  required="true" name="email" placeholder="البريد الالكتروني">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label>
                                    <input type="password" class="form-control" id="password" required="true" name="password" placeholder="كلمة المرور">
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="#" class="a-getpass">هل نسيت كلمة المرور</a>
                            </div>
                            <div class="form-group row-btn">
                                <button type="button" class="btn btn-login btn-default formlogin">
                                    تسجيل الدخول
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="col-sm-6">
                        <div class="login-modal-body-h form-group"><img src="{{asset('assets/front/images/signin-background.png')}}" style="height: 200px;"></div>
                        <div class="form-group">
                                <span>لا يوجد لديك اشتراك ؟</span>
                                <a href="#" class="a-sinin"  onclick="close_modale('login-modal' ,'registration-modal')"  >اشترك الآن</a>
                        </div>
                    </div>
                </div>
                  <!--end form-->
            </div>
      </div>
    </div>
</div>
