<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargoflight extends Model
{
    //

    protected $fillable = [
        'codepolicia' , 'nbparcel' , 'shipcountry' , 'status'
    ];
}
