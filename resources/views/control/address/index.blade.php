@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
					<h2 class="page-header">عناوين الشحن</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <a href="{{ url('control/address/create') }}" class="btn btn-success"> اضافة عنوان </a>
		    <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-middle">
                            <thead>
                            <tr>
                                <th>عنوان الشحن </th>
                                <th>الدولة</th>
                                <th> العنوان سطر1</th>
                                <th>المدينة </th>
                                <th>المحافطة </th>
                                <th> الرمز البريدي</th>
                                <th>الهاتف </th>
                                <th>الحالة</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($address as $address)
                                <tr class="{{ $address->id }}">
                                    <td>
                                        @if($address->type_address == 'sea')
                                            <b> الشحن البحري</b>
                                        @else
                                            <b> الشحن الجوي </b>
                                        @endif
                                    </td>
                                    <td>{{ $address->country }}</td>
                                    <td>{{ $address->Adressline }}</td>
                                    <td>{{ $address->city }}</td>
                                    <td>{{ $address->state }}</td>
                                    <td>{{ $address->zipcode }}</td>
                                    <td>{{ $address->mobile }}</td>
                                    <td>
                                        @if($address->status == 0)
                                            <span class="label label-success label-pill">نشيط</span>
                                        @else
                                            <span class="label label-danger label-pill">موقف</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                            <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{ url('control/address/edit',$address->id)}}" class="btn-link">تعديل العنوان </a></li>
                                                @if($address->status == 0)
                                                    <li><a href="#" class="btn-link activateaddress 1" id="{{ $address->id }}">ايقاف العنوان </a></li>
                                                @else
                                                    <li><a href="#" class="btn-link activateaddress 0" id="{{ $address->id }}">عرض العنوان </a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
		</div>
	</div>
</div>
@endsection
