<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'abdellahB',
            'email' => 'starfilmy@gmail.com',
            'firstname'=>'abdellah',
            'lastname'=>'abdellah',
            'role'=> 'admin',
            'image'=> '-',
            'password' => bcrypt('123456'),
        ]);

        DB::table('pages')->insert([
            'title' => 'معلومات عنا ',
            'slug'     => 'about-us',
            'content'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        //
        DB::table('pages')->insert([
            'title' => 'شروط الاستخدام',
            'slug'     => 'terms-and-conditions',
            'content'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        //
        DB::table('pages')->insert([
            'title'     => 'سياسة الخصوصية',
            'slug'      => 'privacy-and-policy',
            'content'   =>'المحتوى العربي',
            'position'  => 'fixed',
        ]);

        DB::table('pages')->insert([
            'title'     => 'وسائل الشحن و الدفع المعتمدة',
            'slug'      => 'ship-pay-method',
            'content'   => 'المحتوى العربي',
            'position'  => 'fixed',
        ]);

        DB::table('settings')->insert([
            'facebook'   => 'facebook.com',
            'instagram'  => 'instagram.com',
            'twitter'      => 'twitter',
            'taux_purchase'   => 6,
            'taux_purchase_free'   => 6,
            'insurance_costs'   => 6,
            'handling_cost'   => 6,
            'exchange'  => 6,
            'description'      => 'xxx',
            'contact_phone_tripoli'   => 'facebook.com',
            'contact_phone_benghazi'  => 'instagram.com',
            'contact_email_tripoli'      => 'xxx',
            'contact_email_benghazi'   => 'facebook.com',
            'maps_iframe_tripoli'  => 'instagram.com',
            'maps_iframe_benghazi'      => 'xxx',
        ]);
    }
}
