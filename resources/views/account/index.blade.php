@extends('layouts.account')
@section('content')
<section class="user-info">
    <div class="user-name">
        <span class="promo"><a href="{{ url('contact') }}"><i>{{ Auth::user()->plan == 1 ? 'توفير' : 'ترقية'}}</i></a></span>
        <span> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span>
        <span class="user-avatar circle"><img src="{{asset('assets/front/images/user-avatar.png')}}" class="img-circle"></span>

    </div>
    <div class="user-info-body">
        <ul>
            <li><span>رقم الحساب</span><span class="bld">{{ Auth::user()->policia }}</span></li>
            <li><span>نوع العضوية </span><span class="bld">{{ Auth::user()->plan == 1 ? 'توفير' : 'مجاني'}}</span></li>
            <li><span>تاريخ الإشتراك</span><span class="bld">{{ Auth::user()->created_at->format('Y-m-d') }}</span></li>
            <li><span>تاريخ انتهاء الإشتراك</span>
                <span class="bld">
                    {{ Auth::user()->plan == 1 ? Auth::user()->date_end : 'غير محدد' }}
                </span>
            </li>
            <li>
                @if(Auth::user()->identity_status == 1)
                    <input class="btn btn-success"  type="button" value="تم التحقق">
                    @elseif(Auth::user()->identity_file)
                        <input class="btn btn-info"  type="button" value="في انتظار التحقق">
                    @else
                    <button class="btn btn-default btn-confirm" data-toggle="modal" data-target="#verifierProfileModale">تأكيد الهوية</button>
                @endif

            </li>
        </ul>
        <div class="clear"></div>
    </div>
</section>
<!--end user-info-->
<section class="shipping-adress">
    <h3 class="section-title">عناوين الشحن</h3>
    <div class="shipping-adress-info body-border">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">الشحن الجوي </a></li>
            <li><a data-toggle="tab" href="#menu1"> الشحن البحري </a></li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="shipping-adress-header">
                    <div class="row address">
                        @if(count($freightaddress)>0)
                            @foreach ($freightaddress AS $freight)
                                <div class="col-md-4">
                                    <h4>
                                        @if($freight->country == 'usa')
                                            العنوان الامريكي
                                        @elseif($freight->country == 'cn')
                                            العنوان الصيني
                                        @elseif($freight->country == 'uk')
                                            العنوان البريطاني
                                        @elseif($freight->country == 'gb')
                                            العنوان الألماني
                                        @endif
                                    </h4>
                                    <ul class="list-group">
                                        <li>
                                            <span class="control-field">
                                                Full name:
                                                <b> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Adress line 1 :
                                                <b>{{$freight->Adressline}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Line 2 :
                                                <b>{{ Auth::user()->policia }} </b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                City :
                                                <b>{{$freight->city}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                State :
                                                <b>{{$freight->state}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Post/Zipe code :
                                                <b>{{$freight->zipcode}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Country :
                                                <b>
                                                    {{$freight->country}}
                                                </b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Mobile :
                                                <b>{{$freight->mobile}}</b>
                                            </span>
                                        </li>
                                    </ul>
                                </div><!--end -->
                            @endforeach
                        @endif
                    </div>
                </div><!--end shipping-adress-header -->
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="shipping-adress-header">
                    <div class="row address">
                        @if(count($seaaddress)>0)
                            @foreach ($seaaddress AS $sea)
                                <div class="col-md-4">
                                    <h4>
                                    @if($sea->country == 'usa')
                                        العنوان الامريكي
                                    @elseif($sea->country == 'cn')
                                        العنوان الصيني
                                    @elseif($sea->country == 'uk')
                                        العنوان البريطاني
                                    @elseif($sea->country == 'gb')
                                        العنوان الألماني
                                    @endif
                                    </h4>
                                    <ul class="list-group">
                                        <li>
                                            <span class="control-field">
                                                Full name:
                                                <b> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Adress line 1 :
                                                <b>{{$sea->Adressline}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Line 2 :
                                                <b>{{ Auth::user()->policia }} </b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                City :
                                                <b>{{$sea->city}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                State :
                                                <b>{{$sea->state}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Post/Zipe code :
                                                <b>{{$sea->zipcode}}</b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Country :
                                                <b>
                                                    {{$sea->country}}
                                                </b>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="control-field">
                                                Mobile :
                                                <b>{{$sea->mobile}}</b>
                                            </span>
                                        </li>
                                    </ul>
                                </div><!--end -->
                            @endforeach
                        @endif
                    </div>
                </div><!--end shipping-adress-header -->
            </div>
        </div>
    </div>
</section>
<section class="calculator">
    <h3 class="section-title">حاسبة الاسعار</h3>
    <div class="body-border">
        <form action="{{url('/calculator')}}" id="calculatorform" method="post">
            <div class="calculatorform">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" id="country" name="country">
                                <option value="">دولة الشحن</option>
                                <option value="usa"> امريكا </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control input-control" id="prd_price" name="prd_price" placeholder="سعر المنتج" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-control" id="weight" name="weight" placeholder="وزن الشحنة" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-control" id="width" name="width" placeholder="العرض" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control input-control" id="type_account" name="type_account" required>
                                <option value=""> نوع الحساب  </option>
                                <option value="0"> اشتراك مجاني </option>
                                <option value="1"> اشتراك توفير </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-control" id="length" name="length" placeholder="الطول" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-control" id="height" name="height" placeholder="الأرتفاع" required>
                        </div>
                    </div>
                </div>
                @csrf
                <div class="b text-center">
                    <button type="button" class="btn btn-front btn-calcule" style="float: none;">
                        حساب تكلفة الشحن
                    </button>
                </div>
            </div>
        </form>
        <div class="resulte ">
            <div class="table-responsive col-sm-12">
                <table class="table table-bordered hide">
                    <thead>
                        <tr>
                        <th >نوع الحساب</th>
                        <th >سعر المنتج</th>
                        <th >الوزن الفعلي</th>
                        <th >الوزن الحجمي</th>
                        <th ><strong>تكلفة الشحن</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td class="account_type"></td>
                            <td class="prd_price"></td>
                            <td class="actual_weight"></td>
                            <td class="volumetric_charge"></td>
                            <td class="price"></td>
                            <!--<span class="price"></span>-->
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<!--end calculatrice-->
<div class="clear"></div>
@endsection
