<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargotracking extends Model
{
    //

    protected $fillable = [
        'cargoflight_id' , 'message'
    ];
}
