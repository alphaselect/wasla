<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildPurchase extends Model
{
    //

    protected $fillable = [
        'purchase_id','product_name','product_url','product_price','companyurl','trackinginfo',
        'product_quantity','product_properties','status'
    ];

    public function status(){
        $status =  $this->status;
        switch ($status) {
            case 0:
                return "قيد المراجعة";
                break;
            case 1:
                return "في انتظار الدفع";
                break;
            case 2:
                return "قيد التنفيد";
                break;
            case 3:
                return "تم الشراء";
                    break;
            case 4:
                return "مكتمل";
                break;
            case 5:
                return "ملغي";
                break;
            default:
            return "قيد المراجعة";
        }
    }

}
