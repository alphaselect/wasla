<?php

namespace App\Export;

use App\Order;
use Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class TablesExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(int $status)
    {
        $this->status = $status;
    }
    public function view(): View
    {
        $orders = Order::where('user_id','=',Auth::id())->where('status','=',$this->status)->get();
        return view('account.exports.excel',compact('orders'));
    }
}
