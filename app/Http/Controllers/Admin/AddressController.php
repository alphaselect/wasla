<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\ShipAdress;
class AddressController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function list(){
        $address = ShipAdress::get();
        return view('control.address.index',compact('address'));
    }

    public function create(){
        return view('control.address.create');
    }

    public function store(Request $request){
        $data = $request->all();
        $address = ShipAdress::create($data);
        return back()->with('success','تمت اضافة عنوان الشحن المطلوب');
    }

    public function editaddress(Request $request){
        $address = ShipAdress::where('id',$request->id)->first();
        return view('control.address.edit',compact('address'));
    }
    public function updateaddress(Request $request){
        $address = ShipAdress::where('id',$request->id)->first();
        $address->fill($request->all())->save();
        return back()->with('success','تم تحديث عنوان الشحن المطلوب ');
    }
    public function action(Request $request){
        $addressid = $request->addressid;
        $status = $request->status;
        if($addressid){
            $address = DB::table('ship_adresses')
            ->where('id', $addressid)
            ->update(
                array(
                    'status' => $status
                )
            );
            return Response::json(array('success' => true), 200);
        }
    }
}
