<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\helpers;
use Carbon\Carbon;
use Response;
use App\ChildPurchase;
use App\Purchase;
use DB;

class PurchaseController extends Controller
{


    public function __construct(){
        $this->middleware('auth');
    }

    public function allorder(){
        $orders = Purchase::orderBy('created_at', 'desc')->paginate(15);
        return view('account.purchase.allorder')->with('orders',$orders);
    }

    public function neworder(){
        return view('account.purchase.neworder');
    }


    public function addorder(Request $request){
        $data = json_decode($request->data_array,true);

        $validator = Validator::make($data[0], [
            'storeName' => 'required|string|max:255',
            'storeUrl' => 'required|string|max:255',
            'comment' => 'string|max:255',
            'InternalShippingPrice' => 'required|numeric',
            'tax' => 'required|numeric',
            'shippingAddresses' => 'required|numeric',

        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->errors()->first()
            ),200);
        }

        if($data[0]['total_data']<6 && $data[0]['total_data']>0){

            $purchase = Purchase::create([
                'store_name'                => $data[0]['storeName'],
                'store_url'                 => $data[0]['storeUrl'],
                'comment'                   => $data[0]['comment'],
                'internal_shipping_price'   => $data[0]['InternalShippingPrice'],
                'tax'                       => $data[0]['tax'],
                'ship_adresse_id'           => $data[0]['shippingAddresses'],
                'user_id'                   => Auth::user()->id,
                'status'                    => 0
            ]);

            foreach($data as $key => $value) {

                $validator = Validator::make($value, [
                    'c_product' => 'required|string|max:255',
                    'c_url' => 'required|string|max:255',
                    'c_price' => 'required|numeric',
                    'c_quantity' => 'required|numeric',

                ]);
                if ($validator->fails()){
                    return Response::json(array(
                        'success' => false,
                        'errors' => $validator->errors()->first()
                    ),200); // 400 being the HTTP code for an invalid request.
                }

                $child  = ChildPurchase::create([
                    'product_name'               => $value['c_product'],
                    'product_url'                => $value['c_url'],
                    'product_price'              => $value['c_price'],
                    'product_quantity'           => $value['c_quantity'],
                    'product_properties'         => $value['c_properties'],
                    'purchase_id'                => $purchase->id,
                ]);

            }

            return Response::json(array(
                'success' => true,
                'message' => 'تم استقبال طلبك بنجاح وستتم مراجعته خلال ٢٤ ساعه'
            ),200);

        }else{

            return Response::json(array(
                'success' => false,
                'message' => 'لقد تجاوزت الحد الأقصى من الطلبيات او لم تضع اي طلبية',
            ),200);
        }

    }

    public function viewOrder(Request $request){
        $data = $request->all();
        $id=(int)$data['id'];
        $order_code = Auth::user()->purchases()->find($id)->code();
        $purchase=Auth::user()->purchases()->find($id);
        $childPurchases=Auth::user()->purchases()->find($id)->childPurchases;

        $viewOrderChilds = view('account.purchase.orderChilds',compact('childPurchases'))->render();

        return Response::json(array(
            'success'   => true,
            'head'      => $order_code,
            'body'      => $viewOrderChilds,
        ),200);
    }
    public function cancel_purchase(Request $request){
        $id= (int)$request->id;
        $date=Carbon::now();
        $purchase=Purchase::find($id);
        
        if($id>0 && !empty($purchase)){
           
            
            Purchase::find($id)->update([
                'status'=>5,
                'updated_at'=>$date
            ]);
            Purchase::find($id)->childPurchases()->update(['status'=>5,'updated_at'=>$date]);

            return Response::json(array(
                'success'   => true,
                'message'      => "لقد تم الغاء هذا الطلب",
            ),200);  
        }else{
            return Response::json(array(
                'success'   => false,
                'message'      => "المرجو اعادة المحاولة لاحقا",
            ),200);
        }
    }
    public function search(Request $request){
        $s=(int)$request->search;
        $id=$s-1000;
        if($id>0){

        $purchase= Auth::user()->purchases()->find($id);
            if(!empty($purchase)){
                $viewPurchase = view('account.purchase.searchPurchase',compact('purchase'))->render();

                return Response::json(array(
                    'success'   => true,
                    'html'      => $viewPurchase

                ),200);

            }else{
                return Response::json(array(
                    'success' => false,
                    'message' => '<tr><td colspan="7" style="text-align: center">لا يتوفر حاليا اي طلب بهذا الرقم</td></tr>',
                ),200);
            }


        }else{
            return Response::json(array(
                'success' => false,
                'message' => '<tr><td colspan="7" style="text-align: center">لا يتوفر حاليا اي طلب بهذا الرقم</td></tr>',
            ),200);
        }
    }



}
