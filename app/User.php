<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'phone' =>  'integer'
    ];
    protected $fillable = [
        'firstname','lastname', 'second_firstname','second_lastname','email','country','city','address','zipcode','phone','sms_code','phone_status','verifyToken',
        'thumbnail','identity_file','policia','password','date_start','date_end','plan','identity_status','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function purchases(){
        return $this->hasMany('App\Purchase','user_id');
    }
    public function payment(){
        return $this->hasMany('App\Payment','user_id');
    }
    public function routeNotificationForMessagebird($notification){
        return "+218".$this->phone;
    }

}
