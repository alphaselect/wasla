@extends('layouts.admin')
@section('content')
<div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <select class="selectpicker" name="period" data-style="btn-default btn-sm">
                        <option value="today">اليوم</option>
                        <option value="yesterday">الامس</option>
                        <option value="last_7d">أخر 7 ايام</option>
                        <option value="last_1m" selected="selected">اخر 30 يوما </option>
                        <option value="last_3m"> أخر 90 يوما </option>
                    </select>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib"> لوحة التحكم </span>
                </h1>
           </div>
          <div class="row gutter-xs">
            <div class="col-md-6 col-lg-3 col-lg-push-0">
              <div class="card bg-primary">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-primary-inverse circle sq-48">
                        <span class="icon icon-user"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-push-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-success-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">طلبات التغليف</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-3">
              <div class="card bg-info">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-info-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">طلبات الشراء</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-0">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-danger-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">الشحنات المجهولة</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="row gutter-xs">
            <div class="col-xs-12 col-md-6">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Sales</h4>
                  </div>
                  <div class="card-body">
                    <div class="card-chart"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                      <canvas id="demo-sales" data-chart="bar" data-animation="false" data-labels="[&quot;Aug 1&quot;, &quot;Aug 2&quot;, &quot;Aug 3&quot;, &quot;Aug 4&quot;, &quot;Aug 5&quot;, &quot;Aug 6&quot;, &quot;Aug 7&quot;, &quot;Aug 8&quot;, &quot;Aug 9&quot;, &quot;Aug 10&quot;, &quot;Aug 11&quot;, &quot;Aug 12&quot;, &quot;Aug 13&quot;, &quot;Aug 14&quot;, &quot;Aug 15&quot;, &quot;Aug 16&quot;, &quot;Aug 17&quot;, &quot;Aug 18&quot;, &quot;Aug 19&quot;, &quot;Aug 20&quot;, &quot;Aug 21&quot;, &quot;Aug 22&quot;, &quot;Aug 23&quot;, &quot;Aug 24&quot;, &quot;Aug 25&quot;, &quot;Aug 26&quot;, &quot;Aug 27&quot;, &quot;Aug 28&quot;, &quot;Aug 29&quot;, &quot;Aug 30&quot;, &quot;Aug 31&quot;]" data-values="[{&quot;label&quot;: &quot;Sales&quot;, &quot;backgroundColor&quot;: &quot;#d9831f&quot;, &quot;borderColor&quot;: &quot;#d9831f&quot;,  &quot;data&quot;: [3601.09, 2780.29, 1993.39, 4277.07, 4798.58, 6390.75, 3337.37, 6786.94, 5632.1, 5460.43, 3905.17, 3070.82, 4263.55, 7132.64, 6103.88, 6020.76, 4662.25, 4084.34, 3464.87, 4947.89, 4486.55, 5898.46, 5528.33, 3616.03, 3255.17, 7881.06, 7293.8, 6863.6, 3161.31, 6711.08, 7942.9]}]" data-hide="[&quot;legend&quot;, &quot;scalesX&quot;]" height="558" width="1116" style="display: block; width: 558px; height: 279px;"></canvas>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">0 حساب مسجل من بينها 0 حساب نشيط</h4>
                </div>
                <div class="card-body">
                  <div class="card-chart">
                    <canvas data-chart="bar" data-animation="false" data-labels='["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]' data-values='[{"label": "This week", "backgroundColor": "#d9230f", "borderColor": "#d9230f", "data": [3089, 2132, 1854, 2326, 3274, 3679, 3075]}, {"label": "Last week", "backgroundColor": "#d9831f", "borderColor": "#d9831f", "data": [983, 2232, 3057, 2238, 1613, 2194, 3874]}]' data-tooltips='{"mode": "label"}' data-hide='["gridLinesX", "legend"]' height="150"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
 </div>
@endsection
