@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">معلومات الموقع</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form method="POST" action="{{ url('control/general')}}" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                             <label class="default prepend-icon" >فيس بوك</label>
                             <input id="facebook" class="form-control" type="text" name="facebook" placeholder="فيس بوك" value="" required="true">
                        </div>
                        @csrf
                        <div class="form-group">
                            <label class="default prepend-icon" >انستغرام</label>
                            <input id="instagram" class="form-control" type="text" name="instagram" placeholder="انستغرام" value="" required="true">
                       </div>
                       <div class="form-group">
                            <label class="default prepend-icon" >تويتر </label>
                            <input id="facebook" class="form-control" type="text" name="twitter" placeholder="تويتر" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" > عمولة الشراء عضوية مجانية</label>
                            <input id="instagram" class="form-control" type="text" name="taux_purchase_free" placeholder="عمولة الشراء عضوية مجانية" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" >ايميل ننغازي </label>
                            <input id="instagram" class="form-control" type="text" name="contact_email_benghazi" placeholder="ايميل ننغازي" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" > خريطة بنغازي </label>
                            <input id="instagram" class="form-control" type="text" name="maps_iframe_benghazi" placeholder=" خريطة بنغازي" value="" required="true">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="default prepend-icon" >سعر الصرف </label>
                            <input id="facebook" class="form-control" type="text" name="exchange" placeholder="سعر الصرف" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" > عمولة الشراء عضوية توفير</label>
                            <input id="instagram" class="form-control" type="text" name="taux_purchase" placeholder="عمولة الشراء عضوية توفير" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" >هاتف بنعازي </label>
                            <input id="instagram" class="form-control" type="text" name="contact_phone_tripoli" placeholder="هاتف بنعازي" value="" required="true">
                       </div>
                       <div class="form-group">
                            <label class="default prepend-icon" >هاتف طرابلس</label>
                            <input id="facebook" class="form-control" type="text" name="contact_phone_benghazi" placeholder="هاتف طرابلس " value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" >ايميل طرابلس</label>
                            <input id="instagram" class="form-control" type="text" name="contact_email_tripoli" placeholder="ايميل طرابلس" value="" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" > خريطة طرابلس </label>
                            <input id="instagram" class="form-control" type="text" name="maps_iframe_tripoli" placeholder=" خريطة طرابلس  " value="" required="true">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="default prepend-icon">وصف الموقع </label>
                            <textarea class="form-control" rows="4" name="description" required="true"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-info btn-block">تحديث المعلومات</button>
                        </div>
                    </div>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection
