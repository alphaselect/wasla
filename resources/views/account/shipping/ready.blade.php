@extends('layouts.account')
@section('content')
<section>
    <div class="row shipment">
        <div class="col-md-4">
            <div class="article">
                <div class="header-article backg-c">
                    <span>{{ $item1}}</span>
                    <a href="{{ url('shipment') }}">
                        <h3><i class="icon-ch icon-accountShipments"></i>شحنات في الحساب</h3>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="article">
                <div class="header-article backg-c active">
                    <span>{{ $item2}}</span>
                    <a href="{{ url('ready') }}">
                        <h3><i class="icon-ch icon-shipmentReady"></i>شحنات جاهزة للتسليم</h3>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="article">
                <div class="header-article backg-c">
                    <span>{{ $item3}}</span>
                    <a href="{{ url('delivered') }}">
                        <h3><i class="icon-ch icon-shipmentsDelivered"></i>شحنات تم تسليمها</h3>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @if(count($parcels)>0)
    <div class="row shipment">
        @foreach ($parcels as $parcel)
        <div class="col-md-4">
            <div class="article">
                <div class=" body-article">
                    <div class="img-article">
                        @foreach ($parcel->GetImage as $image)
                            <img src="{{ asset('upload/ship') }}/{{ $image->imagename}}" alt="{{ $image->imagename}}">
                            @break
                        @endforeach
                        @if($parcel->statuspay == 1)
                            <img class="onhold" title="تم الدفع للشحنة " src="{{ asset('assets/front/images/paid3.png') }}">
                        @endif
                    </div>
                    <div class="shipping-list">
                        <ul>
                            <li><span class="txt"> كود الشحنة  </span><span class="nb parcelcode">#{{ $parcel->parcelcode }}</span></li>
                            <li><span class="txt">رقم تتبع الشحنة </span><span class="nb">{{ $parcel->tracking }}</span></li>
                            <li><span class="txt">وزن الشحنة </span><span class="nb">{{ $parcel->weight }} كيلو</span></li>
                            <li><span class="txt">أبعاد الشحنة</span><span class="nb">{{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} سم</span></li>
                            <li>
                                <span class="txt">دولة الشحن</span><span class="nb">
                                    @if($parcel->shipcountry == "usa")
                                        امريكا
                                    @endif
                                </span>
                            </li>
                            @if($parcel->shipnumber !='-')
                                <li><span class="txt">رقم الرحلة </span><span class="nb">
                                    <a href="#">
                                        {{ $parcel->shipnumber }}
                                    </a>
                                </span></li>
                            @endif
                            @if($parcel->pricetopay)
                                <li>
                                    <span class="txt"> تكلفة الشحن </span>
                                    <span class="nb pricetopays">
                                        <b>{{ $parcel->pricetopay }}$ / {{ $parcel->pricetopay * get_exchange() }} د.ل</b>
                                    </span>
                                    <input type="hidden" value="@if(Auth::user()->plan == 1) {{ count($parcel->getCountService) * 15 }}  @else {{ count($parcel->getCountService) * 25 }}  @endif" id="{{ $parcel->id }}" class="price_Service"/>
                                </li>
                            @endif
                            @if($parcel->status == 5)
                            <li class="text-center">
                                <span class="txt textred">
                                    سيتم حساب سعر الشحن بناء على الوزن الحجمي للشحنة للموافقة على  الامر يرجي النقر على زر الموافقة اسفله
                                </span>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="bottom-article">
                        <button class="btn btn-shipping delared" id="{{ $parcel->id }}">  محتوى التصريح </button>
                        @if($parcel->statuspay == 0)
                            <button class="btn btn-shipping payshipping" value="{{ $parcel->pricetopay * get_exchange() }} د.ل" id="{{ $parcel->id }}"> الدفع الان </button>
                        @endif
                        <button class="btn btn-shipping reportError" id="{{ $parcel->id }}">التبليغ عن خطأ</button>
                        @if(count($parcel->getService)>0)
                            <button class="btn btn-shipping SpecialService" id="{{ $parcel->id }}">
                                تفاصيل الخدمة المطلوبة
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        {!! $parcels->links() !!}
    </div>
    @else
    <div class="shipment">
        <div class="box-body">
            <div class="no-packagebox">
                <img src="{{ asset('assets/front/images/dropbox.png') }}"/>
                <h1>
                    ليس لديك اي شحنات في الوقت الحالي
                </h1>
            </div>
        </div>
    </div>
    @endif
    <!--end shipment-->
</section>
@endsection('content')
