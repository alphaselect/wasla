<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('second_firstname');
            $table->string('second_lastname');
            $table->string('email')->unique()->index();
            $table->string('country');
            $table->string('city');
            $table->text('address');
            $table->string('phone');
            $table->integer('sms_code');
            $table->integer('phone_status')->default(0);
            $table->string('thumbnail')->nullable();
            $table->string('identity_file')->nullable();
            $table->string('policia',20)->unique();
            $table->string('password');
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->integer('plan')->default(0);
            $table->integer('identity_status')->default(0);
            $table->integer('status')->default(0);
            $table->string('verifyToken');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
