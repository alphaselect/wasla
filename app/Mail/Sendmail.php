<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Sendmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        $address = 'no-reply@global-link.ly';
        $name    = 'الوصلة العالمية‎';

        if($this->data['status'] == 'admin_email'){

            $subject = $this->data['subject'];
            return $this->view('emails.contact')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['message'],'resp' => $this->data['resp']]);

        }elseif ($this->data['status'] == 'contact_email'){

            $subject = $this->data['resp']['subject'];

            return $this->view('emails.contact')
            ->from($address, $name)
            ->replyTo($this->data['resp']['email'], $this->data['resp']['name'])
            ->subject($subject)
            ->with(['content' => $this->data['resp']['message'] , 'resp' => '']);

        }elseif ($this->data['status'] == 'import'){

            $subject = "وصلت شحنتك الى المستودع ";
            return $this->view('emails.import')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }elseif ($this->data['status'] == 'arrive'){

            $subject = "شحنتك وصلت و جاهزة للاستلام";
            return $this->view('emails.arrive')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }elseif ($this->data['status'] == '7days'){

            $subject = "شحنتك وصلت و جاهزة للاستلام مند 7 ايام";
            return $this->view('emails.remind7day')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }elseif ($this->data['status'] == '15days'){

            $subject = "شحنتك وصلت و جاهزة للاستلام مند 15 يوم";
            return $this->view('emails.remind15days')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }elseif ($this->data['status'] == '50days'){

            $subject = "شحنتك تجاوزت 50 يوما في مقر الشركة ";
            return $this->view('emails.remind50days')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }elseif ($this->data['status'] == 'activate'){

            $subject = "تفعيل حسابك على الموقع";
            return $this->view('emails.activation')->from($address, $name)->subject($subject)
            ->with(['content' => $this->data['resp']]);

        }

    }
}
