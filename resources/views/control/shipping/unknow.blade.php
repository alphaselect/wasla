@extends('layouts.admin')
@section('content')
<style>
    .resp_error{
        padding: 10px;
        text-align: center;
        color: red;
        font-size: 14px;
    }
</style>
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> شحنات بدون هوية </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>الصورة</th>
                          <th>رقم الشحنة</th>
                          <th> العميل</th>
                          <th> الاسم الكامل</th>
                          <th> تاريخ الوصول </th>
                          <th>الوزن/الأبعاد</th>
                          <th>شركة الشحن </th>
                          <th>التتبع </th>
                          <th>المخزن </th>
                          <th>الدولة </th>
                          <th> الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
