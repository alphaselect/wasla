@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
					<h2 class="page-header"> كل المنتجات </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <a href="{{ url('control/product/create') }}" class="btn btn-success pull-right"> اضافة منتج جديد  </a>
		    <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-middle">
                            <thead>
                            <tr>
                                <th style="width:15%">اسم المنتج	 </th>
                                <th>الكمية المتاحة</th>
                                <th>السعر </th>
                                <th>وصف المنتج </th>
                                <th>تاريخ النشر </th>
                                <th>الحالة</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                   <tr>
                                      <td> {{ $product->product_name }} </td>
                                      <td> {{ $product->product_qty }} </td>
                                      <td> {{ $product->product_price }} د.ل</td>
                                      <td class="maw-320">
                                            <span class="truncate" style="white-space: break-spaces;"> {!! $product->product_desc !!} </span>
                                      </td>
                                      <td>{{ $product->created_at->format('Y-m-d') }}</td>
                                      <td>
                                            @if($product->product_status == 0)
                                                <span class="label label-success">متاح</span>
                                            @else
                                                <span class="label label-primary">ملغي</span>
                                            @endif
                                      </td>
                                      <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($product->product_status == 0)
                                                    <li><a href="{{ url('control/product/delete', $product->id)}}" class="btn-link"> حدف المنتج </a></li>
                                                @else
                                                    <li><a href="{{ url('control/product/backup', $product->id)}}" class="btn-link"> استعادة المنتج </a></li>
                                                @endif
                                                <li><a href="{{ url('control/product/edit', $product->id)}}" class="btn-link"> تحرير المنتج </a></li>
                                            </ul>
                                        </div>
                                      </td>
                                   </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
		</div>
	</div>
</div>
@endsection
