@extends('layouts.app')

@section('content')

<div class="container">
   <div class="logo-hom contact">
       <h1 class="h-home">
           تواصل معنا
       </h1>
   </div><!--end logo--hom-->
   <div class="section">
        <p class="section-description text-center">
            لا تتردد بالتواصل معنا .. هدفنا خدمتك وتوفير أفضل وسائل الشحن للتوفير في التكلفة وضمان سلامة مشترياتك
        </p>
        <div class="row">
            <div class="col-lg-5 mb-4">
                <div class="panel panel-default panels">
                    <form class="panel-body">
                        <div class="panel-heading blue accent-1">
                            <h3><i class="fa fa-envelope-o fa-1x"></i>  هل لديك استفسار؟ </h3>
                        </div>
                        <div class="form-group">
                            <input type="text" name="fullname" placeholder="الاسم الكامل " required="true" id="form-name" class="form-control">
                            <label for="form-name"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="البريد الالكتروني" required="true" id="form-email" class="form-control">
                            <label for="form-email"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" placeholder="موضوع الرسالة" required="true" id="form-Subject" class="form-control">
                            <label for="form-Subject"></label>
                        </div>
                        <div class="form-group">
                            {!! csrf_field() !!}
                            <textarea id="form-text" name="message" placeholder="محتوى الرسالة" required="true" class="form-control md-textarea" rows="4"></textarea>
                        </div>
                        <div class="form-group text-center mt-4">
                            <button type="submit" class="btn btn-secondary btn-block sendemail">إرسـال </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-7">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home"> عنوان طرابلس</a></li>
                    <li><a data-toggle="tab" href="#menu1"> عنوان بنغازي  </a></li>
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div id="map-container-google-11" style="padding-top: 20px;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3714.49629236612!2d39.29618875985275!3d21.409729294601103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3cb6afcb68313%3A0x78440391dbf6b0fe!2sAlajaweed%2C%20Jeddah%2022442!5e0!3m2!1sen!2ssa!4v1590587725808!5m2!1sen!2ssa"
                              frameborder="0" width="100%" height="350px" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="row text-center" style="padding-top:10px;">
                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-map-marker fa-2x"></i></a>
                              <p>طرابلس </p>
                            </div>

                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-phone fa-2x"></i></a>
                              <p>+218 910457534</p>
                            </div>

                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-envelope-o fa-2x"></i></a>
                              <p>info@global-link.ly</p>
                            </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div id="map-container-google-11" style="padding-top: 20px;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3714.49629236612!2d39.29618875985275!3d21.409729294601103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3cb6afcb68313%3A0x78440391dbf6b0fe!2sAlajaweed%2C%20Jeddah%2022442!5e0!3m2!1sen!2ssa!4v1590587725808!5m2!1sen!2ssa"
                              frameborder="0" width="100%" height="350px" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="row text-center" style="padding-top:10px;">
                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-map-marker fa-2x"></i></a>
                              <p>بنغازي </p>
                            </div>

                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-phone fa-2x"></i></a>
                              <p>+218 910457534</p>
                            </div>

                            <div class="col-md-4">
                              <a class="btn-floating blue accent-1"><i class="fa fa-envelope-o fa-2x"></i></a>
                              <p>info@global-link.ly</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
@endsection
