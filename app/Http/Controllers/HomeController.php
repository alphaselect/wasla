<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\helpers;

use Auth;
use App;
use Response;

use App\Page;
use App\Question;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        return view ('index');
    }

    public function faq(){
        $questions = Question::get();
        return view('faq',compact('questions'));
    }
    public function contactus(){
        return view('contact');
    }
    public function privacy(){
        $privacy = Page::where('id','=',3)->first();
        return view('privacy',compact('privacy'));
    }
    public function terms(){
        $terms = Page::where('id','=',2)->first();
        return view('terms',compact('terms'));
    }
    public function modepay(){
        $methode = Page::where('id','=',4)->first();
        return view('modepay',compact('methode'));
    }
    public function about(){
        $about = Page::where('id','=',1)->first();
        return view('about',compact('about'));
    }

}
