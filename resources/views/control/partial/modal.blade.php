<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Shipping information
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form role="form">
                    <div class="form-group">
                        <label>Date Shipping (mm/dd/yy)</label>
                        <div class="input-with-icon">
                        <input class="form-control" type="text" name="dateship" data-provide="datepicker" data-date-today-highlight="true">
                        <span class="icon icon-calendar input-icon"></span>
                        </div>
                        <input type="hidden" name="packing_id" class="packing_id" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="companiesname"> Company </label>
                        <input type="text" class="form-control" name="companiesname" id="companiesname" placeholder="company name"/>
                    </div>
                    <div class="form-group">
                        <label for="demo-select2-2" class="form-label">tracking</label>
                        <select type="text" id="demo-select2-3" name="tracking[]" class="demo-select2-3 form-control" multiple="multiple"/>
                        </select>
                    </div>
                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary addtracking">
                    Save changes
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalQuestion" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">اغلاق</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    الاسئلة الشائعة
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label class="default prepend-icon" >السؤال </label>
                        <input class="form-control questions" type="text" name="questions" tabindex="2" required="true">
                    </div>
                    <div class="form-group">
                        <label>الجواب </label>
                        <textarea class="form-control answer" rows="3" name="answer" required="true"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveQuestion">
                   اضافة السؤال
                </button>
            </div>
        </div>
    </div>
</div>

<!-- new users -->
<div class="modal fade" id="myModalUsers" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">اغلاق</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   اضافة المستخدمين
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form data-toggle="validator" class="addusers" role="form" method="POST" action="" enctype="multipart/form-data">
					<div class="col-lg-6">
						<div class="form-group">
							<label class="default prepend-icon" >الاسم الاول</label>
							<input id="firstname" class="form-control" type="text" name="firstname" tabindex="2" required="true">
						</div>
						<div class="form-group">
							<label class="default prepend-icon" >اختر الرتبة</label>
							<select name="role" class="form-control" tabindex="1" required="true">
								<option value="">اختر الرتبة</option>
								<option value="admin">مدير عام</option>
								<option value="employers">موظف</option>
								<option value="trainee">متدرب</option>
							</select>
						</div>
						<div class="form-group">
							 <label class="default prepend-icon" >كلمة المرور</label>
								<input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="من فضلك ادخل كلمة المرور" required="true">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							 <label class="default prepend-icon" >الاسم الثاني</label>
								<input id="lastname" class="form-control" type="lastname" name="lastname" tabindex="2" required="true">
						</div>
						<div class="form-group">
							 <label class="default prepend-icon" >البريد الالكتروني</label>
							<input id="email" class="form-control" type="email" name="email" tabindex="2" required="true">
						</div>
						<div class="form-group">
							<label class="default prepend-icon" >صورة شخصية</label>
							<input type="file" name="image"  class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button type="submit" name="submit" class="btn btn-primary btn-block addusers">اضافة المستخدم</button>
					</div>
				</form>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form sendemailtouser" method="POST">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        ارسال رسالة للعميل
                    </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subject">موضوع الرسالة </label>
                        <input type="text" class="form-control subject" required name="subject" id="subject" placeholder="موضوع الرسالة"/>
                    </div>
                    <div class="form-group">
                        <label for="message">محتوى الرسالة  </label>
                        <textarea type="text" class="form-control" required rows="4" name="message" id="message" placeholder="محتوى الرسالة"></textarea>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-block sendemailtouser" value="ارسال">
                    <input type="hidden" value="" required class="user_id" name="user_id">
                    <input type="hidden" value="0" class="service_id" name="service_id">
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>


<div class="modal fade" id="myModalNewsLetter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form role="form" method="post" action="#">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        ارسال رسالة لكل العملاء
                    </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">موضوع الرسالة </label>
                        <input type="text" class="form-control" required name="subject" id="subject" placeholder="موضوع الرسالة"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">محتوى الرسالة  </label>
                        <textarea type="text" class="form-control content" required rows="4" name="message" id="message" placeholder="محتوى الرسالة"></textarea>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-block sendemail" value="ارسال">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" value="" class="cargo_code" name="cargo_code">
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>


<div id="classModalOrderWafirha" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width: 83%;margin-right:16%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
          </button>
          <h4 class="modal-title" id="classModalLabel">
             منتجات الطلب
          </h4>
        </div>
        <div class="modal-body">
          <table id="demo-dynamic-tables-2" class="table table-middle">
            <thead>
                <th> رقم الطلب  </th>
                <th> المنتج </th>
                <th> الرابط </th>
                <th>الكمية</th>
                <th>سعر المنتج </th>
                <th>ملاحظة</th>
                <th>رابط التتيع</th>
                <th>رقم التتيع</th>
                <th>الحالة</th>
                <th></th>
            </thead>
            <tbody class="show_data_wafirha_prd">

            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">
            اغلاق الصفحة
          </button>
        </div>
      </div>
    </div>
</div>

<!-- Edit product --->
<div id="myModalEditProduct" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row getproduct_info">

                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block update_fastbuyf">  تحديث معلومات المنتج </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalTracking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="companyurl"> رابط التتبع </label>
                        <input type="hidden" id="productid" name="productid" value="">
                        <input type="hidden" id="orderId" name="orderId" value="">
                        <input type="text" class="form-control" name="companyurl" id="companyurl" placeholder="رابط التتبع"/>
                    </div>
                    <div class="form-group">
                        <label for="trackinginfo"> رقم التتبع  </label>
                        <input type="text" class="form-control" name="trackinginfo" id="trackinginfo" placeholder="رقم التتبع"/>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            اغلاق الصفحة
                </button>
                <button type="button" class="btn btn-primary submittrackinginfo">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalShowDeclaration" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
            <table role="table" class="table table-purchase">
                <thead role="rowgroup" border-spacin="10">
                    <tr role="row">
                        <th>كود الشحنة </th>
                        <th> اسم المنتج </th>
                        <th> تكلفة المنتج </th>
                        <th>الكمية</th>
                        <th>تاريخ الاضافة </th>
                    </tr>
                </thead>
                <tbody class="result_decla">

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">اغلاق الصفحة</button>
        </div>
      </div>
    </div>
</div>

<div id="myModalShowProduct" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> الشحنة </th>
                        <th scope="col">كود الشحنة </th>
                        <th scope="col">كود المنتج</th>
                        <th scope="col">الشحنة</th>
                        <th scope="col">الوزن / الأبعاد</th>
                        <th scope="col">الوصف</th>
                    </tr>
                </thead>
                <tbody class="getlist_s">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="myModalPacking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات تعليف الشحنة
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="formPack">
                    <div class="form-group">
                        <label> وحدة القياس</label>
						<select class="form-control" name="ship_unity" required="true">
							<option value="kg">Kg</option>
							<option value="lbg">Lbs</option>
						</select>
                    </div>
                    <div class="form-group">
                        <label for="weight"> وزن الشحنة </label>
                        <input type="hidden" id="packign_id" name="packign_id" value="">
                        <input type="number" class="form-control" name="weight" required="true" id="weight" placeholder="وزن الشحنة"/>
                    </div>
                    <div class="form-group">
                        <label for="length"> الطول  </label>
                        <input type="number" class="form-control" name="length" required="true" id="length" placeholder="الطول"/>
                    </div>
                    <div class="form-group">
                        <label for="width"> العرض  </label>
                        <input type="number" class="form-control" name="width" required="true" id="width" placeholder="العرض"/>
                    </div>
                    <div class="form-group">
                        <label for="height"> الارتفاع  </label>
                        <input type="number" class="form-control" name="height" required="true" id="height" placeholder="الارتفاع"/>
                    </div>
                    <div class="form-group">
                        <label for="trackinginfo"> صور الشحنة  </label>
                        <input type="file" class="form-control" id="fileToUpload" name="images" required="true" multiple id="images"/>
                    </div>
                    @csrf
                    <div class="form-group">
                        <button type="button" name="submit" class="btn btn-primary btn-block packFinish">
                            حفط المعلومات
                        </button>
                        <img class="loading_icons" style="height:60px;width:60px;margin-left: auto; text-align: center;margin-right:auto;display:none;" src="{{asset('assets/user/img/loading.gif')}}"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalTracking_Info" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="trackings" role="form">
                    <div class="form-group">
                        <label for="tracking_info"> رقم التتبع  </label>
                        <input type="text" class="form-control trackinginfo" readonly required="true" name="tracking_info" placeholder="رقم التتبع"/>
                    </div>
                    <div class="form-group">
                        <label for="message"> مكان وصول الشحنة </label>
                        <input type="hidden" class="cargoflight_id cargo_id" id="cargo_id" name="cargoflight_id" value="">
                        <textarea class="form-control message_track" rows="3" id="message" required="true" name="message"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_tracking_info">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProductImage" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="images" role="form">
                    <div class="form-group">
                        <label> الخدمة المطلوبة </label>
						<select class="form-control service_s" required ="true" name="service_s" required="true">
                            <option value="1"> خدمة اخرى </option>
                            <option value="2">صور اضافية </option>
                            <option value="3">رفع فيديو</option>
                        </select>
                        <input type="hidden" id="product_id" name="product_id" value="">
                        <input type="hidden" id="service_id" name="service_id" value="">
                    </div>
                    <div class="form-group photo">
                        <label for="company_url"> صور اضافية </label>
                        <input type="file" class="form-control" name="more_images" id="more_images" multiple />
                    </div>
                    <div class="form-group video">
                        <label for="company_url"> رفع فيديو </label>
                        <input type="file" class="form-control" name="more_video" id="more_video"/>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_product_img">
                    تنفيد الطلب
                 </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalProductReturn_price" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="price_return" role="form">
                    <div class="form-group">
                        <label for="price_topay"> مصاريف الشحن </label>
                        <input type="number" placeholder="مصاريف الشحن" class="form-control" name="price_topay" id="price_topay"/>
                        <input type="hidden" class="return_id" id="return_id" name="return_id" value="">
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_product_return">
                    اضافة سعر الشحن
                 </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProductReturn_Track" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="track_return" role="form">
                    <div class="form-group">
                        <label for="company_urls"> رابط التتبع </label>
                        <input type="text" placeholder="رابط التتبع" class="form-control" name="company_urls" id="company_urls"/>
                        <input type="hidden" class="return_id" id="return_id" name="return_id" value="">
                    </div>
                    <div class="form-group">
                        <label for="company_track"> رقم التتبع </label>
                        <input type="text" placeholder="رقم التتبع" class="form-control" name="company_track" id="company_track"/>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_track_return">
                   اضافة رقم التتيع
                 </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalUserPolicia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="resp_error"></div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="userpolicia"> رقم حساب العميل </label>
                    <input type="text" placeholder="رقم حساب العميل" class="form-control userpolicia" name="userpolicia" id="userpolicia"/>
                    <input type="hidden" class="shippingid" id="shippingid" name="shippingid" value="">
                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block update_shipping"> ربط الشحنة  </button>
            </div>
        </div>
    </div>
</div>


<div id="myModalShowDetail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> اسم الخدمة  </th>
                        <th scope="col">رقم الطلب  </th>
                        <th scope="col">سعر الطلب </th>
                        <th scope="col">كود المعاملة</th>
                        <th scope="col">التاريخ </th>
                        <th scope="col">الحالة</th>
                    </tr>
                </thead>
                <tbody class="getlist_details">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div id="myModalShowDirectShip" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>

                        <th scope="col"> # </th>
                        <th scope="col"> رقم الطلب </th>
                        <th scope="col">وزن الشحنة </th>
                        <th scope="col">ابعاد الشحنة </th>
                        <th scope="col">محتوى الشحنة</th>
                        <th scope="col">قيمة الشحنة</th>
                    </tr>
                </thead>
                <tbody class="getlist_directShip">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div class="modal fade" id="myModalDirectShipTracking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="tracking_direct" role="form">
                    <div class="form-group">
                        <label for="company_name_direct"> شركة الشحن </label>
                        <input type="hidden" id="directship_id" name="directship_id" value="">
                        <input type="text" class="form-control company_name_direct" required="true" name="company_name_direct" id="company_name_direct" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="tracking_number_direct"> رقم التتبع  </label>
                        <input type="text" class="form-control" required="true" name="tracking_number_direct" id="tracking_number_direct" placeholder="رقم التتبع"/>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_tracking_direct">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div id="classModalParcelList" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfos" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width: 83%;margin-right:16%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
          </button>
          <h4 class="modal-title" id="classModalLabel">
            شحنات الرحلة
          </h4>
        </div>
        <div class="modal-body">
          <table id="demo-dynamic-tables-2" class="table table-middle">
            <thead>
                <th>الصورة</th>
                <th> كود الشحنة</th>
                <th>رقم العميل</th>
                <th>تاريخ الاضافة</th>
                <th>الوزن / الأبعاد</th>
                <th>الدولة </th>
                <th> التتبع </th>
                <th> المحتوى </th>
                <th> السعر </th>
                <th> الحالة </th>
                <th> الدفع </th>
            </thead>
            <tbody class="show_data_ship_list">

            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">
            اغلاق الصفحة
          </button>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="myModalAddUser" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    املأ الخانات التالية لإنشاء حساب مجاني
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="modal-body registerfrom">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstname">الاسم الشخصي <samp class="asterisk">*</samp> </label>
                                <input id="firstname" name="firstname" class="form-control" type="text" placeholder="الاسم الشخصي" required="true" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastname">الاسم العائلي <samp class="asterisk">*</samp> </label>
                                <input id="lastname" name="lastname" class="form-control" type="text" placeholder="الاسم العائلي" required="true" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="second_firstname"> اسم الأب  <samp class="asterisk">*</samp> </label>
                                <input id="second_firstname" name="second_firstname" class="form-control" type="text" placeholder="إسم الأب" required="true" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="second_lastname"> إسم الجد  <samp class="asterisk">*</samp> </label>
                                <input id="second_lastname" name="second_lastname" class="form-control" type="text" placeholder="إسم الجد " required="true" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="country">اختر الدولة <samp class="asterisk">*</samp> </label>
                                <select class="form-control country" name="country" required="true" id="country">
                                    <option value="ليبيا">ليبيا</option>
                                    <!--<option value="مصر">مصر</option>
                                    <option value="تونس">تونس</option>
                                    <option value="المغرب">المغرب</option>
                                    <option value="الجزائر">الجزائر</option>
                                    <option value="السعودية">السعودية</option>
                                    <option value="انجلترا">انجلترا</option>
                                    <option value="امريكيا">امريكيا</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="city"> مدينة الاستلام <samp class="asterisk">*</samp> </label>
                                <select class="form-control" name="city" required="true" id="mycity">
                                    <option value="">اختر المدينة</option>
                                    <option value="طرابلس">طرابلس</option>
                                    <option value="بنغازي">بنغازي</option>
                                    <!--<option value="مصراتة">مصراتة</option>
                                    <option value="البيضاء">البيضاء</option>
                                    <option value="الزاوية">الزاوية</option>
                                    <option value="زليتن">زليتن</option>
                                    <option value="أجدابيا">أجدابيا</option>
                                    <option value="طبرق">طبرق</option>
                                    <option value="سبها">سبها</option>
                                    <option value="الخمس">الخمس</option>
                                    <option value="درنة">درنة</option>
                                    <option value="صبراتة">صبراتة</option>
                                    <option value="زوارة">زوارة</option>
                                    <option value="الكفرة">الكفرة</option>
                                    <option value="المرج">المرج</option>
                                    <option value="ترهونة">ترهونة</option>
                                    <option value="سرت">سرت</option>
                                    <option value="غريان">غريان</option>
                                    <option value="مسلاتة">مسلاتة</option>
                                    <option value="بني وليد">بني وليد</option>
                                    <option value="الزنتان">الزنتان</option>
                                    <option value="الجميل">الجميل</option>
                                    <option value="صرمان">صرمان</option>
                                    <option value="شحات">شحات</option>
                                    <option value="أوباري">أوباري</option>
                                    <option value="يفرن">يفرن</option>
                                    <option value="الأبيار">الأبيار</option>
                                    <option value="رقدالين">رقدالين</option>
                                    <option value="القبة">القبة</option>
                                    <option value="تاورغاء">تاورغاء</option>
                                    <option value="الماية">الماية</option>
                                    <option value="مرزق">مرزق</option>
                                    <option value="البريقة">البريقة</option>
                                    <option value="هون">هون</option>
                                    <option value="جالو">جالو</option>
                                    <option value="العجيلات">العجيلات</option>
                                    <option value="نالوت">نالوت</option>
                                    <option value="سلوق">سلوق</option>
                                    <option value="زلطن">زلطن</option>
                                    <option value="مزدة">مزدة</option>
                                    <option value="راس لانوف">راس لانوف</option>
                                    <option value="العربان">العربان</option>
                                    <option value="ودان">ودان</option>
                                    <option value="توكرة">توكرة</option>
                                    <option value="براك">براك</option>
                                    <option value="غدامس">غدامس</option>
                                    <option value="غات">غات</option>
                                    <option value="أوجلة">أوجلة</option>
                                    <option value="سوسة">سوسة</option>-->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone">رقم الهاتف <samp class="asterisk">*</samp> </label>
                                <input id="phonenumber" name="phone" class="form-control phonenumbers allow_numeric" type="tel" value="09" maxlength="10" placeholder="رقم الهاتف" required="true" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">البريد الالكتروني <samp class="asterisk">*</samp> </label>
                                <input class="form-control emails" id="email" name="email" type="email" placeholder="البريد الالكتروني" required="true" />
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label for="plan">نوع الباقة <samp class="asterisk">*</samp> </label>
                        <select class="form-control" name="plan" required="true">
                            <option value="0"> اشتراك مجاني  </option>
                        </select>
                    </div>-->
                    <div class="form-group">
                        <label for="address">عنوان السكن <samp class="asterisk">*</samp> </label>
                        <input class="form-control fulladdress" id="fulladdress" name="address" type="text" placeholder="عنوان السكن" required="true" />
                    </div>
                    <div class="form-group">
                        <label for="password">كلمة المرور <samp class="asterisk">*</samp> </label>
                        <input class="form-control plainPasswords" id="plainPassword" name="password" type="password" placeholder="كلمة المرور" required="true"/>
                    </div>
                    <div class="form-group">
                        <label> <input type="radio" name="terms" value="1" required="true" /> <span style="display: inline;">لقد قرأت ووافقت على الأحكام و الشروط </span><a href="{{url('terms')}}">إعرض الشروطٍ</a> </label>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" id="register-form" class="btn btn-register btn-default btn-block registerform">إنشاء الحساب</button>
            </div>
        </div>
    </div>
</div>
