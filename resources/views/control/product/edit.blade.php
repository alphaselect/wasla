@extends('layouts.admin')
@section('content')
<div class="layout-content" >
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">  تحرير المنتج  </h2>
			</div>
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success_add'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a href="{{ url('control/product') }}" class="btn btn-info"> كل المنتجات  </a>
            <form data-toggle="validator" role="form" method="POST" action="{{ url('control/product/edit')}}" enctype="multipart/form-data">
                <div class="col-lg-2"></div>
				<div class="col-lg-8">
                    @csrf
					<div class="form-group">
						<label> اسم المنتج </label>
						<input type="text" name="product_name" value="{{$product->product_name}}" required class="form-control"/>
					</div>
					<div class="form-group">
						<label> سعر المنتج </label>
						<input type="number" name="product_price" value="{{$product->product_price}}" required class="form-control"/>
					</div>
					<div class="form-group">
						<label> الكمية </label>
						<input type="number" name="product_qty" value="{{$product->product_qty}}" required class="form-control"/>
					</div>
					<div class="form-group">
						<label>صور المنتج </label>
                        <input type="file" name="product_image[]" multiple class="form-control"/>
                        <input type="hidden" name="product_id" value="{{$product->id}}"/>
                    </div>
                    <div class="form-group">
						<label>وصف المنتج </label>
						<textarea name="product_desc" placeholder="وصف المنتج" class="form-control content" required>
                            {{$product->product_desc}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"> تحرير المنتج </button>
                    </div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
