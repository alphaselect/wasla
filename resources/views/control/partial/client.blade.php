@foreach ($clients as $client )
<tr id="{{ $client->id }}">
    <td data-order="Jessica Brown">
        <span class="icon-with-child m-r">
            <img class="circle" width="36" height="36" src="@if($client->thumbnail != "") {{asset('assets/user/img/'.$client->thumbnail)}} @else {{asset('assets/admin/img/0180441436.jpg')}} @endif" alt="{{$client->firstname}}"/>
        </span>
    </td>
    <td class="maw-320">
        <span class="truncate">
            {{ $client->policia }}
        </span>
    </td>
    <td class="maw-320">
        <span class="truncate"> {{ $client->firstname }} {{ $client->lastname }}</span>
    </td>
    <td class="maw-320">
        <span class="truncate"> {{ $client->email }}</span>
    </td>
    <td class="maw-320">
        <span class="truncate">{{ $client->getcountry->countriename }}</span>
    </td>
    <td class="maw-320">
        <span class="truncate"> {{ $client->city }}</span>
    </td>
    <td class="maw-320">
        <span class="truncate"> {{ $client->phone }}</span>
    </td>
    <td class="maw-320">
        <span class="truncate"> {{ $client->created_at->format('Y-m-d') }}</span>
    </td>
    <td>
        @if($client->status == 0 )
            <span class="label label-info label-pill">غير مفعل</span>
        @elseif($client->status == 1)
            <span class="label label-success label-pill">نشيط</span>
        @else
            <span class="label label-primary label-pill">موقف</span>
        @endif
    </td>
  <td>
    <div class="btn-group pull-right dropdown">
      <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
        <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
            @if($client->status == 0 || $client->status == 2)
                <li><a href="#" class="btn-link operaccount 1" id="{{ $client->id }}">تفعيل الحساب</a></li>
            @elseif($client->status == 1)
                <li><a href="#" class="btn-link operaccount 2" id="{{ $client->id }}">توقيف الحساب</a></li>
            @endif
            <li><a href="#" class="btn-link sendEmail " id="{{ $client->id }}"> ارسال رسالة </a></li>
      </ul>
    </div>
  </td>
</tr>
@endforeach
