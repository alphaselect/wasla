<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('twitter');
            $table->float('taux_purchase',8,3);
            $table->float('taux_purchase_free',8,3);
            $table->float('exchange',8,3);
            $table->float('insurance_costs',8,3);
            $table->float('handling_cost',8,3);
            $table->text('description');
            $table->string('contact_phone_tripoli');
            $table->string('contact_phone_benghazi');
            $table->string('contact_email_tripoli');
            $table->string('contact_email_benghazi');
            $table->text('maps_iframe_tripoli');
            $table->text('maps_iframe_benghazi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
