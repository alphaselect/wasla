<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| file ....
*/

//Auth::routes();
//use App\Mail\Sendmail;

Route::get('/', 'HomeController@index')->name('index');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/contactus', 'HomeController@contactus')->name('contactus');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/modepay', 'HomeController@modepay')->name('modepay');
Route::get('/about', 'HomeController@about')->name('about');

//login //register //resetpassword
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('verify');
Route::post('/verify_phone', 'Auth\RegisterController@verify_phone')->name('verify_phone');
Route::post('/reset', 'Auth\RegisterController@reset')->name('reset');
Route::get('/resetpassword/{token}', 'Auth\RegisterController@resetpassword')->name('resetpassword');
Route::post('/changepassword', 'Auth\RegisterController@changepassword')->name('changepassword');

//account
Route::get('/account','AccountController@account');
Route::get('/profile','AccountController@profile');
Route::post('/update_profile','AccountController@update_profile');
Route::post('/verifier_profile','AccountController@verifier_profile');

Route::get('/calculator','AccountController@calculator');
Route::get('/plan','AccountController@plan');
Route::get('/contact','AccountController@contact');
Route::post('/contact','AccountController@sendemail');

Route::post('/calculator_calcule','CalculatorController@calcule_price');

//cart
Route::get('/cart','CartController@cart');
Route::post('/cart','CartController@add');
Route::get('/destroy/{id}','CartController@destroy');

//transaction
Route::get('/transaction','TransactionController@transaction');
Route::get('/addfunds','TransactionController@addfunds');

Route::get('/purchase/payer_purchase','TransactionController@payer_purchase');
Route::post('/purchase/payer_purchase','TransactionController@payer_purchase');
Route::post('/plan/upgrade','TransactionController@plan_upgrade');
Route::post('/plan/plan_duration_price','TransactionController@plan_duration_price');
Route::post('/paynowshipping','TransactionController@paynowshipping');

//shipping
Route::get('/shipment','ShipController@shipment');
Route::get('/ready','ShipController@ready');
Route::get('/delivered','ShipController@delivered');

Route::POST('/addinformation','ShipController@addinformation');
Route::POST('/delared','ShipController@delared');
Route::POST('/addservices','ShipController@addservices');
Route::POST('/acceptshipping','ShipController@acceptshipping');
Route::POST('/specialservice','ShipController@specialservice');


//purchase
Route::get('/purchase','PurchaseController@allorder');
Route::get('/neworder','PurchaseController@neworder');
Route::post('/addorder','PurchaseController@addorder');
Route::post('/view-order','PurchaseController@viewOrder');
Route::post('/purchase/search','PurchaseController@search');
Route::get('purchase/cancel_purchase','PurchaseController@cancel_purchase');
//product
Route::get('/product','ProductController@product');
Route::post('/getinfo','ProductController@getinfo');

//control
Route::prefix('control')->group(function() {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('control.auth.login');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('control.auth.logout');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('control.auth.login.submit');
    Route::post('/register', 'Auth\AdminLoginController@register')->name('control.auth.register.submit');
    Route::post('/adduser', 'Auth\AdminLoginController@adduser');

    Route::get('/', 'Admin\AdminController@index')->name('control.index');
    Route::get('/profile', 'Admin\AdminController@profile')->name('control.profile');

    //user
    Route::get('/users', 'Admin\AdminController@users')->name('control.settings.users');
    Route::get('/clients', 'Admin\AdminController@clients')->name('control.clients');
    Route::get('/identity', 'Admin\AdminController@identity')->name('control.identity');
    Route::POST('/operaccount', 'Admin\AdminController@operaccount');
    Route::POST('/operidentity', 'Admin\AdminController@operidentity');

    //email
    Route::POST('/sendemail', 'Admin\AdminController@sendemail');

    //shipping
    Route::get('/import', 'Admin\ShipController@import')->name('control.shipping.import');
    Route::post('/import', 'Admin\ShipController@GetallUser');
    Route::post('/signleUser', 'Admin\ShipController@signleUser');
    Route::post('/add_import', 'Admin\ShipController@add_import');

    Route::get('/shipping/newshipment', 'Admin\ShipController@newshipment')->name('control.shipping.newshipment');
    Route::get('/shipping/unknow', 'Admin\ShipController@unknow')->name('control.shipping.unknow');
    Route::get('/shipping/ontheway', 'Admin\ShipController@ontheway')->name('control.shipping.ontheway');
    Route::get('/shipping/ready', 'Admin\ShipController@ready')->name('control.shipping.ready');
    Route::get('/shipping/delivred', 'Admin\ShipController@delivred')->name('control.shipping.delivred');
    Route::post('/shipping/getdeclaration', 'Admin\ShipController@getdeclaration');
    Route::post('/shipping/all/addparceltopolicia', 'Admin\ShipController@addparceltopolicia');
    Route::post('/shipping/all/selectedparcel', 'Admin\ShipController@selectedparcel');
    Route::get('/shipping/service', 'Admin\ShipController@service')->name('control.shipping.service');
    Route::post('/shipping/opservice', 'Admin\ShipController@opservice');
    Route::post('/shipping/p_shipped', 'Admin\ShipController@p_shipped');
    Route::post('/shipping/delivred_p', 'Admin\ShipController@delivred_p');


    //purchase
    Route::get('/purchase/new', 'Admin\PurchaseController@new')->name('control.purchase.new');
    Route::get('/purchase/waiting', 'Admin\PurchaseController@waiting')->name('control.purchase.waiting');
    Route::get('/purchase/proccess', 'Admin\PurchaseController@proccess')->name('control.purchase.proccess');
    Route::get('/purchase/purchase', 'Admin\PurchaseController@purchase')->name('control.purchase.purchase');
    Route::get('/purchase/completed', 'Admin\PurchaseController@completed')->name('control.purchase.completed');

    Route::post('/purchase/getprd', 'Admin\PurchaseController@getprd');
    Route::post('/purchase/editprd', 'Admin\PurchaseController@editproduct');
    Route::post('/purchase/updateprd', 'Admin\PurchaseController@updateprd');
    Route::post('/purchase/new', 'Admin\PurchaseController@accept');
    Route::post('/purchase/addtracking', 'Admin\PurchaseController@addtracking');
    Route::post('/purchase/prd_arrive', 'Admin\PurchaseController@prd_arrive');

    //
    Route::get('/shipping/all/{id}', 'Admin\ShipController@all')->name('control.shipping.all');
    Route::get('/shipping/cargoflights', 'Admin\ShipController@cargoflights')->name('control.shipping.cargoflights');
    Route::post('/shipping/cargoflights', 'Admin\ShipController@savepolicia');
    Route::post('/shipping/shiplist', 'Admin\ShipController@shiplist');
    Route::post('/shipping/tracking_info', 'Admin\ShipController@tracking_info');


    //product
    Route::get('/product', 'Admin\ProductController@list')->name('control.product.index');
    Route::get('/product/create', 'Admin\ProductController@create')->name('control.product.create');
    Route::get('/product/edit/{id}', 'Admin\ProductController@edit')->name('control.product.edit');
    Route::get('/product/delete/{id}', 'Admin\ProductController@delete');
    Route::get('/product/backup/{id}', 'Admin\ProductController@backup');
    Route::post('/product/create', 'Admin\ProductController@store');
    Route::post('/product/edit', 'Admin\ProductController@update');


    //address
    Route::get('/address','Admin\AddressController@list')->name('control.adress.index');
    Route::get('/address/create','Admin\AddressController@create')->name('control.address.create');
    Route::get('/address/edit/{id}','Admin\AddressController@editaddress')->name('control.address.edit');
    Route::POST('/address/create','Admin\AddressController@store');
    Route::POST('/address/edit','Admin\AddressController@updateaddress');
    Route::POST('/address/action','Admin\AddressController@action');


    //transactions
    Route::get('/transaction','Admin\TransactionController@transaction')->name('control.transaction');
    Route::post('/addfunds','Admin\TransactionController@addfunds');

    //pages
    Route::get('/terms','Admin\PagesController@terms')->name('control.pages.terms');
    Route::get('/privacy','Admin\PagesController@privacy')->name('control.pages.privacy');
    Route::get('/faq','Admin\PagesController@faq')->name('control.pages.faq');
    Route::get('/about','Admin\PagesController@about')->name('control.pages.about');
    Route::get('/methode','Admin\PagesController@methode')->name('control.pages.methode');

    Route::POST('/terms', 'Admin\PagesController@saveterms');
    Route::POST('/privacy', 'Admin\PagesController@saveprivacy');
    Route::POST('/about', 'Admin\PagesController@saveabout');
    Route::POST('/methode', 'Admin\PagesController@savemethode');
    Route::POST('/addquestion', 'Admin\PagesController@addquestion');
    Route::get('/faq/{id}','Admin\PagesController@delete');

    //settings
    Route::get('/general','Admin\SettingController@general')->name('control.settings.general');



});
