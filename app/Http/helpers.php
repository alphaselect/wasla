<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

function generateRandomNumber($length) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
}

//
function generateRandomString($length) {
     $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
     $charactersLength = strlen($characters);
     $randomString = '';
     for ($i = 0; $i < $length; $i++) {
         $randomString .= $characters[rand(0, $charactersLength - 1)];
         }
         return $randomString;
 }


function flaotNnumber($number){
	//if(is_float($number)) {
    if($number && intval($number) != $number){
        $number= number_format($number, 1);
        $number_ex=explode('.',$number);
        $number_s=$number_ex[1]; //float
        $number_p=$number_ex[0]; //int
	    if($number_s>5 && $number_p>=1 || ($number_s>5 && $number_p==0)){
		    $number=$number_p+1;
		}else if($number_s<5 && $number_p>=1){
			 $number=$number_ex[0] + 0.5;
		}else if($number_p==0 && $number_s<=5){
				 $number=0.5;
		}
		 	return $number;
	}else {
			 return $number;
    }


};

    function forma_date_dmy($date){
        $date_s = str_replace('-', '/', $date);
        $date_r = date("Y-m-d", strtotime($date_s));
        return $date_r;
    };

    function get_taux_purchase(){
       $settings= DB::table("settings")->select('taux_purchase')->first();
       if(!empty($settings)){
            return $settings->taux_purchase;
       }else{
           return 0;
       }

    }

    function get_pricePurchaseTotale($purchase){
       return ($purchase->purchases_price() + $purchase->internal_shipping_price + $purchase->tax)*(get_taux_purchase()/100 + 1);
    }

    function get_exchange(){
        $settings= DB::table("settings")->select('exchange')->first();
        if(!empty($settings)){
             return $settings->exchange;
        }else{
            return 0;
        }
    }

    function round_up($value, $places=1) {
        if ($places < 0) { $places = 0; }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }
    function round_down($value, $places=1) {
        if ($places < 0) { $places = 0; }
        $mult = pow(10, $places);
        return floor($value * $mult) / $mult;

    }

    function get_solde(){
        $solde = Auth::user()->payment->where('status','0')->first();
        //if(!empty($solde)){
        return $solde->funds_total;
        //}else{
            //$solde=0;
       // }
        //return $solde;
    }

    function generate_policia($code='AD202'){
        $alphabetcode=substr($code,0,-3);
        $intcode=(int)substr($code,-3);
        if($intcode<999){
             $intcode=$intcode+1;
        }else{
            $alphabetcode++;
            $intcode=0;
        }
        $count= strlen($intcode);
        if($count==1){
            $intcode= '00'.$intcode;
        }elseif($count==2){
            $intcode= '0'.$intcode;
        }elseif($count==3){
            $intcode= $intcode;
        }
        return $alphabetcode.$intcode;

    };
?>
