<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
       // return view('auth.login');
    }

    protected function login(Request $request){

        $data = $request->all();
        //dd($data);
        $validator = Validator::make($data,[
            'lg_username' => 'required|string|email',
            'lg_password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            )); // 400 being the HTTP code for an invalid request.

        }else{
            //$credentials = $request->only('email', 'password');
            if (Auth::attempt(['email'=> $request->lg_username,'password' => $request->lg_password ,'status' => 1])){
                return Response::json([
                    'redirect' => true,
                    'success' => true
                ], 200);
            }else{
                return Response::json(array(
                    'redirect' => false,
                    'success' => false
                ),200);
            }
        }

    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
