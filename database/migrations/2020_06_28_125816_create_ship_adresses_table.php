<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_adresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_address');
            $table->string('Adressline');
           // $table->string('Adresslinetwo');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('mobile');
           // $table->text('fulladdress');
            $table->string('country');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ship_adresses');
    }
}
