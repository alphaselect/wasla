@extends('layouts.app')

@section('content')


<div class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="header-login">
                <ul>
                    @if(!Auth::check())
                        <li>
                            <button data-toggle="modal" data-target="#login-modal" class="button-login"><i class="fa fa-user"></i>تسجيل الدخول</button>
                        </li>
                        <li>
                            <button data-toggle="modal" data-target="#registration-modal" class="button-subscrip"> <i class="fa fa-user"></i> الإشتراك بالخدمة</button>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="text-slider">
            <h1>شركة <span class="bigtext"> الوصلة العالمية</span> </h1>
            <h3>لخدمات الشحن  و  التسوق  عبر  الإنترنت</h3>
            <h3>خيارك الأفضل دائمًا!</h3>
        </div>
        </div>
       </div>

</div> <!--and slider -->
<div class="container">

     <div class="widget">
        <div class="container-widget">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="widget-content">
                        <!--<img src="images/icon/sin-in.png" />-->
                        <i class="icon-sin-in"></i>
                        <h3>1. قم بالإشتراك لدينا</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="widget-content">
                        <i class="icon-adress"></i>
                        <h3>2. تحصل على عنوانك بالخارج</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="widget-content">
                        <!--<i class="icon"><img src="images/icon/shipin.png" /></i>-->
                        <i class="icon-shipin"></i>
                        <h3>3. استلم شحنتك في ليبيا</h3>
                    </div>
                </div>
            </div>
        </div>
    </div> <!--end widget -->
    <div class="logo-hom">
        <h1 class="h-home">
            لماذا عليك إستخدام خدمة الوصلة العالمية
        </h1>
        <img class="logo"  src="{{asset('assets/front/images/logo.png')}}"/>
    </div><!--end logo--hom-->
    <div class="text-home">
        <p>تختص شركة الوصلة العالمية في تقديم خدمات التسوق الإلكتروني والشحن حيث تقدم لعملائها الكرام عناوين شحن في الخارج يمكن لهم إستخدامها أثناء تسوق الانترنت كذلك تقدم خدمة الدفع الإلكتروني لمن يرغب في شراء منتجات/خدمات من المواقع العالمية عبر الإنترنت، من الخدمات التي تقدمها شركة الوصلة العالمية هي ترويج صفحات الفيسبوك حيث نقدم لكم باقات متنوعة من الإعلانات الممولة عبر فيسبوك للصفحات والمنشورات كي تتحصل على عدد أكبر من المشاهدين والمتابعين لصفحتك الخاصة.</p>
    </div>
    <h1 class="h-home">خدمات نقدمها</h1>
</div>
<!--end container -->
<div class="services" id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="services-content">
                    <div class="cercle">
                        <img src="{{ asset('assets/front/images/icon/ship.png') }}" />
                    </div>
                    <h1>شحن الطرود</h1>
                    <p>احصل على عنوانك الخاص بالخارج، واستلم الطرود بإحدى مقار شركتنا في ليبيا. </p>
                </div><!--end services-content-->
            </div>
            <div class="col-md-4">
                <div class="services-content">
                    <div class="cercle">
                        <img src="{{ asset('assets/front/images/icon/ship.png') }}" />
                    </div>
                    <h1>الدفع الالكتروني</h1>
                    <p>تسوق و ابحث عما ترعب من المنتجات عبر الأنترنت و أترك لنا عملية الدفع الالكتروني </p>
                </div><!--end services-content-->
            </div>
            <div class="col-md-4">
                <div class="services-content">
                    <div class="cercle">
                        <img src="{{ asset('assets/front/images/icon/pub.png') }}" />
                    </div>
                    <h1>الإعلانات الممولة</h1>
                    <p>خدمة الإعلانات الممولة عبر مواقع التواصل الإجتماعي مثل Facebook و Instagram.</p>
                </div><!--end services-content-->
            </div>
        </div>
    </div>
</div>
<!--end services -->

<div class="calculator">
    <div class="container">
        <h1 class="h-home"> حاسبة الأسعار </h1>
        <div class="calculator-form">
        <form action="{{url('/calculator')}}" id="calculatorform" method="post">
        <div class="calculatorform">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="country" name="country">
                            <option value="">دولة الشحن</option>
                            <option value="usa">امريكا </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="prd_price" name="prd_price" placeholder="سعر المنتج" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="weight" name="weight" placeholder="وزن الشحنة" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="width" name="width" placeholder="العرض" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control input-control" id="type_account" name="type_account" required>
                            <option value=""> نوع الحساب  </option>
                            <option value="0"> اشتراك مجاني </option>
                            <option value="1"> اشتراك توفير </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="length" name="length" placeholder="الطول" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-control" id="height" name="height" placeholder="الأرتفاع" required>
                    </div>
                </div>
            </div>
            @csrf
            <div class="b text-center">
                <button type="button" class="btn btn-front btn-calcule" style="float: none;">
                    حساب تكلفة الشحن
                </button>
            </div>
        </div>
    </form>
    <div class="resulte ">
        <div class="table-responsive col-sm-12">
            <table class="table table-bordered hide">
                <thead>
                    <tr>
                    <th >نوع الحساب</th>
                    <th >سعر المنتج</th>
                    <th >الوزن الفعلي</th>
                    <th >الوزن الحجمي</th>
                    <th ><strong>تكلفة الشحن</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td class="account_type"></td>
                        <td class="prd_price"></td>
                        <td class="actual_weight"></td>
                        <td class="volumetric_charge"></td>
                        <td class="price"></td>
                        <!--<span class="price"></span>-->
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="clear"></div>
    </div>
        </div>
    </div>
</div>
<!--end calculatrice-->
<div class="container">
    <div class="prices nprices">
        <h1 class="h-home">أسعار الباقات</h1>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="price-content saving-account">
                    <h3 class="h-priceaccount">حساب التوفير</h3>
                    <div class="price-txt">
                        <table>
                            <thead></thead>
                            <tbody>
                                <tr><td>سنويا </td><td>:100$</td></tr>
                                <tr><td>تجمبع الشحنات </td><td>:2$</td></tr>
                                <tr><td>صور الشحنة </td><td>:5$</td></tr>
                                <tr><td>خدمات خاصة </td><td>:8%</td></tr>
                                <tr><td>الشراء من موقع اخر&nbsp;&nbsp;&nbsp;</td><td>:10%</td></tr>
                                <tr><td>سقف التأمين </td><td>:2000$</td></tr>
                                <tr><td>الشحن الجوي </td><td>:13$</td></tr>
                                <tr><td>الشحن البحري </td><td>:3.5$</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="price-content free-account">
                    <h3 class="h-priceaccount">حساب مجاني</h3>
                    <div class="price-txt">
                        <table>
                            <thead></thead>
                            <tbody>
                                <tr><td>سنويا </td><td>:مجانا</td></tr>
                                <tr><td>تجمبع الشحنات </td><td>:5$</td></tr>
                                <tr><td>صور الشحنة </td><td>:8$</td></tr>
                                <tr><td>خدمات خاصة </td><td>:12%</td></tr>
                                <tr><td>الشراء من موقع اخر&nbsp;&nbsp;&nbsp;</td><td>:--</td></tr>
                                <tr><td>سقف التأمين </td><td>:1000$</td></tr>
                                <tr><td>الشحن الجوي </td><td>:15$</td></tr>
                                <tr><td>الشحن البحري </td><td>:4.5$</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end pricing-->

<div class="ratings" id="ratings">
    <h1 class="h-home"> ماذا قال العملاء عن خدماتنا </h1>
        <div class="rating-carousel">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="{{asset('assets/front/images/slider/2.jpg')}}" alt="..." class="profile">
                  <div class="carousel-caption">
                  <h2>امس شريت</h2>
                  <h3>استفدت</h3>
                    <p>امس شريت من eBay عن طريقكم، استفدت من عرض شهر أغسطس من غير عمولة ، بارك الله فيكم موفرين سهولة وسرعة في الدفع وتعامل لا قوة الا بالله ،، استمروا</p>
                  </div>
                </div>
                <div class="item">
                  <img src="{{asset('assets/front/images/slider/3.jpg')}}" alt="..." class="profile">
                  <div class="carousel-caption">
                  <h2>امس شريت</h2>
                  <h3>استفدت</h3>
                    <p>امس شريت من eBay عن طريقكم، استفدت من عرض شهر أغسطس من غير عمولة ، بارك الله فيكم موفرين سهولة وسرعة في الدفع وتعامل لا قوة الا بالله ،، استمروا</p>
                  </div>
                </div>

              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

        </div>
</div>
@endsection
