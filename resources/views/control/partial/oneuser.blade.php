
<li class="messenger-list-item">
    <a class="messenger-list-link" data-id="{{$oneuser->id }}" href="#0601274412" data-toggle="tab">
        <div class="messenger-list-avatar">
        <img class="rounded" width="40" height="40" src="@if($oneuser->thumbnail){{asset('users/thumbnails/'.$oneuser->thumbnail)}} @else {{asset('assets/img/0180441436.jpg')}} @endif">
        </div>
        <div class="messenger-list-details">
        <div class="messenger-list-date">@if($oneuser->messages->last()["created_at"]) {{date_format($oneuser->messages->last()["created_at"],'Y-m-d')}} @endif </div>
        <div class="messenger-list-name">{{$oneuser->firstname}} {{$oneuser->lastname}}</div>
        <div class="messenger-list-message">
            <small class="truncate">{{ $oneuser->messages->last()["message"]}}</small>
        </div>
        </div>
    </a>
</li>