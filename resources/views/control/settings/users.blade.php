@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
					<h2 class="page-header">كافة المستخدمين</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
		<a href="#" data-toggle="modal" data-target="#myModalUsers"  class="btn btn-success">اضافة مستخدم جديد</a>
		<div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-middle">
                      <thead>
                        <tr>
						  <th>رقم </th>
						  <th>صورة </th>
                          <th>الاسم الكامل</th>
                          <th> الايميل</th>
						  <th> الرتبة </th>
						  <th> الحالة </th>
					      <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($admins as $admin)
                        <tr id="{{ $admin->id}}">
							<td>{{ $admin->id}}</td>
							<td data-order="">
								<span class="icon-with-child m-r">
								  <img class="circle" width="36" height="36" src="@if($admin->image != '-') {{asset('assets/admin/img/'.$admin->image)}} @else {{asset('assets/admin/img/0180441436.jpg')}} @endif" alt="{{$admin->username}}"/>
								</span>
							</td>
							<td>{{ $admin->firstname }} {{ $admin->lastname }}</td>
							<td class="maw-320">{{ $admin->email }}</td>
							<td class="maw-320">
                                {{ $admin->role }}
							</td>
							<td>
                                @if($admin->status == 0)
                                    <span class="label label-success"> نشيط </span>
                                @else
                                    <span class="label label-danger"> موقف </span>
                                @endif
							</td>
							<td>
                            <div class="btn-group pull-right dropdown">
                              <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#" class="btn-link">تعديل</a></li>
                                    @if($admin->status == 0)
                                        <li><a href="#" class="btn-link deleteusers 1" id="{{ $admin->id}}"> ايقاف الحساب </a></li>
                                    @else
                                        <li><a href="#" class="btn-link deleteusers 2" id="{{ $admin->id}}"> تفعيل الحساب </a></li>
                                    @endif
                              </ul>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
		</div>
	</div>
</div>
@endsection
