<div class="col-lg-6">
    <div class="form-group">
        <label class="default prepend-icon" >رابط المنتج</label>
        <input id="product_url" class="form-control" type="text" name="product_url" tabindex="3" value="{{$product->product_url}}">
    </div>
    <div class="form-group">
        <label class="default prepend-icon" >العدد</label>
        <input id="product_qty" class="form-control" type="number" name="product_qty" tabindex="4" value="{{$product->product_quantity}}">
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group">
        <label class="default prepend-icon" >اسم المنتج</label>
        <input id="product_name" class="form-control" type="text" name="product_name" tabindex="2" value="{{$product->product_name}}">
    </div>
    <div class="form-group">
        <label class="default prepend-icon" >سعر المنتج ب $</label>
        <input id="product_price" class="form-control" type="number" name="product_price" tabindex="5" value="{{$product->product_price}}">
        <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
        <input type="hidden" name="order_id" id="order_id" value="{{$product->purchase_id}}">
    </div>
</div>
<div class="col-lg-12 form-group">
    <label> ملاحظات عن المنتج </label>
    <textarea class="form-control" rows="5" id="product_des" name="product_des">
        {{$product->product_properties}}
    </textarea>
</div>
