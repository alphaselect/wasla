<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipimage extends Model
{
    //

    protected $fillable = [
        'shipping_id', 'imagename'
    ];
}
