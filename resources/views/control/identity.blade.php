@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">حسابات العملاء</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
        <div class="row">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
					<table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input id="checkAll" class="custom-control-input checkAll" value="all" type="checkbox">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </th>
                            <th>رقم العميل</th>
                            <th>اسم العميل</th>
						  	<th>الأيميل</th>
                            <th>المدينة</th>
							<th>الهاتف</th>
                            <th>العنوان</th>
                            <th> ملف الهوية </th>
                            <th> الحالة </th>
						  	<th></th>
                        </tr>
                      </thead>
                      <tbody class="resp_results">
                        @foreach ($clients as $client )
                        <tr id="{{ $client->id }}">
                            <td>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" name="checkAllcargo" type="checkbox" value="">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td class="maw-320">
                                <span class="truncate">
                                    {{ $client->policia }}
                                </span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->firstname }} {{ $client->lastname }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->email }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->city }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->phone }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->address }}</span>
                            </td>
							<td>
                                <a href="{{ asset('storage/identity').'/'.$client->identity_file }}" target="_blank">
                                    الهوية
                                </a>
                            </td>
                            <td>
                                @if($client->identity_status == 1)
                                    <span class="label label-success"> تم التحقق </span>
                                @else
                                    <span class="label label-danger"> قيد المراجعة  </span>
                                @endif
                            </td>
                          <td>
                            <div class="btn-group pull-right dropdown">
                              <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#" class="btn-link operaidentity 1" id="{{ $client->id }}">رفض الهوية</a></li>
                                    <li><a href="#" class="btn-link operaidentity 2" id="{{ $client->id }}">الموافقة على الهوية</a></li>
                              </ul>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
                <div class="box-body">
                    {{ $clients->render() }}
                </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
