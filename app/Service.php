<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //

    protected $fillable = [
        'shipping_id' , 'type' , 'content' , 'status'
    ];

    public function getShip(){
        return $this->belongsTo('App\Shipping','shipping_id');
    }
}
