<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title> شركة الوصلة العالمية </title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-cSfiDrYfMj9eYCidq//oGXEkMc0vuTxHXizrMOFAaPsLt1zoCUVnSsURN+nef1lj" crossorigin="anonymous">
<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
-->
<link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}" />

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>


</head>
<body>
	<!-- header -->
	<div class="container-full user-backend">
    	<div class="header">
            <div class="container-fluid">
                <div class="top-menu-user">
                    <div class="row">
                        <div class="right logo col-md-3 col-sm-12 col-xs-12">
                            <a href="{{ url('account')}}">
                                <img src="{!! asset('assets/front/images/logo.png')!!}" />
                            </a>
                        </div>
                        <div class="top-menu-user-right col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class=" col-md-5  col-xs-12 col-sm-12 hidden-xs">
                                    <form class="forminline form-search" action="">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            <input type="text" class="form-control" name="search" placeholder="البحث عن طلبات الشراء أو الشحنات ">
                                        </div>
                                    </form>
                                </div>
                                <div class="user-cart-info col-md-7 col-sm-12  col-xs-12">
                                    <ul>
                                        <li class="cart">
                                            <a href="{{ url('cart') }}">
                                                <i class="fa fa-shopping-cart cart"><span>
                                                    {{ Cart::instance('wasla')->content()->count() }}
                                                </span></i>
                                            </a>
                                        </li>
                                        <li class="bold Portfolio">
                                            <a href="{{ url('transaction')}}">
                                                <span class="wallet right"> المحفظة </span><span class="wprice"> {{ round_down(get_solde(),2) }} د.ل </span>
                                            </a>
                                        </li>
                                        <li class="user-profil-m">
                                            <a href="#">
                                                <i class="fa fa-angle-down left flesh"></i>
                                                {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                                            </a>
                                            <ul class="sous-menu">
                                                <li><a href="{{ url('profile')}}">الملف الشخصي</a></li>
                                                <li><a href="{{url('/logout')}}">تسجيل الخروج</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                   <div class="clear"></div>
                </div>
            </div>
           <!--end menu top -->
        </div>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-1 col-xs-12 sidebar-right">
                        <div class="" id="sidebar-right">
                            <div class="menu-user">
                                <ul>
                                    <li class="item  menu-sm"><a href="#"><i class="icon icon-home"></i><span>الرئسية</span></a> <i class="fa fa-bars"></i></li>
                                    <li class="item {{ Request::is('account') ? 'active' : '' }}{{ Request::is('plan') ? 'active' : '' }}{{ Request::is('profile') ? 'active' : '' }}"><a href="{{ url('account')}}"><i class="icon icon-home"></i>الرئسية</a></li>
                                    <li class="item {{ Request::is('shipment') ? 'active' : '' }}"><a href="{{url('shipment')}}"><i class="icon icon-shipping"></i>الشحنات</a></li>
                                    <li class="item {{ Request::is('purchase') ? 'active' : ''}} {{ Request::is('neworder') ? 'active' : '' }}"><a href="{{url('purchase')}}"><i class="icon icon-order"></i>طلبات الشراء</a></li>
                                    <li class="item {{ Request::is('transaction') ? 'active' : '' }}"><a href="{{url('transaction')}}"><i class="icon icon-balance"></i>المعاملات المالية</a></li>
                                    <!--<li class="item {{ Request::is('prices') ? 'active' : '' }}"><a href="#"><i class="icon icon-pricing"></i>جدول الأسعار</a></li>-->
                                    <li class="item {{ Request::is('calculator') ? 'active' : '' }}"><a href="{{url('calculator')}}"><i class="icon icon-calculator"></i>حاسبة الأسعار</a></li>
                                    <li class="item {{ Request::is('product') ? 'active' : '' }}"><a href="{{url('product')}}"><i class="icon icon-offre"></i>منتجات وعروض خاصة</a></li>
                                    <li class="item {{ Request::is('contact') ? 'active' : '' }}"><a href="{{url('contact')}}"><i class="icon icon-support"></i>الدعم الفني</a></li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-md-10 col-sm-11 col-xs-12 content-left" id="content-left">
                        @yield('content')
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
       </div>
        <div class=" partners">
       		<div class="container">
                <h1 class="h-home">تسوق عبر الأنترنت من آلاف المواقع الأمريكية</h1>
                <ul class="list-partners">
                    <li>
                        <a href="http://amazon.com" target="_blank">
                              <object data="{{asset('assets/front/images/logo-amazon.svg')}}" class="amazon displayed" type="image/svg+xml" width="100"></object>
                          </a>
                       </li>
                       <li>
                             <a href="https://www.ebay.com/" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-ebay.svg')}}" class="ebay displayed" type="image/svg+xml" width="85"></object>
                             </a>
                       </li>
                       <li>
                             <a href="https://www.hollisterco.com/shop/wd" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-hollister.svg')}}" class="hollister displayed" type="image/svg+xml" width="160"></object>
                             </a>
                       </li>
                       <li>
                             <a href="https://www.abercrombie.com/shop/wd" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-abercrombie-&-fitch.svg')}}" class="fitch displayed" type="image/svg+xml" width="235"></object>
                             </a>
                       </li>
                       <li>
                             <a href="http://www.payless.com/" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-payless.svg')}}" class="payless displayed" type="image/svg+xml" width="120"></object>
                             </a>
                       </li>
                </ul>
             </div>
       </div>

        <div class="footer">
       		<div class="container">
                <footer>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="supported-payment">
                                <h3 class="footer-title">وسائل الدفع المعتمدة</h3>
                                <ul>
                                    <li><img src="{!! asset('assets/front/images/amex-card.jpg')!!}" /></li>
                                    <li><img src="{!! asset('assets/front/images/visa-card.jpg')!!}" /></li>
                                    <li><img src="{!! asset('assets/front/images/master-card.jpg')!!}" /></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4">
                            <div class="footer-contact">
                                <h3 class="footer-title">تواصل معنا</h3>
                                <ul>
                                	<li><a href="#"><i class="fa fa-globe"></i></a></li>
                                	<li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                	<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                	<li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                        	<div class="footer-menu">
                            	<ul>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('faq')}}" target="_blank">الأسئلة الشائعة والمتكررة</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('contactus')}}" target="_blank">هل لديك استفسارات؟</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('privacy')}}" target="_blank">سياسة الخصوصية</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('terms')}}" target="_blank">شروط الاستخدام</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('modepay')}}" target="_blank">وسائل الدفع والشحن المعتمده</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <div class="copyright">
            <p>حقوق النشر محفوظة  2020 &copy;</p>
        </div>
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{url('assets/front/js/smoothproducts.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{url('assets/front/js/script.js')}}"></script>
    <div class="loading hide"><img src="{{asset('assets/front/images/icon/Loading.gif')}}"></div>
    @include ('partial.modal');
</body>
</html>

