<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Page;
use App\Question;
use Response;
class PagesController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function terms(){
        $terms = Page::where('id','=',2)->first();
        return view('control.pages.terms',compact('terms'));
    }
    public function privacy(){
        $privacy = Page::where('id','=',3)->first();
        return view('control.pages.privacy',compact('privacy'));
    }
    public function about(){
        $about = Page::where('id','=',1)->first();
        return view('control.pages.about',compact('about'));
    }
    public function methode(){
        $methode = Page::where('id','=',4)->first();
        return view('control.pages.methode',compact('methode'));
    }
    public function saveterms(Request $request){
        $content = $request->content;
        $idterms = $request->idterms;
       if($content && $idterms){

            DB::table('pages')->where('id', $idterms)
            ->update(
                array(
                    'content' => $content
                )
            );
            return back();

       }
    }

    public function saveprivacy(Request $request){
        $content = $request->content;
        $idprivacy = $request->idprivacy;
       if($content && $idprivacy){

            DB::table('pages')->where('id', $idprivacy)
            ->update(
                array(
                    'content' => $content,
                )
            );
            return back();

       }

    }

    public function saveabout(Request $request){
        $content = $request->content;
        $idabout = $request->idabout;
       if($content && $idabout){

            DB::table('pages')->where('id', $idabout)
            ->update(
                array(
                    'content' => $content,
                )
            );
            return back();

       }
    }

    //
    public function savemethode(Request $request){
        $content= $request->content;
        $idmethode = $request->idmethode;
       if($content && $idmethode){

            DB::table('pages')->where('id', $idmethode)
            ->update(
                array(
                    'content' => $content,
                )
            );
            return back();
       }
    }

    //FAQ ...
    public function faq(){
        $questions = Question::paginate(30);
        return view('control.pages.faq',compact('questions'));
    }
    public function delete(Request $request){
        $question = Question::where('id',$request->id)->delete();
        return back();
    }
    public function addquestion(Request $request){
        $questions = $request->questions;
        $answer = $request->answer;
        if($questions && $answer){
            $question = Question::create([
                "questions"  => $questions,
                "answer"     => $answer,
            ]);

            return Response::json(array('success' => true), 200);

        }
    }
}
