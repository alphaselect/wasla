<!-- Modales-->
<div class="modal fade reportError-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="reportError-modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>التبليغ عن خطأ</h3>
                        <form class="form request error">
                            <div class="form-group">
                                <textarea class="form-control content" rows="4" required="true" id="content" name="content" placeholder="محتوى التبليغ"></textarea>
                                <input type="hidden" name="idparcel" class="idparcel" value=""/>
                            </div>
                            <div class="form-group row-btn">
                                <button type="button" id="error" class="btn btn-front addservices">أرسل</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end form-->
            </div>
        </div>
    </div>
</div>

<!--end Modale Report Error-->
<div class="modal fade RequestSpecialService-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="reportError-modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>طلب خدمة خاصة</h3>
                        <form class="form request service">
                            <div class="form-group">
                                <textarea class="form-control content" rows="4" required="true" id="content" name="content" placeholder="محتوى الطلب "></textarea>
                                <input type="hidden" name="idparcel" class="idparcel" value=""/>
                            </div>
                            <div class="form-group row-btn">
                                <button type="button" id="service" class="btn btn-front addservices">أرسل</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end form-->
            </div>
        </div>
    </div>
</div>
    <!--end Modale Request a Special Service-->
    <div class="modal fade declarationContentShipment-modal" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content reportError-modal-content">
                <div class="modalheader">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="reportError-modal-body">
                    <h3>التصريح بمحتوى الشحنة</h3>
                    <div class="row row_ts declaration">
                        <div id="field" class="field_resp">
                            <div id="field0" class="bts">
                                <div class="form-group col-md-6 col-xs-6">
                                    <label class="control-label" for="productname"> اسم المنتج </label>
                                    <input id="productname" name="productname" required="true" type="text" placeholder="اسم المنتج" class="form-control productname" />
                                    <input id="idparcel" name="idparcel" type="hidden" class="idparcel" />
                                </div>
                                <div class="form-group col-md-3 col-xs-3">
                                    <label class="control-label" for="productqty"> الكمية </label>
                                    <input id="productqty" name="productqty" required="true" type="number" placeholder="الكمية" class="form-control productqty" />
                                </div>
                                <div class="form-group col-md-3 col-xs-3">
                                    <label class="control-label" for="productcost"> السعر </label>
                                    <input id="productcost" name="productcost" required="true" type="number" placeholder="السعر" class="form-control productcost" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <button id="add-more" name="add-more" style="font-weight:bold;font-size:14px;" class="btn-link"> إضافة منتج </button>
                    <!--end form-->
                </div>
                <div class="modal-footer" style="margin-bottom: -25px;padding:13px 13px 13px 13px">
                    <button type="button" class="btn btn-front addinformation"> حفط المعلومات </button>
                </div>
            </div>
        </div>
    </div>
    <!--end Modale Declaration of the content of the shipment-->
    <div class="modal fade ModalTable_declaraion" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content reportError-modal-content">
                <div class="modalheader">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="reportError-modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3> محتوى الشحنة المصرح به ...</h3>
                            <div class="chipment">
                                <div class="chipment-table">
                                   <table role="table" class="table table-purchase">
                                        <thead role="rowgroup" border-spacin="10">
                                            <tr role="row">
                                                <th>كود الشحنة </th>
                                                <th> اسم المنتج </th>
                                                <th> تكلفة المنتج </th>
                                                <th>الكمية</th>
                                                <th>تاريخ الاضافة </th>
                                            </tr>
                                        </thead>
                                        <tbody class="result_decla">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end form-->
                </div>
            </div>
        </div>
    </div>
     <!--item child purchase --->
     <div class="modal fade  ProducteSliderModal" tabindex="-1" role="dialog" aria-labelledby="" id="ProducteSliderModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content reportError-modal-content">
                <div class="modalheader">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="reportError-modal-body product_List">

                </div>
            </div>
        </div>
    </div><!-- end Modale showe item Purchase--->

    <!--item child purchase --->
    <div class="modal fade itmesOrderModal" tabindex="-1" role="dialog" aria-labelledby="" id="itmesOrderModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content reportError-modal-content">
                <div class="modalheader">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="reportError-modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>رقم الطلب:<span class="code-order"></span> </h3>
                            <div class="chipment">
                                <div class="chipment-table">
                                   <table role="table" class="table table-purchase table-responsive">
                                        <thead role="rowgroup" border-spacin="10">
                                            <tr role="row">
                                            <th>المنتج</th>
                                            <th> رابط المنتج</th>
                                            <th>السعر</th>
                                            <th>الكمية</th>
                                            <th>الخصائص</th>
                                            <th>اخر تحديث</th>
                                            <th> التتبع </th>
                                            <th>الحالة</th>
                                            </tr>
                                        </thead>
                                        <tbody role="rowgroup">

                                        <tr><td colspan="7" style="text-align: center"><img src="{{asset('assets/front/images/icon/Loading.gif')}}" width="60"></td></tr>


                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- end Modale showe item Purchase--->
<!-- verifier Profile--->
<div class="modal fade verifierProfileModale" tabindex="-1" role="dialog" aria-labelledby="" id="verifierProfileModale">
        <div class="modal-dialog " role="document">
            <div class="modal-content reportError-modal-content">
                <div class="modalheader">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="reportError-modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>اثبات الهوية</h3>
                            <div class="panel panel-default">
                            <div class="row modale-alert-message" style="margin-top:1em">
                            <div class="alert alert-success hidde" role="alert">
						  	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="alert alert-danger hidde" role="alert">
						  	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                            <p>لإثبات الهوية المرجو ارسال صورة للبطاقة الوطنية او جواز السفر</p>
                            <form action="" method="post" enctype="multipart/form-data" id="form_verifier_profile">
                                <div class="form-group">
                                        @if(Auth::user()->status==2)
                                            <input class="form-control"  type="text" readonly="" value="تم التحقق">
                                            @elseif(Auth::user()->identity_file)
                                            <input class="form-control"  type="text" readonly="" value="في انتظار التحقق">
                                            @else
                                            <input class="form-control" id="verifier_profile" name="verifier_profile" type="file"  >
                                        @endif
                                </div>

                                    <div class="form-group">
                                        @csrf
                                        <button type="button" class="btn btn-front btn-block btn-verifier-profile">
                                        ارسال
                                        </button>
                                    </div>

                            </form>
                            <div class="clear"></div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- end Modale verifier Profile--->
<div class="modal fade payerPurchaseModal" tabindex="-1" role="dialog" aria-labelledby="" id="payerPurchaseModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row modale-alert-message" style="margin-top:1em">
                    <div class="alert alert-success hidde" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="alert alert-danger hidde" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
                <div class="row"><div class="formgroupresult text-center"></div></div>
				<div class="row">
					<div class="form-group">
						<label class="toright"> رصيد المحفطة  </label>
						<input type="text" placeholder="" class="form-control solde" name="solde" value="{{ round_down(get_solde(),2) }}" readonly />
					</div>
                    <div class="form-group">
						<label class="toright">  رقم الطلب  </label>
						<input type="text" placeholder="" class="form-control purchase_code" name="purchase_code" value="" readonly />
					</div>
                    <div class="form-group">
						<label class="toright">  المبلغ المطلوب دفعه    </label>
						<input type="text" placeholder="المطلوب دفعه " class="form-control purchase_price" name="purchase_price" value="" readonly />
					</div>
				</div>
				<div class="modal-footer" style="margin-bottom: -25px;padding: 15px 15px 0px 15px;">
					<button type="button" class="btn btn-default purchase-colose" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success payer-purchase-now"> الدفع الان   </button>
				</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade upadetPlanModal" tabindex="-1" role="dialog" aria-labelledby="" id="upadetPlanModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row modale-alert-message" style="margin-top:1em">

                            <div class="alert alert-success hidde" role="alert">
						  	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <p></p>
                            </div>
                            <div class="alert alert-danger hidde" role="alert">
						  	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <p></p>
                            </div>
                </div>
				<div class="row">
					<div class="form-group">
						<label class="toright"> رصيد المحفطة  </label>
						<input type="text" placeholder="" class="form-control solde" name="" value="{{ round_down(get_solde(),2) }}" readonly />
					</div>
                    <div class="form-group">
						<label class="toright">  مدة الترقية  </label>
						<select class="form-control plan_duration" id="plan_duration" name="plan_duration" required>
                            <option value="1">سنة واحدة </option>
                        </select>
					</div>
                    <div class="form-group">
						<label class="toright">  المبلغ المطلوب دفعه    </label>
						<input type="text" placeholder="المطلوب دفعه " class="form-control price_payer" name="" value="" readonly />
					</div>
				</div>
				<div class="modal-footer" style="margin-bottom: -25px;padding: 15px 15px 0px 15px;">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-upgrade-plan" value="1"> الدفع الان بالدينار  </button>
				</div>
            </div>
        </div>
    </div>
</div>
 <!--end Modales-->
 <div class="modal fade PayShippingModal" tabindex="-1" role="dialog" aria-labelledby="" id="PayShippingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row"><div class="formgroupShipping text-center" style="padding-bottom:20px;"></div></div>
				<div class="row">
					<div class="form-group">
						<label class="toright"> رصيد المحفطة  </label>
						<input type="text" placeholder="رصيد المحفطة" class="form-control" name="" value="{{ round_down(get_solde(),2) }}" readonly />
					</div>
                    <div class="form-group">
						<label class="toright">  رقم الشحنة   </label>
                        <input type="text" placeholder="رقم الشحنة" class="form-control parcel_code" name="parcel_code" value="" readonly />
                        <input id="idparcel" name="idparcel" type="hidden" class="idparcel" />
                    </div>
                    <div class="form-group">
						<label class="toright"> تكلفة الخدمات  </label>
						<input type="text" class="form-control pricetopay_service" name="" value="" readonly />
					</div>
                    <div class="form-group">
						<label class="toright">  تكلفة الشحن  </label>
						<input type="text" class="form-control pricetopay_d" name="" value="" readonly />
                    </div>
                    <div class="form-group">
						<label class="toright"> السعر المطلوب دفعه </label>
						<input type="text" class="form-control pricetopay_global" name="" value="" readonly />
					</div>
				</div>
				<div class="modal-footer" style="margin-bottom: -25px;padding: 15px 15px 0px 15px;">
					<button type="button" class="btn btn-default" data-dismiss="modal">اغلاق الصفحة </button>
                    <button type="button" class="btn btn-success paynowShipping"> الدفع الان برصيد المحفظة  </button>
				</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade ModalTable_Service" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content reportError-modal-content">
            <div class="modalheader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="reportError-modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3> تفاصيل الخدمات  المطلوبة على الشحنة </h3>
                        <div class="chipment">
                            <div class="chipment-table">
                               <table role="table" class="table table-purchase">
                                    <thead role="rowgroup" border-spacin="10">
                                        <tr role="row">
                                            <th> كود الشحنة </th>
                                            <th> وصف الخدمة </th>
                                            <th> تاريخ الطلب  </th>
                                            <th> الحالة </th>
                                        </tr>
                                    </thead>
                                    <tbody class="result_Service">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end form-->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="cancelPurchaseModal">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="row modale-alert-message" style="margin-top:1em">
            <div class="alert alert-success hidde" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p></p>
            </div>
            <div class="alert alert-danger hidde" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p></p>
            </div>
        </div>
        <p style="font-size: 18px;">هل تريد حقا إلغاء هذا الطلب؟</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">لا</button>
        <button type="button" class="btn btn-danger cancel-purchase-now" value="0">نعم</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
