@extends('layouts.app')

@section('content')
<div class="container">
    <div class="logo-hom contact">
        <h1 class="h-home">
            {!! $methode->title !!}
        </h1>
    </div><!--end logo--hom-->
    <div class="section">
         <p class="section-description">
           {!! $methode->content !!}
         </p>
    </div>
 </div>
@endsection
