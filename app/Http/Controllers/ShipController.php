<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipping;
use Auth;
use Response;
use App\Declaration;
use App\Service;
class ShipController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }

    public function shipment(){
        $parcels = Shipping::where('user_id',Auth::user()->id)->whereIn('status',[0,1,5])->paginate(15);
        return view('account.shipping.newshipment',compact('parcels'));
    }

    public function ready(){
        $parcels = Shipping::where('user_id',Auth::user()->id)->where('status',2)->paginate(15);
        return view('account.shipping.ready',compact('parcels'));
    }

    public function delivered(){
        $parcels = Shipping::where('user_id',Auth::user()->id)->where('status',3)->paginate(15);
        return view('account.shipping.delivered',compact('parcels'));
    }

    public function round_up($value, $places=1) {
        if ($places < 0) { $places = 0; }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }
    public function float_to_int($value){
       $var = round_up($value);
       return str_replace(".0", "", $var);
    }

    public function addinformation(Request $request){

        $data  = json_decode($request->data_array,true);
        $productcost = 0;
        foreach($data AS $value){

            $delcare = Declaration::create([
                'shipping_id' => $value['idparcel'],
                'productname' => $value['productname'],
                'productcost' => $value['productcost'],
                'productqty'  => $value['productqty'],
            ]);

            $productcost = $value['productcost'] * $value['productqty'];
            $idparcel    = $value['idparcel'];
        }


        if($idparcel && $productcost){

            $parcels = Shipping::where('user_id',Auth::user()->id)->where('id',$idparcel)->first();

            $sum_length = $parcels->lenght + $parcels->width + $parcels->height;
            $fee = ($productcost * 0.015) + 2;
            $volumetric_charge_weight = ($parcels->lenght * $parcels->width * $parcels->height)/5000;
            $volumetric_charge = $this->round_up($volumetric_charge_weight,1);

            if($volumetric_charge>60 || $parcels->weight>60){
                //can'be be shipped
            }else{
                if($sum_length>150 && $volumetric_charge > $parcels->weight){
                    $i=$volumetric_charge;
                    if(0<$i & $i<=10){
                        $price = $i*15+$fee;
                    }
                    if(10<$i & $i<=20){
                        $price = $i*12+$fee;
                    }
                    if(20<$i & $i<=30){
                        $price = $i*10+$fee;
                    }
                    if(30<$i){
                        $price = $i*9+$fee;
                    }
                    $price= $this->round_up($price,1);
                }else{

                    if($parcels->getuser->plan == 0){

                        $price = ($this->round_up($parcels->weight)-0.1)*10+3+floor($parcels->weight)*3-floor($parcels->weight)+$fee;
                        $price = $this->round_up($price,3);

                    }else{

                        $price=($parcels->weight * 10)+$fee;
                        $price= $this->round_up($price,1);

                    }

                }
            }

            Shipping::where('id', $idparcel)->update(
                array(
                    'pricetopay' => $price
                )
            );

            return Response::json(array('success' => true), 200);

        }

    }

    public function delared(Request $request){
        $parcel = Shipping::where('user_id',Auth::user()->id)->where('id',$request->idparcel)->first();
        $viewRendered = view('partial.declaration',compact('parcel'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function addservices(Request $request){
        $idparcel   = $request->idparcel;
        $content    = $request->content;
        $type       = $request->type;
        if($idparcel && $content && $type){
            //dd($request->all());
            $service = Service::create([
                'shipping_id' => $idparcel,
                'type' => $type,
                'content' => $content,
            ]);

            return Response::json(array('success' => true), 200);

        }
    }

    public function acceptshipping(Request $request){
        $idparcel   = $request->idparcel;
        if($idparcel){
            Shipping::where('id', $idparcel)->update(
                array(
                    'status' => 0
                )
            );
            return Response::json(array('success' => true), 200);
        }
    }

    public function specialservice(Request $request){
        $parcel = Shipping::where('user_id',Auth::user()->id)->where('id',$request->idparcel)->first();
        $viewRendered = view('partial.service',compact('parcel'))->render();
        return Response::json(['html'=>$viewRendered]);
    }
}
