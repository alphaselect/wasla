<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //

    protected $fillable = [
        'user_id' , 'invoice' ,'operations','funds_add','funds_total','modpayment','status'
    ];

    public function getuser(){
        return $this->belongsTo('App\User','user_id');
    }


}
