@extends('layouts.app')

@section('content')

<div class="container">
   <div class="logo-hom contact">
       <h1 class="h-home">
        الأسئلة الشائعة والمتكررة
       </h1>
   </div><!--end logo--hom-->
   <div class="section">
        <p class="section-description text-center">
        </p>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group" id="accordion1">
                    @foreach ($questions as $question)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <h5 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_{{ $question->id}}">{{ $question->questions}}</a>
                            </h5>
                            </div>
                            <div id="accordion1_{{ $question->id}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    {!! $question->answer !!}
                                </p>
                            </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
   </div>
</div>
@endsection
