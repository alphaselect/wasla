@extends('layouts.account')
@section('content')
<section>
    <div class="pricing">
        <h1 class="h-front">اسعار الباقات</h1>
            <div class="prices">
                <div cass="row">
                    <div class="counttime">
                        <p>الوقت المتبقي لانتهاء اشتراكك الحالي على الموقع:</p>
                        <ul>
                        <li class="item year"><span id="year">00</span>سنة</li>
                        <li class="item month"><span id="month">00</span>شهر</li>
                        <li class="item daye"><span id="daye">00</span>يوم</li>
                        <li class="item hour"><span id="hour">00</span>ساعة</li>
                        <li class="item minute"><span id="minute">00</span>دقيقة</li>
                        <li class="item second"><span id="second">00</span>ثانية</li>

                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row alert-message" style="margin-top:1em">
                    <div class="alert alert-success hidde" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p></p>
                    </div>
                    <div class="alert alert-danger hidde" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        
                        <div class="price-contenaire {{$plan==0? 'active' : ''}}">
                            <div class="price-content free-account">
                                <h3 class="h-priceaccount">حساب المجاني</h3>
                                <div class="price-txt">
                                    <table>
                                        <thead></thead>
                                        <tbody>
                                            <tr><td>سنويا </td><td>:مجانا</td></tr>
                                            <tr><td>تجمبع الشحنات </td><td>:5$</td></tr>
                                            <tr><td>صور الشحنة </td><td>:8$</td></tr>
                                            <tr><td>خدمات خاصة </td><td>:12%</td></tr>
                                            <tr><td>الشراء من موقع اخر&nbsp;&nbsp;&nbsp;</td><td>:--</td></tr>
                                            <tr><td>سقف التأمين </td><td>:1000$</td></tr>
                                            <tr><td>الشحن الجوي </td><td>:15$</td></tr>
                                            <tr><td>الشحن البحري </td><td>:4.5$</td></tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <button class="btn btn-upgrade-plan-free btn-pricing-free" value="0">اختر الباقة</button>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="price-contenaire {{$plan==1? 'active' : ''}}">
                            <div class="price-content saving-account">
                                <h3 class="h-priceaccount">حساب التوفير</h3>
                                <div class="price-txt">
                                    <table>
                                        <thead></thead>
                                        <tbody>
                                            <tr><td>سنويا </td><td>:100$</td></tr>
                                            <tr><td>تجمبع الشحنات </td><td>:2$</td></tr>
                                            <tr><td>صور الشحنة </td><td>:5$</td></tr>
                                            <tr><td>خدمات خاصة </td><td>:8%</td></tr>
                                            <tr><td>الشراء من موقع اخر&nbsp;&nbsp;&nbsp;</td><td>:10%</td></tr>
                                            <tr><td>سقف التأمين </td><td>:2000$</td></tr>
                                            <tr><td>الشحن الجوي </td><td>:13$</td></tr>
                                            <tr><td>الشحن البحري </td><td>:3.5$</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button class="btn btn-pricing btn-pricing-saving " data-toggle="modal" data-target="#upadetPlanModal" value="1">اختر الباقة</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</section>
<script type="text/javascript" >
    countDownDate('{{$date_end}}');

    function countDownDate($date="0"){
        //$date="Jun 1, 2025 15:37:25"
        var countDownDate = new Date($date).getTime();
        var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
            
        // Time calculations for Years Months days, hours, minutes and seconds
        var years = Math.floor(distance / (1000 * 60 * 60 * 24*365));
        var months=Math.floor((distance % (1000 * 60 * 60 * 24 * 365))/(1000 * 60 * 60 * 24 * 30.41666666));
        var days = Math.floor((distance % (1000 * 60 * 60 * 24 * 30.41666666))/(1000 * 60 * 60 * 24) );
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            

        document.getElementById("year").innerHTML = years;
        document.getElementById("month").innerHTML = months;
        document.getElementById("daye").innerHTML = days;
        document.getElementById("hour").innerHTML = hours;
        document.getElementById("minute").innerHTML = minutes;
        document.getElementById("second").innerHTML = seconds;
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("year").innerHTML = "00";
        document.getElementById("month").innerHTML = "00";
        document.getElementById("daye").innerHTML = "00";
        document.getElementById("hour").innerHTML = "00";
        document.getElementById("minute").innerHTML = "00";
        document.getElementById("second").innerHTML = "00";
        }
        }, 1000);

}
</script>
@endsection('content')
