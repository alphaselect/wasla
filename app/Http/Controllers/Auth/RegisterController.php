<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\RegistersUsers;
//use \Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Notifications\Notifiable;

use App\Http\helpers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Response;
use Session;
use Carbon\Carbon;
use App\Mail\Sendmail;
use Mail;
use App\Payment;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    //use RegistersUsers;
    use Notifiable;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function register(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $validator = Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'second_firstname' => 'required|string|max:255',
            'second_lastname'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6',
            'country'   => 'required',
            'city'      => 'required',
            'address'   => 'required',
            'phone'     => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }

        $user = User::create([
            'firstname'   => $data['firstname'],
            'lastname'    => $data['lastname'],
            'second_firstname'   => $data['second_firstname'],
            'second_lastname'    => $data['second_lastname'],
            'email'       => $data['email'],
            'country'     => $data['country'],
            'city'        => $data['city'],
            'address'     => $data['address'],
            'password'    => bcrypt($data['password']),
            'verifyToken' => Str::random(60),
            'policia'     => $this->createPolicia(),
            'phone'       => $data['phone'],
            'sms_code'    => generateRandomNumber(4),
            'plan'        => 0,
            'status'      => 0
        ]);

        $payment = Payment::create([
            'user_id' =>  $user->id,
            'invoice' => str_random(15),
            'operations' => 'حساب مجاني ',
            'funds_add' => 0,
            'funds_total' => 0,
            'modpayment' => '-'
        ]);


        $data = [
            'resp'   => $user,
            'status' => 'activate',
        ];

        $sms = "رمز تفعيل رقم هاتفك هو :".$user->sms_code;
        $user->notify(new \App\Notifications\SmsNotify($sms));

        Mail::to($user->email)->send(new Sendmail($data));

        return Response::json(array('success' => true , 'user_id' => $user->id , 'phone' => $user->phone), 200);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    /*protected function create(array $data)
    {

    }*/

    public function verify($token){
        User::where('verifyToken',$token)->update(array('status' => 1));
        return redirect('/')->with('activated', 1);
    }
    public function verify_phone(Request $request){
        $userid= $request->userID;
        $sms_code= $request->sms_code;
        $my_phone= $request->my_phone;
        if($userid && $sms_code && $my_phone){

            User::where('id',$userid)->where('sms_code','LIKE',$sms_code)
            ->update(
                array(
                    'phone_status' => 1
                )
            );
            return Response::json(array('success' => true), 200);
        }
    }
    public function createPolicia()
    {
        $user = User::select('policia')->orderBy('id', 'desc')->first();
        if($user){
            $policia = generate_policia($user->policia);
        }else{
            $policia = generate_policia();
        }
        return $policia;
    }
    protected function getPolicia($policia)
    {
        return User::select('policia')->where('policia', 'like', $policia.'%')->get();
    }
}
