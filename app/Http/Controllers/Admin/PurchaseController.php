<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Purchase;
use App\ChildPurchase;
use Response;
class PurchaseController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function new(){
        $orders = Purchase::where('status',0)->get();
        return view('control.purchase.new',compact('orders'));
    }

    public function waiting(){
        $orders = Purchase::where('status',1)->get();
        return view('control.purchase.waiting',compact('orders'));
    }

    public function proccess(){
        $orders = Purchase::where('status',2)->get();
        return view('control.purchase.processing',compact('orders'));
    }

    public function purchase(){
        $orders = Purchase::where('status',3)->get();
        return view('control.purchase.purchase',compact('orders'));
    }
    public function completed(){
        $orders = Purchase::where('status',4)->get();
        return view('control.purchase.completed',compact('orders'));
    }


    public function getprd(Request $request){
        $order = Purchase::where('id',$request->fastbuyetat_prd)->first();
        $viewRendered = view('control.partial.product',compact('order'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function editproduct(Request $request){
        $product = ChildPurchase::where('id',$request->edit_fastbuy_product)->first();
        $viewRendered = view('control.partial.editprd',compact('product'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function updateprd(Request $request){

        DB::table('child_purchases')->where('id',$request->product_id)->update(
            array(
                'product_name'      => $request->product_name,
                'product_url'       => $request->product_url,
                'product_quantity'       => $request->product_qty,
                'product_price'     => $request->product_price,
                'product_properties'       => $request->product_des
            )
        );
        return Response::json(array('success' => true), 200);
    }

    public function accept(Request $request){
        $orderid = $request->orderid;
        $status = $request->status;
        if($orderid && $status){

            DB::table('purchases')->where('id',$orderid)->update(
                array(
                    'status'  => $status,
                    'updated_at' => Now()
                )
            );

            DB::table('child_purchases')->where('purchase_id',$orderid)->update(
                array(
                    'status'  => $status,
                    'updated_at' => Now()
                )
            );

            /*$data = [
                'resp'   => $order,
                'status' => 'acceptOrder',
                'lang'   => $order->getUser->lang
            ];

            Mail::to($order->getUser->email)->send(new Sendmail($data));*/

            return Response::json(array('success' => true), 200);
        }
    }

    public function addtracking (Request $request){
        $data = $request->all();
        //dd($data);
        DB::table('child_purchases')->where('id',$data['chilorder_id'])->update(
            array(
                'companyurl'   => $data['companyurl'],
                'trackinginfo' => $data['trackinginfo'],
                'status'  => 3,
                'updated_at' => Now()
            )
        );
        $child = ChildPurchase::where('purchase_id',$data['orderId'])->where('status',2)->count();
        if($child <1){
            DB::table('purchases')->where('id',$data['orderId'])->update(
                array(
                    'status'  => 3,
                    'updated_at' => Now()
                )
            );
        }
        return Response::json(array('success' => true), 200);
    }
    public function prd_arrive (Request $request){

        DB::table('child_purchases')->where('id',$request->product_id)->update(
            array(
                'status'  => 4,
                'updated_at' => Now()
            )
        );
        $child = ChildPurchase::where('purchase_id',$request->orderId)->where('status',3)->count();
        if($child <1){
            DB::table('purchases')->where('id',$request->orderId)->update(
                array(
                    'status'  => 4,
                    'updated_at' => Now()
                )
            );
        }

        return Response::json(array('success' => true), 200);

    }
}
