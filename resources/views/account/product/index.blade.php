@extends('layouts.account')
@section('content')
<h1 class="h-front">منتجات وعروض خاصة</h1>
<section class="mycompt">
    <div class="special_offers">
        <div class="row">
            @foreach ($products as $product)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="offre">
                        <div class="img-product">
                            @foreach ($product->Prdimages as $img)
                                <img class="showprd" id="{{ $product->id }}" src="{!! asset('upload/product') !!}/{{ $img->product_image }}" alt="">
                                @break
                            @endforeach
                        </div>
                        <div class="title-product">
                            <span class="title">{{ $product->product_name }}</span>
                            <span class="price">{{ $product->product_price }} د.ل </span>
                        </div>
                        <form action="{{ url('cart')}}" method="post">
                            @csrf
                            <input type="hidden" name="product_qty" value="1"/>
                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                            <input type="hidden" name="product_name" value="{{$product->product_name}}"/>
                            <input type="hidden" name="product_price" value="{{$product->product_price}}"/>
                            <button type="submit" class="btn btn-front-full" style="withd">
                                <i class="fa fa-shopping-cart"><span>+</span></i> اضف لسلة التسوق
                            </button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@endsection('content')
