<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title> وصلة العالمية | لخدمات الشحن والتسوق عبر الإنترنت </title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
-->
<link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

	<div class="container-full">
    	<div class="header">
            <div class="contact">
                <ul class="pull-left">
                    <li><i class="fa fa-envelope-o"></i>info@global-link.ly</li>
                    <li><i class="fa fa-phone"></i> +218 910457534  </li>
                </ul>
                <ul class="pull-right">
                    @if(!Auth::check())
                        <li>
                            <button data-toggle="modal" style="padding: 6px;" class="btn-login" data-target="#login-modal"> <i class="fa fa-user"></i> تسجيل الدخول </button>
                        </li>
                        <li>
                            <button data-toggle="modal" style="padding: 6px;" class="btn-register" data-target="#registration-modal"> <i class="fa fa-user"></i>الإشتراك بالخدمة</button>
                        </li>
                    @else
                        <li>
                            <a href="{{ url('/account')}}"> لوحة التحكم </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout')}}"> تسجيل الخرورج </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!--end contact-->
            <div class="top-menu">
                <div class="clear"></div>
                <div class="menu">
                        <a href="{{ url('/')}}" class="logo">
                            <img src="{{asset('assets/front/images/logo.png')}}" />
                        </a>
                        <nav class="nav " id="topnav">
                        	<ul class="menu-rs">
                                <li class="rs-select-item"><a href="{{ url('/')}}">الرئيسية</a></li>
                                <li class="nav-rs"><a href="javascript:void(0);" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a></li>
                            </ul>
                            <div class="clear"></div>
                            <ul class="nav-main-item">
                                <li class="active first-nav item-nav"><a href="{{ url('/')}}">الرئيسية</a></li>
                                <li class="item-nav"><a href="{{ url('about')}}">عن الشركة</a></li>
                                <li class="item-nav"><a href="#services">خدماتنا</a></li>
                                <li class="item-nav"><a href="{{ url('faq') }}">الأسئلة الشائعة</a></li>
                                <li class="item-nav-last"><a href="{{ url('contactus')}}">تواصل معنا</a></li>
                            </ul>
                    </div>
                </div>
               <div class="clear"></div>
            </div>
        <!--end menu top -->
        </div>
        @yield('content')
       <!--end ratings -->
       <div class="partners indexpartners">
       		<div class="container">
                <h1 class="h-home">تسوق عبر الأنترنت من آلاف المواقع الأمريكية</h1>
                <ul class="list-partners">
                    <li>
                        <a href="http://amazon.com" target="_blank">
                              <object data="{{asset('assets/front/images/logo-amazon.svg')}}" class="amazon displayed" type="image/svg+xml" width="100"></object>
                          </a>
                       </li>
                       <li>
                             <a href="https://www.ebay.com/" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-ebay.svg')}}" class="ebay displayed" type="image/svg+xml" width="85"></object>
                             </a>
                       </li>
                       <li>
                             <a href="https://www.hollisterco.com/shop/wd" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-hollister.svg')}}" class="hollister displayed" type="image/svg+xml" width="160"></object>
                             </a>
                       </li>
                       <li>
                             <a href="https://www.abercrombie.com/shop/wd" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-abercrombie-&-fitch.svg')}}" class="fitch displayed" type="image/svg+xml" width="235"></object>
                             </a>
                       </li>
                       <li>
                             <a href="http://www.payless.com/" target="_blank">
                                 <object data="{{asset('assets/front/images/logo-payless.svg')}}" class="payless displayed" type="image/svg+xml" width="120"></object>
                             </a>
                       </li>
                </ul>
             </div>
       </div>
       <!--end partners -->
       <div class="footer">
       		<div class="container">
                <footer>
                    <div class="row">
                        <div class="col-md-4">
                        	<div class="footer-menu">
                            	<ul>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('faq')}}">الأسئلة الشائعة والمتكررة</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('contactus')}}">هل لديك استفسارات؟</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('privacy')}}">سياسة الخصوصية</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('terms')}}">شروط الاستخدام</a></li>
                                	<li><i class="fa fa-caret-left"></i><a href="{{ url('modepay')}}">وسائل الدفع والشحن المعتمده</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-contact">
                                <h3 class="footer-title">تواصل معنا</h3>
                                <ul>
                                	<li><a href="#"><i class="fa fa-globe"></i></a></li>
                                	<li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                	<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                	<li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="supported-payment">
                                <h3 class="footer-title">وسائل الدفع المعتمدة</h3>
                                <ul>
                                    <li><img src="{{asset('assets/front/images/amex-card.jpg')}}" /></li>
                                    <li><img src="{{asset('assets/front/images/visa-card.jpg')}}" /></li>
                                    <li><img src="{{asset('assets/front/images/master-card.jpg')}}" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
       </div>
       <div class="copyright">
            <p>حقوق النشر محفوظة  2020 &copy;</p>
       </div>
    </div>
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="{{ asset('assets/front/js/script.js')}}"></script>
<script>
    $('.carousel').carousel();
    $(document).on('click','.nav-rs a',function(){
        //$("top-menu nav").addClass("nav-responsive");
        $(".top-menu nav").toggleClass("nav-responsive");
        $(".top-menu .nav .nav-main-item").slideToggle();
    });

    function get_windowWith(){
        var $with=$( window ).width();
        if($with<769){
            $(".nav").addClass("menu-rs-active");
            }else{
            $(".nav").removeClass("menu-rs-active");
                }
	}
    get_windowWith();
    $(window).resize(function() {
        get_windowWith();
    });
    $(document).on("click",".menu-rs-active .nav-main-item li",function(){
        $(".nav-main-item li").removeClass("active");
        $(this).addClass("active");
        var $txt=$(".nav-main-item li.active a").text();
        $(".menu-rs .rs-select-item a").text("");
        $(".menu-rs .rs-select-item a").text($txt);
        $(".top-menu .nav .nav-main-item").slideUp();
    });
</script>
@if(!empty(Session::get('activated')) && Session::get('activated') == 1)
    <script>$(function() { $('#ModalVerify').modal('show'); });</script>
@endif
@include('auth.login')
@include('auth.register')
</body>
</html>

