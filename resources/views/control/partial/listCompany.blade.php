
@foreach($companies as $companie)
    <tr class="{{$companie->id}}">
        <td>
            <img style="height:40px;width: 40px;" src="{{asset('upload/company')}}/{{$companie->companylogo}}"/>
        </td>
        <td>{{$companie->companiename}}</td>
        <td><a href="{{$companie->company_url}}" target="_blank">رابط التتبع</a></td>
        <td>{{$companie->datedelevier}}</td>
        <td>{{$companie->updated_at}}</td>
        <td><span class="label label-success">متاحة</span></td>
        <td>
            <div class="btn-group pull-right dropdown">
                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{url('control/company/show',$companie->id)}}" class="btn-link ">الاسعار</a></li>
                    <li><a href="{{url('control/company/edit',$companie->id)}}" class="btn-link ">تحرير</a></li>
                </ul>
            </div>
        </td>
    </tr>
@endforeach
