@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> كل الشحنات  </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <input type="search" id="7" class="form-control search_parcel" placeholder="البحث عن الشحنة">
                    </div>
                    <div class="col-lg-6">
                        <a href="#" class="btn btn-warning pull-right addselectedparcel"> اضافة الشحن للرحلة </a>
                        <a href="{{ url('control/shipping/cargoflights')}}" style="margin-left: 10px;" class="btn btn-info pull-right"> كل رحلات الشجن  </a>
                        <input type="hidden" value="{{ $id }}" class="idpolicia"/>
                    </div>
                </div>
          </div>
          <br/>
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" value="all" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>الصورة</th>
                          <th> كود الشحنة</th>
                          <th>رقم العميل</th>
                          <th>تاريخ الاضافة</th>
                          <th>الوزن / الأبعاد</th>
                          <th>الدولة </th>
                          <th> التتبع </th>
                          <th> المحتوى </th>
                          <th> حالة </th>
                          <th> الدفع </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($parcels as $parcel)
                                <tr class="{{ $parcel->id}}">
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAllcargo" type="checkbox" value="{{ $parcel->id}}">
                                            <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <span class="icon-with-child m-r">
                                            @foreach ($parcel->GetImage as $image)
                                                <img class="circle" width="40" height="40" src="{{ asset('upload/ship') }}/{{ $image->imagename}}" alt="">
                                                @break
                                            @endforeach
                                        </span>
                                    </td>
                                    <td> {{ $parcel->parcelcode }} </td>
                                    <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
                                    <td> {{ $parcel->created_at->format('Y-m-d')}} </td>
                                    <td>
                                        {{ $parcel->weight }} كيلو <br>
                                        {{ $parcel->width }} * {{ $parcel->height }} * {{ $parcel->lenght }} سم
                                    </td>
                                    <td>
                                        @if($parcel->shipcountry == 'usa')
                                            امريكا
                                        @endif
                                    </td>
                                    <td>{{ $parcel->tracking }}</td>
                                    <td> <a href="#" id="{{ $parcel->id}}" class="getDeclaration">مشاهدة </a> </td>
                                    <td>
                                        @if($parcel->status == 0)
                                            <span class='label label-info'> في المستودع </span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($parcel->statuspay == 0)
                                            <span class='label label-primary'> ليس بعد </span>
                                        @elseif($parcel->statuspay == 1)
                                            <span class='label label-success'> تم الدفع  </span>
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" id="{{ $parcel->id}}" class="btn btn-success btn-sm addparcel"> اضف الشحنة  </button>
                                    </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                {!! $parcels->links() !!}
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
