<!-- Modal -->
<div class="modal fade" id="myModalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form role="form" method="post" action="#">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        ارسال رسالة
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subject">موضوع الرسالة </label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"/>
                    </div>
                    <div class="form-group">
                        <label for="message">محتوى الرسالة  </label>
                        <textarea type="text" class="form-control" name="message" id="message" placeholder="Message"></textarea>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary sendemail" value="ارسال">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" value="" id="email_userid" name="email_userid">
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>

</div>
